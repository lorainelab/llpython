#!/usr/bin/env python

import unittest
import os,sys

from Mapping.FeatureFactory import makeFeatFromGffFields

class PhytozomeGff(unittest.TestCase):

   # note: it doesn't have the header 
   test_file = 'data/PhytozomeGFF/Zmays_121_gene.TEST.gff3'

   def setUp(self):
      fh = open(self.test_file)
      self.lines = fh.readlines()
      fh.close()
      self.tokss = []
      for line in self.lines:
         self.tokss.append(line.rstrip().split('\t'))

   def tearDown(self):
      del(self.lines)
      del(self.tokss)
      
   def testMakeFeatFromGffFields(self):
      "makeFeatFromGffFields should make something for every line in the file."
      feats = map(lambda x:makeFeatFromGffFields(x),self.tokss)
      self.assertEquals(len(feats),len(self.tokss))

                       
if __name__ == "__main__":
    unittest.main()
    
