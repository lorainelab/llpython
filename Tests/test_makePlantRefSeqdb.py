#!/usr/bin/env python

import unittest
import os,sys

import makePlantRefSeqDb as m

class RefSeqTest(unittest.TestCase):

   test_path = 'data/RefSeq'

   def test_makeDb(self):
      "makeDb should create a database file for every species when sep is true"
      m.makeDb(sep=True,species=m.SPECIES,path=self.test_path)
      num_made=len(filter(lambda x:x.endswith('.aa.fa'),os.listdir(self.test_path)))
      d={}
      for s in m.SPECIES:
         name='_'.join(s.split(' ')[0:2])
         d[name]=1
      right_answer=len(d.keys())
      self.assertEquals(num_made,right_answer)

   def test_nonzero(self):
      "Soybean database should have non-zero size."
      m.makeDb(sep=True,species=m.SPECIES,path=self.test_path)
      test_db = self.test_path+os.sep+'Glycine_max.aa.fa'
      right_answer=True
      answer=os.path.isfile(test_db) and os.path.getsize(test_db)>0
      self.assertEquals(answer,right_answer)
   
   def test_rightSize(self):
      "soybean database should have four sequences in it"
      m.makeDb(sep=True,species=m.SPECIES,path=self.test_path)
      soybean=self.test_path+os.sep+'Glycine_max.aa.fa'
      right_answer=4
      fh = open(soybean)
      lines = fh.readlines()
      numseqs=len(filter(lambda x:x.startswith('>'),lines))
      self.assertEquals(numseqs,right_answer)

   def tearDown(self):
      "Clean up after a test."
      files=os.listdir(self.test_path)
      for f in files:
         if not f.endswith('aa.fa'):
            continue # I'm feeling negative today
         if not f.startswith(self.test_path):
            f=self.test_path+os.sep+f
         os.unlink(f)

if __name__ == "__main__":
    unittest.main()
    
