#!/usr/bin/env python

import os,sys

ANNOTSBACKGROUND='FFFFFF'
ANNOTSFOREGROUND='000000'

class QuickloadStyle():
    "Optional attributes of file tag in QuickLoad annots.xml"
    def __init__(self,load_hint=None,label_field=None,
                 background=None,foreground=None,max_depth=None,
                 name_size=None,show2tracks=None,direction_type=None,
                 positive_strand_color=None,negative_strand_color=None,
                 view_mode=None,**kwargs):
        d = {}
        d['load_hint']=load_hint
        d['label_field']=label_field
        d['background']=background
        d['foreground']=foreground
        d['max_depth']=max_depth
        d['name_size']=name_size
        d['show2tracks']=show2tracks
        d['direction_type']=direction_type
        d['positive_strand_color']=positive_strand_color
        d['negative_strand_color']=negative_strand_color
        d['view_mode']=view_mode
        self._d = d
    def getLoadHint(self):
        return self._d['load_hint']
    def getLabelField(self):
        return self._d['label_field']
    def getBackground(self):
        d = self._d
        return d['background']
    def getForeground(self):
        d = self._d
        return d['foreground']
    def getMaxDepth(self):
        d = self._d
        return d['max_depth']
    def getNameSize(self):
        d = self._d
        return d['name_size']
    def getShow2Tracks(self):
        d = self._d
        return d['show2tracks']
    def getDirectionType(self):
        d = self._d
        return d['direction_type']
    def getPositiveStrandColor(self):
        d = self._d
        return d['positive_strand_color']
    def getNegativeStrandColor(self):
        d = self._d
        return d['negative_strand_color']
    def getViewMode(self):
        d = self._d
        return d['view_mode']
    def safeSet(self,key,val):
        if not self._d.has_key(key):
            self._d[key]=val
    def forceSet(self,key,val):
        self._d[key]=val
    def getStyles(self):
        return self._d

class QuickloadAnnotStyle(QuickloadStyle):
    "Annotations style."
    def __init__(self,**kwargs):
        QuickloadStyle.__init__(self,**kwargs)
        self.safeSet('background',ANNOTSBACKGROUND) 
        self.safeSet('direction_type','arrow')
        self.safeSet('name_size','14')
        self.safeSet('label_field','id')
        self.safeSet('foreground',ANNOTSFOREGROUND)

class QuickloadReadStyle(QuickloadStyle):
    "Style for sequence read alignments"
    def __init__(self,**kwargs):
        QuickloadStyle.__init__(self,**kwargs)
        self.safeSet('background',ANNOTSBACKGROUND) 
        self.safeSet('max_depth','10') 
        self.safeSet('show2tracks','False')

class QuickloadCoverageStyle(QuickloadStyle):
    "Style for sequence coverage graph"
    def __init__(self,**kwargs):
        QuickloadStyle.__init__(self,**kwargs)
        self.safeSet('background',ANNOTSBACKGROUND) 
        self.safeSet('name_size','12')

class QuickloadJunctionStyle(QuickloadAnnotStyle):
    "Style for sequence coverage graph"
    def __init__(self,**kwargs):
        QuickloadAnnotStyle.__init__(self,**kwargs)
        self.forceSet('label_field','score')
        self.forceSet('show2tracks',False)
        self.forceSet('direction_type','arrow')

class Annotations():
    "Functional genomics or annotation data set to be displayed in QuickLoad"
    def __init__(self,fn=None,
                 title=None, 
                 description=None,
                 url=None,
                 style=None):
        self._file = fn
        self._title = title
        self._description = description
        self._url = url
        self._style = style
    def setStyle(self,style):
        self._style=style
    def getStyle(self):
        return self._style
    def getFile(self):
        "Path or URL of file"
        return self._file
    def getTitle(self):
        "Data set title."
        return self._title
    def getDescription(self):
        "Human-friendly description that will appear in a tooltip in IGB."
        return self._description
    def getUrl(self):
        return self._url
    def setUrl(self,url):
        self._url = url

def makeFilesTag():
    return '<files>\n'

def closeFilesTag():
    return '</files>\n'

def makeFileTag(sample):
    txt = '  <file\n    name="%s"\n    title="%s"\n    description="%s"\n'%(sample.getFile(),
                                                                            sample.getTitle(),
                                                                            sample.getDescription())
    if sample.getUrl():
        txt = txt + '    url="%s"\n'%sample.getUrl()
    style = sample.getStyle()
    styles_d = style.getStyles()
    for key in styles_d.keys():
        val = styles_d[key]
        if val != None:
            
            txt = txt + '    %s="%s"\n'%(key,val)
    txt = txt + "  />\n"
    return txt

# file name, data set title, data set description
def createAnnotations(s=None,autoLoadGeneModels=None,lsts=None):
    annots=[]
    if s:
        toks = s.split('\n')
        tokss = map(lambda x:x.split('\t'),toks)
#        print s
    elif lsts:
        tokss=lsts
    else:
        raise IllegalArgumentException("Need s or lsts")
    i = 0
    for toks in tokss:
        s = QuickloadAnnotStyle()
        if i == autoLoadGeneModels:
            s.forceSet('load_hint','Whole Sequence')
            on_first = None
        u = None
        if len(toks)==4:
            u = toks[3]
        a = Annotations(fn=toks[0],
                        title=toks[1],
                        description=toks[2],
                        url=u,
                        style=s)
        annots.append(a)
        i = i + 1
    return annots

def makeSamplesForStudy(lsts=None,
                        folder=None,
                        deploy_dir=None,
                        url=None):
    samples = []
    for lst in lsts:
        f = lst[0]
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        if len(lst)>4:
            url=lst[4]
        if f.endswith('.bam'):
            style=QuickloadReadStyle(foreground=foreground,
                                     background=background)
        elif f.endswith('.bedgraph.gz'):
            style=QuickloadCoverageStyle(foreground=foreground,
                                         background=background)
        elif f.endswith('bed.gz'):
            style=QuickloadRegionsStyle(foreground=foreground,
                                        background=background)
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        samples.append(s)
    return samples
                          
def makeSamplesForRNASeq(lsts=None,
                         folder=None,
                         deploy_dir=None,
                         url=None,
                         all=False):
    samples = []
    on_first = True
    for lst in lsts:
        f = lst[0]
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        if len(lst)>4:
            url = lst[4]
        # alignments
        style=QuickloadReadStyle(foreground=foreground,
                                 background=background)
        descr = "alignments for reads that mapped once onto the genome"
        fn = deploy_dir + os.sep + f + '.sm.bam'
        title = folder + " / SM / Reads / " + sample + ", SM alignments"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first and url:
            s.setUrl(url)
        samples.append(s)
        descr = "alignments for reads that mapped to multiple places in the genome"
        fn = deploy_dir + os.sep + f + '.mm.bam'
        title = folder + " / MM / Reads / " + sample + ", MM alignments"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first:
            s.setUrl(url)
        samples.append(s)
        if all:
            descr = "alignments for ALL reads (multi- and single-mapping)"
            fn = deploy_dir + os.sep + f + '.bam'
            title = folder + " / ALL / Reads / " + sample + ", ALL alignments"
            s = Annotations(fn=fn,description=descr,title=title,style=style)
            if on_first and url:
                s.setUrl(url)
            samples.append(s)
        style=QuickloadCoverageStyle(foreground=foreground
                                     ,background=background)
        descr = "coverage graph for reads that mapped once onto the genome"
        fn = deploy_dir + os.sep + f + '.sm.bedgraph.gz'
        title = folder + " / SM / Graph / " + sample + ", SM coverage"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first and url:
            s.setUrl(url)
        samples.append(s)
        descr = "coverage graph for reads that mapped to multiple places in the genome"  
        fn = deploy_dir + os.sep + f + '.mm.bedgraph.gz'
        title = folder + " / MM / Graph / " + sample + ", MM coverage"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first and url:
            s.setUrl(url)
        samples.append(s)
        if all:
            descr = "coverage graph for ALL reads (multi- and single-mapping)"
            fn = deploy_dir + os.sep + f + '.bedgraph.gz'
            title = folder + " / ALL / Graph / " + sample + ", ALL coverage"
            s = Annotations(fn=fn,description=descr,title=title,style=style)
            samples.append(s)
        style=QuickloadJunctionStyle(foreground=foreground,
                                     background=background)
        descr = "Junctions predicted by TopHat"
        fn = deploy_dir + os.sep + f + '.bed.gz'
        title = folder + " / TH Juncs / " + sample + ", tophat junctions"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first:
            s.setUrl(url)
            on_first = True
        samples.append(s)

        descr = "Junctions predicted by Loraine Lab FindJunctions, single-mapping reads only"
        fn = deploy_dir + os.sep + f + '.sm.FJ.bed.gz'
        title = folder + " / FJ Juncs / " + sample + ", LL SM junctions"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if on_first and url:
            s.setUrl(url)
            on_first = True
        samples.append(s)

    return samples

# no SM or MM 
def makeSamplesForRNASeqSimple(lsts=None,
                            folder=None,
                            deploy_dir=None,
                            url=None):
    samples = []
    for lst in lsts:
        f = lst[0]
        sample=lst[1]
        foreground=lst[2]
        background=lst[3]
        if len(lst)>4:
            url = lst[4]
        # alignments
        style=QuickloadReadStyle(foreground=foreground,
                                 background=background)
        descr = "read alignments"
        fn = deploy_dir + os.sep + f + '.bam'
        title = folder + " / Reads / " + sample + " alignments"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if url:
            s.setUrl(url)
        samples.append(s)
        style=QuickloadCoverageStyle(foreground=foreground,background=background)
        descr = "coverage graph"
        fn = deploy_dir + os.sep + f + '.bedgraph.gz'
        title = folder + " / Graph / " + sample + ", coverage"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if url:
            s.setUrl(url)
        samples.append(s)
        style=QuickloadJunctionStyle(foreground=foreground,
                                     background=background)
        descr = "Junctions predicted by TopHat"
        fn = deploy_dir + os.sep + f + '.bed.gz'
        title = folder + " / Junctions / " + sample + ", tophat junctions"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if url:
            s.setUrl(url)
        samples.append(s)
        descr = "Junctions predicted by Loraine Lab FindJunctions, single-mapping reads only"
        fn = deploy_dir + os.sep + f + '.FJ.bed.gz'
        title = folder + " / FJ Juncs / " + sample + ", LL junctions"
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        if url:
            s.setUrl(url)
        samples.append(s)
    return samples

