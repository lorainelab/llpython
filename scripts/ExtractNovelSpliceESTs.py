#!/usr/bin/env python

"""
Reads any set of ests and with a gene models bed file this filters out all the novel splicing event ests.

The approach:

Using the gene models create a dictionary of all the introns like so:

{key:val}

where key = chrom:start-stop, start, stop are the start and end positions of the intron, and val is something. 

Next, create a dictionary of all the junctions in the shape of:

{'est bed line':["chr:start-stop","chr:start-stop"...]} where each "chr:start-stop" is an intron in the line

Finally, I'll loop through all the ests and make a try statement
for key in junctions.keys():
	try:
		for intron in junction[key]:
			x = genes[intron]
		write to found output
	except KeyError:
		write to novel output
	
"""
from optparse import OptionParser
from Utils.General import readfile
import sys,os,re
    
def parseGenesFile(file):
	"""
	Reads a genes.bed file and creates a dictionary
        {"chr:start-stop":"bed line"}
	"""
	ret = {}
	for line in file.readlines():
		if line.startswith("track"):
			continue
		#get all blocks starts and stops
		#create a dictionary entry for all of them
		dat = line.split('\t')
		chrom = dat[0]
		chromStart = int(dat[1])
		sizes = dat[10].split(',')
		starts = dat[11].split(',')
		if dat[10].endswith(','):
			sizes = sizes[:-1]
			starts = starts[:-1]
		
		starts = map(lambda x:int(x)+chromStart,starts)
		sizes = map(lambda x:int(x), sizes)
		
		sortBlocks(starts,sizes)	
	
		for i in range(len(starts)-1):
			st1 = starts[i]
			sz1 = sizes[i]
			st2 = starts[i+1]
			
			ret[chrom+":"+str(st1 + sz1)+"-"+str(st2)] = line
	
	
	return ret
	
def parseESTsFile(file,minIntron=20,minCount=5):
	"""
	Reads a ests.bed file and creates a dictionary, where bed lines are keys and values are list of
          introns they contain.
        Checks the score field. If the value is greater than 0, then assumes that the score
        respresents the number of ESTs that support a given junction, or set of junctions, represented in the file.
        In that case, only count an EST line when the score is >= minCount. This is useful when working with
        TopHat junction files.

        ex)
        
        {"est bed line":["chr:start-stop","chr:start-stop"...]} each "chr:start-stop" is a putative intron
	"""
	ret = {}
	for line in file.readlines():
		if line.startswith("track"):
			continue
		#get all blocks starts and stops
		#create a dictionary entry for all of them
		dat = line.split('\t')
                score = int(dat[4])
                if score > 0:
                    if score < minCount:
                        continue # skip this line
		chrom = dat[0]
		chromStart = int(dat[1])
		sizes = dat[10].split(',')
		starts = dat[11].split(',')
		if dat[10].endswith(','):
			sizes = sizes[:-1]
			starts = starts[:-1]
		starts = map(lambda x:int(x)+chromStart,starts)
		sizes = map(lambda x:int(x), sizes)
		
		sortBlocks(starts,sizes)	
		introns = []
		for i in range(len(starts)-1):
			st1 = starts[i]
			sz1 = sizes[i]
			st2 = starts[i+1]
                        if abs(st2-st1+sz1)>=minIntron:
                            introns.append(chrom+":"+str(st1 + sz1)+"-"+str(st2))
			
			else:
                            continue
		ret[line] = introns	
	return ret

    

def sortBlocks(starts,lengths):
    """
    Function: ensures the block starts read from a bed file are in order with a simple bubble sort.
              This needs to be checked for order to make introns
    Args: starts - list of block starts
          lengths - list of block lengths (these need to be moved in order with starts
    Returns: None, all sorting is done in place
    """

    for i in range(0,len(starts)-1):
        swapMade = False#short circuiting. if no swaps, we can quit
        for j in range(0,len(starts)-1-i):
            if starts[j+1]<starts[j]:
                tempS = starts[j]
                tempL = lengths[j]
                starts[j] = starts[j+1]
                lengths[j] = lengths[j+1]
                starts[j+1] = tempS
                lengths[j+1] = tempL
                swapMade = True

        if not swapMade: break


def doIt(options,args):
    	novel = options.novelJuncs
	genes = options.genes
	old = options.oldJuncs
	ests = options.ests
        newgenes = options.newgenes
	
	if novel != None:
		novelfh = open(novel,'w')
	
	if genes == None:
		sys.stderr.write("--genes must be provided!\n")
		sys.exit()
	else:
		genes = readfile(genes)
		
	if old == None:
		old = sys.stdout
	else:
		old = open(old, 'w')
	
	if ests == None:
		ests = sys.stdin
	else:
		ests = readfile(ests)
	
	genesDictionary = parseGenesFile(genes)
	estsDictionary = parseESTsFile(ests)
	
	for key in estsDictionary:
		try:
			for intron in estsDictionary[key]:
				x = genesDictionary[intron]
			old.write(key)
		except KeyError:
			try:
				novelfh.write(key)
			except AttributeError:
				pass
		
        # now, annotate novel junctions with overlapping gene ids, if available
        """
        lined = {}
        loci = {}
        genes_d = {}
        for gene_line in genes.values():
            lined[gene_line]=1
        for gene_line in lined.keys():
            toks=gene_line.rstrip().split('\t')
            gene = [toks[0],toks[1],toks[2],toks[3].split('.')[0],toks[5]]
            if genes_d.has_key(gene[3]):
                genes_d[gene[3]].append(gene)
            else:
                genes_d[gene[3]]=[gene]
        for gene_id in genes_d.keys():
            genes = genes_d[gene_id]
            start = min(map(lambda x:x[1],genes))
            end = max(map(lambda x:x[1],genes))
            strand = 
        novelfh.close()
        old.close()
        fh = open(novel,'r')
        introns = []
        while 1:
            line = fh.readline()
            if not line:
                break
            toks = line.rstrip().split('\t')
            chrom = toks[0]
            start = toks
        """
        
if __name__ == '__main__':
	parser = OptionParser()
	
	parser.add_option("-n", "--novel-juncs", dest="novelJuncs", help="All the ests containing introns that are *not* already represented in the gene models file", metavar="FILE")
	parser.add_option("-g", "--genes", dest="genes", help=".bed file of the gene models to compare to", metavar="FILE")
        parser.add_option("-n", "--new-genes", dest="genes", help="File with junctions for new genes, or extensions to old genes", metavar="FILE")
	parser.add_option("-e", "--ests", dest="ests", help="ESTs that we want to analyze. Default reads from stdin", metavar="FILE")
	parser.add_option("-o", "--old-juncs", dest="oldJuncs", help="All the ests containing introns that are already represented in the gene models file. Default is stdout.", metavar="FILE")
	
	(options, args) = parser.parse_args()
	doIt(options,args)
