#!/usr/bin/env python

"Writes out qsub scripts for running TopHat in a cluster computing environment that uses PBS to schedule jobs."

import os,sys,optparse

def getFastqNames(dir='/gpfs/fs3/scratch/heat_drought_2010/fastq'):
    "Get the list of Fastq files we want to align. TopHat output directories will be named for the FASTQ file prefixes, e.g., for Control.fastq output will be put into a new directory named Control. I recommend using names like C1, C2, and so on so that it will easier to recognize output files further downstream."
    files = os.listdir(dir)
    newfiles = []
    for file in files:
        if file.endswith('fastq'):
            newfiles.append(file)
    return newfiles

def writeSubmissionScripts(files=None,run_name=None,
                           fastq_dir=None,
                           base_dir='/gpfs/fs3/scratch/heat_drought_2010',
                           processors=8,
                           index='~/bowtieindex/A_thaliana_Jun_2009'):
    "Write a job submission script for each of the given fastq files. Job submission scripts will be named for files they process, e.g., control.sh will process control.fastq."
    qsub_script = run_name + '.sh'
    qsub_fh = open(qsub_script,'w')
    qsub_fh.write('#!/bin/bash\n')
    for file in files:
        filename = file.split('/')[-1]
        prefix = '.'.join(file.split('.')[0:-1])
        job_submission_script = prefix+'.sh'
        fh = open(job_submission_script,'w')
        fh.write('#!/bin/bash\n')
        fh.write("#PBS -N %s.%s\n"%(prefix,run_name))
        fh.write("#PBS -l nodes=1:ppn=4\n")
        fh.write("#PBS -l pvmem=2000mb\n")
        fh.write("#PBS -l walltime=20:00:00\n")
        fh.write("cd %s\n"%base_dir)
        fh.write("tophat -G %s -p 4 --solexa1.3-quals -I 2000"% G)
        fh.write(" -o %s"%run_name+'/'+prefix)
        fh.write(" %s %s/%s\n"%(index,fastq_dir,filename))
        fh.close()
        os.chmod(job_submission_script,0744)
        qsub_fh.write('qsub -o %s.out -e %s.err  %s\n'%(prefix,prefix,job_submission_script))
    qsub_fh.close()
    os.chmod(qsub_script,0744)
    

def main(options,args):
    base_dir = options.base_dir
    fastq_dir = options.fastq_dir
    run_name = options.run_name
    files = getFastqNames(dir=fastq_dir)
    if len(files)==0:
        raise Error("Found no .fastq files in %s"%fastq_dir)
    writeSubmissionScripts(files=files,
                           run_name=run_name,
                           base_dir=base_dir,
                           fastq_dir=fastq_dir)

                           
if __name__ == '__main__':
    usage = "%prog [options]\n\nex) %prog -b /gpfs/fs3/scratch/heat_drought_2010 -f /gpfs/fs3/scratch/heat_drought_2010/fastq -r tophat_1"
    parser = optparse.OptionParser(usage)
    parser.add_option("-b","--base_dir",
                      dest="base_dir",
                      help="Base directory where TopHat output directories will reside [required]",
                      default=None)
    parser.add_option("-f","--fastq_dir",
                      dest="fastq_dir",
                      help="Directory containing fastq files to align [required]",
                      default=None)
    parser.add_option("-r","--run_name",
                      dest="run_name",
                      help="Name of the tophat run. Directory used for tophat output directories [required]",
                      default=None)
    parser.add_option("-b","--bowtie_index",
                      dest=index,
                      help="Path to the bowtie index to use for this run [required, default is a_thaliana]",
                      default="a_thaliana")
    (options,args)=parser.parse_args()
    if not options.index:
        parser.error("Please specify bowtie index.")
    if not options.base_dir:
        parser.error("Please specify base directory.")
    if not options.fastq_dir:
        parser.error("Please specify directory with fastq files.")
    if not options.run_name:
        parser.error("Please specify tophat run name, no whitespace please.")
    main(options,args)
    
                     
