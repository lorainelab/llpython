USAGE = """

Use SamTools to get Sam lines for alignments that overlap RI difference regions and have 75M CIGAR code (perfect score)

Make a "Fake" Bedline from these groups of SAM lines with start = minStart and end= maxEnd and score = numreads within difference region of bed line.

Suggested path from this script: Combine these new "Fake" bedlines with TopHat's junctions.bed and then go through the ArabiTag/v3.0/src/TopHatJunctions pipeline (see README in that directory)

Notes:

	SamTools MUST be installed to run this script

FAQ: 
	Where do I get the RI difference Regions - It is the CSV output from ArabiTagOnline (Only select [x]RI as the AS type.
		CAUTION! - As of 3/16/10 @ 11:45am, ArabiTag CSV output is broken and won't select based on AS type...
"""

from optparse import OptionParser
import sys, os, commands, re

def parseBAM_RI_Support(fn, diffRegions, XXM):
	"""
	Function: Extracts all BAM lines from fn that have XXM CIGAR code and are within diffRegions, then combines them to make a single bed line
	Args: fn - BAM filename  
		diffRegions - list of all the diffRegions we want ESTs within (returned from getAllDiffRegions)
	Returns: a list of bed lines
	"""	
	ret = []
	
	for region in diffRegions:
		overlaps = []
		samLines = commands.getoutput("samtools view %s %s" % (fn, region)).split('\n')
		for line in samLines:
			try:
				if line.split('\t')[5] == XXM:
					overlaps.append(line)
			except IndexError:
				pass;
		
		a = reduceMultipleSAMtoBED(overlaps)
		if a:
			ret.append(a)
		
	return ret
		
def reduceMultipleSAMtoBED(samLines):
	"""
	Function: Turns a list of SAM lines to a single bed line with start = minStart and end= maxEnd and score = number of SamLines
	Args: samLines: a list with structure [samDR1,samDR1,...]
	Returns: A Single bed line of Bed Lines
	
	NOTE:
	
	The Bedline will automatically be named after the last samLine we use
	start = minStart and end= maxEnd and score = number of SamLines
	
	e.x.  (S = Sam input, B = Bed output)
	
		SSSS SSS    SSS SSSSSS  SSS
		       SSS  S    SSS
		BBBBBBBBBBBBBBBBBBBBBBBBBBB	
	"""
	if len(samLines) == 0:
		return None
	minStart = None
	maxEnd = None 
	for line in samLines:
		samFields = line.strip().split('\t')
		samFlag = int(samFields[1])
		# Only create a BED entry if the read was aligned
		if (samFlag & 0x0004):
			continue
		chrom = samFields[2]
		start = int(samFields[3])-1
		length = len(samFields[9])
		end = start + length
		name = samFields[0]	
		strand = getStrand(samFlag)
		
		if minStart == None:
			minStart = start
			maxEnd = end
		else:
			if start < minStart:
				minStart = start
			if end > maxEnd:
				maxEnd = end
	return "%s\t%i\t%i\t%s\t%s\t%s\t%i\t%i\t%i\t%i\t%i,\t%i," % (chrom,minStart,maxEnd,name,len(samLines),strand,minStart,minStart,0,1,maxEnd-minStart,0)



def getStrand(samFlag):
	"""Get a string representation of the strand from the samFlag"""
	strand = "+"
	if (samFlag & (0x10)):	# minus strand if true.
		strand = "-"		
	return strand
	
def getAllDiffRegions(fn, FLANK=20):
	"""
	Function: Creates a list of all the difference regions in fn in the format ["chr_:start-stop",...]
	Args: fn - Difference 
		  FLANK - the amount of space we need each EST to strech  within the difference region
	Returns: The list of difference regions where each Dr is a single string "chr_:start-stop"
	
	The Flank Argument:
		Used when we want the ESTs we'll parse out in parseSAM to cover at least FLANK number of basepairs of the difference region.
		e.x.   (X = 10bp and FLANK is 20bp)
		
			XXXXXXXXXXXX     <--diffRegion 
			XX   XX   XXXX 	 <--Supports
		XXXXX    X        	 <--Doesn't Support
		
	fn is expected to be a .csv and have the format:
	as_id,gene_absent,gene_absent_ests,gene_present,gene_present_ests,chr,gdiff_start,gdiff_end,as_type,p
	
	0	  1			  2				   3			4				  5    6          7         8        9

	
	"""
	fh = open(fn,'r')
	fh.readline()#gets rid of header!! There Better be a Header if you got it from ArabiTagOnline
	
	ret = []
	
	for line in fh.readlines():
		dat = line.split(',')
		chrom = dat[5]
		start = int(dat[6])+FLANK
		end = int(dat[7])-FLANK
		ret.append("%s:%i-%i" % (chrom, start, end))
		
	return ret
			
def uniq(items):
	"""
	returns only the unique items in the list
	"""
	# Not order preserving
	keys = {}
	for e in items:
		keys[e] = 1
	return keys.keys()
 
def groupRanges(diffRegions):
	"""
	Given a list of all my diffRegions, I'm going to split them by chromosome, 
	then I'm going to make the groups of the ranges using make groups,
	then return the result
	"""
	split = splitByChr(diffRegions)
	ret = []
	for chrom in split.keys():
		ret.extend(makeGroups(split[chrom]))
		
	return ret
	
	
def makeGroups(diffRegions):
	"""
	Function: Given a list of of diffRegions, group those that overlap.
	Returns: A list of the new groupped and squished together regions
			
	Algorithm:
		1) sort genes in ascending order based on start then stop
		2) for i in range(len(genes)-1):
				add genes[i] to current loop
				if don't overlap with next:
					start over
				else:
					continue, the next guy needs to be added to curGroup
	"""
	ret = []
	all = []
	chrom = diffRegions[0][:4]
	coordRe = re.compile("chr.:(\d*)-(\d*)")
	
	for reg in diffRegions:
		(start,stop) = coordRe.match(reg).groups()
		start = int(start)
		stop = int(stop)
		all.append((start,stop))
		
	all.sort()#python handles tuples perfectly! (after I added __cmp__ to Gene
	
	curGroup = []
	for i in range(len(all)-1):
		curGroup.append(all[i])
		if not overlaps(all[i], all[i+1]):
			#work on curGroup compress group
			start = curGroup[0][0]
			end = curGroup[0][1]
			for cur in curGroup:
				start = min(start,cur[0])
				end = max(end,cur[1])
			ret.append("%s:%i-%i" % (chrom, start, end))
			curGroup = []
			
	return ret
				
def splitByChr(diffRegions):
	"""
	Splits genes into a dictionary of structure
	{chr:[gene1, gene2]}
	"""
	ret = {}
	for line in diffRegions:
		seq = line[:4]
		if seq in ret.keys():
			ret[seq].append(line)
		else:
			ret[seq] = [line]
	return ret
	
def overlaps(reg1, reg2):
	"""
	Function: Given two genes, check to see if they overlap.
	Args: gene1, gene2 - the two Gene objects to compare
		  cds - when true, look at the CDS for overlap, when false, look at genomic coordinates
	Returns: Boolean - True for overlaps, False for don't overlap
	"""
	(s1, e1) = reg1
	(s2, e2) = reg2
	return not ( s2 >= e1 or s1 >= e2 )
	
	
if __name__ == '__main__':
	parser = OptionParser(USAGE)
	
	parser.add_option("-b", "--bam", dest="bam", help="The BAM file containing the alignments. Note: This BAM file must have it's index with it!", metavar="STRING", default=None)
	
	parser.add_option("-r", "--ri", dest="ri", help="The Retained Intron File. Note: This MUST only contain RI events!!", metavar="STRING", default=None)
	
	parser.add_option("-m", dest="m", help="The number for field 6 of the BAM file to be match to be extracted. Default is 75M", metavar="STRING", default="75M")
	
	parser.add_option("-f", "--flank", dest="flank", help="The number of basepairs a sequence needs to cover over the difference region. Default is 20bp", metavar="INT", default=20, type='int')
	
	(options, args) = parser.parse_args()
	
	bamFile = options.bam
	if bamFile == None:
		sys.stderr.write("BAM file is required.\n")
		sys.exit(1)

	retainedInt = options.ri
	if retainedInt == None:
		sys.stderr.write("Retained intron file is required.\n")
		sys.exit(1)
	
	match = options.m+"M"
	flank = options.flank
	
	diffs = getAllDiffRegions(retainedInt,flank)
	diffs = groupRanges(diffs)
	
	bedLines = parseBAM_RI_Support(bamFile, diffs, match)
		
	bedLines = uniq(bedLines)
	
	sys.stdout.write("\n".join(bedLines))
