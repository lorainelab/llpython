#! /usr/bin/env python

"""A script that converts TAIR GFF3 to bed format files"""

from optparse import OptionParser
import Utils.General as utils
import Mapping.FeatureFactory as factory
import Mapping.Parser.Bed as b
import re,sys,os

def writeFeats(fn,feats_d):
    "Write feature objects to file called fn."
    fh = open(fn,'w')
    feats = feats_d.values()
    for feat in feats:
        line = b.feat2bed(feat)
        fh.write(line)
    fh.close()
    
def gff2beds(fn=None,
             fh=None,
             prefix=None):
    "Write bed files for various annotation types, named with prefix, e.g., TAIR10_mRNA.bed."
    if fn:
        in_fh = utils.readfile(fn)
    elif fh:
        in_fh = fh
    else:
        raise ValueError("Need a file or a file handle for reading")
    d = readTairGff3(in_fh=in_fh)
    both = assemblemRNAs(d)
    writeFeats(prefix+'_mRNA.bed',both[0])
    writeFeats(prefix+'_TE_gene.bed',both[1])
    writeFeats(prefix+'_snoRNA.bed',assembleSnoRNAs(d))
    writeFeats(prefix+'_rRNA.bed',assemblerRNAs(d))
    writeFeats(prefix+'_tRNA.bed',assembletRNAs(d))
    writeFeats(prefix+'_ncRNA.bed',assembleNcRNAs(d))
    writeFeats(prefix+'_miRNA.bed',assembleMiRNAs(d))
    writeFeats(prefix+'_pseudogene.bed',assemblePseudoGenes(d))
    writeFeats(prefix+'_TE.bed',assembleTransposableElements(d))
    writeFeats(prefix+'_snRNA.bed',assembleSnRNAs(d))
    return d
    
def parseKeyVals(extra_feat):
    "Parse extra feature fields from GFF. Handle weird case where some features have multiple parents."
    if extra_feat.endswith(';'):
        vals = extra_feat.split(';')[:-1]
    else:
        vals = extra_feat.split(';')
    key_vals = {}
    for item in vals:
        pair=item.split('=')
        if not len(pair)==2:
            raise ValueError("GFF3 line has weird extra feature value: %s."%extra_feat)
        if pair[0]=='Parent':
            parents = pair[1].split(',')
            if len(parents)>1:
                parents = filter(lambda x:not x.endswith('-Protein'),parents)
                if len(parents)>1:
                    raise ValueError("GFF3 line has weird extra parents: %s."%extra_feat)
                else:
                    pair[1]=parents[0]
        if pair[0]=='Derives_from':
            pair[0]='Parent'
        key_vals[pair[0]]=pair[1]
    return key_vals

chr_reg = re.compile(r'(^C)(hr[MC12345])')

def fixChr(tok):
    "Fix chromosome name - change to lower-case from upper-case"
    seqname = chr_reg.sub(r'c\2',tok)
    return seqname

def readTairGff3(in_fh=None):
    "Read TAIR GFF3 and return feature objects representing genes, ncRNAs, transposable elements, etc. Fix chromosome names to use lower-case c, e.g., change Chr1 to chr1."
    d = {}
    kids = {}
    while 1:
        line = in_fh.readline()
        if not line:
            if not in_fh==sys.stdin:
                in_fh.close()
            break
        toks = line.strip().split('\t')
        typ = toks[2]
        if typ in ['chromosome','five_prime_UTR','three_prime_UTR']:
            continue
        key_vals = parseKeyVals(toks[8])
        toks[0]=fixChr(toks[0])
        f = factory.make_feat_from_gff_fields(vals=toks,
                                              cfeat=(not typ in ['protein','exon','CDS',
                                                                 'transposon_fragment',
                                                                 'pseudogenic_exon']))
        f.setKeyVals(key_vals)
        if key_vals.has_key('ID'):
            f.setDisplayId(key_vals['ID'])
        if not d.has_key(typ):
            d[typ]=[]
        d[typ].append(f)
    return d

def makeDict(feats):
    "Make a dictionary where keys are feature display ids and values are feature objects"
    d = {}
    for feat in feats:
        d_id = feat.getDisplayId()
        if d.has_key(d_id):
            raise ValueError("Can't store feat in dictionary - duplicates: %s"%d_id)
        d[d_id]=feat
    return d

def addToParent(parents_d,children,ignore_key_error=False):
    "Add features in children as child feats to parents in parents. Child Parent key value should be represented in parents_d, unless ignore_key_error is False."
    for child in children:
        parent_id = child.getVal('Parent')
        try:
            f = parents_d[parent_id]
            f.addFeat(child)
        except KeyError:
            if ignore_key_error:
                continue
            else:
                parents_d[parent_id]

def assembleSnoRNAs(d):
    "Make gene models representing snoRNA"
    exons = d['exon']
    transcripts=d['snoRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assemblerRNAs(d):
    "Make gene models representing rRNA"
    exons = d['exon']
    transcripts=d['rRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assembletRNAs(d):
    "Make gene models representing tRNA"
    exons = d['exon']
    transcripts=d['tRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assembleSnRNAs(d):
    "Make gene models representing snRNA"
    exons = d['exon']
    transcripts=d['snRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assembleMiRNAs(d):
    "Make gene models representing miRNA"
    exons = d['exon']
    transcripts=d['miRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assembleNcRNAs(d):
    "Make gene models representing ncRNA"
    exons = d['exon']
    transcripts=d['ncRNA']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    return newd

def assemblemRNAs(d):
    "Make gene models representing mRNA and transposable element mRNA."
    exons = d['exon']
    transcripts=d['mRNA']
    cdss = d['CDS']
    newd = makeDict(transcripts)
    addToParent(newd,exons,ignore_key_error=True)
    addToParent(newd,cdss)
    te_genes = d['transposable_element_gene']
    te_d = makeDict(te_genes)
    newd2={}
    for mRNA_id in newd.keys():
        parent_id = mRNA_id.split('.')[0]
        if te_d.has_key(parent_id):
            newd2[mRNA_id]=newd[mRNA_id]
            del(newd[mRNA_id])
    return (newd,newd2)

def assembleTransposableElements(d):
    "Make feature objects representing transposable elements"
    exons = d['transposon_fragment']
    for exon in exons:
        exon.setFeatType('exon')
    transcripts=d['transposable_element']
    newd = makeDict(transcripts)
    addToParent(newd,exons)
    return newd

def assemblePseudoGenes(d):
    "Make feature objects representing pseudogenes"
    exons = d['pseudogenic_exon']
    for exon in exons:
        exon.setFeatType('exon')
    transcripts=d['pseudogenic_transcript']
    newd = makeDict(transcripts)
    addToParent(newd,exons)
    return newd


if __name__ == '__main__':
    usage = "%prog [options] [GFF file from TAIR]\n  If name of GFF file is not supplied, we'll try to read it from stdin. \n  It can be compressed (e.g., we can read TAIR10_GFF3_genes_transposons.gff.gz)"
    parser = OptionParser(usage)
    parser.add_option("-p","--prefix",dest='prefix',default=None,help='prefix for bed files, e.g., if -p is TAIR10, TAIR10_mRNA.bed, TAIR10_miRNA.bed, etc. will be created.')
    (options, args) = parser.parse_args()
    if not options.prefix:
        sys.stderr.write("Can't execute! Option -p|--prefix is required.\n\n")
        parser.print_help()
        exit(-1)
    else:
        prefix = options.prefix
    if len(args)==0:
        sys.stderr.write("Reading GFF from TAIR from stdin\n")
        in_fh = sys.stdin
    else:
        sys.stderr.write("Reading GFF from TAIR from: %s\n"%args[0])
        in_fh = utils.readfile(args[0])
    gff2beds(fh=in_fh,prefix=prefix)
