"""Functions for getting information about genes, transcripts, and other genomic features."""
import sys

"""Functions for computing on gene structures, especially junction mining."""

def count_genes_junctions(counts):
    """
    Function:
    Returns :
    Args    :
    """
    n = 0
    for e1e in counts.keys():
        n = n + len(counts[e1e].keys())
    return n

def get_genes_junctions(genes,prev=None,all=1):
    """
    Function:
    Returns :
    Args    :
    """
    if not prev:
        prev = {}
    for g in genes:
        prev = get_gene_junctions(g,prev=prev,all=all)
    return prev

def get_gene_junctions(gene,prev=None,all=1):
    """
    Function:
    Returns :
    Args    :
    """
    txs = gene.feats(type='transcript')
    if not prev:
        prev = {}
    for tx in txs:
        jlst = get_transcript_junctions(tx,all=all)
        for j in jlst:
            e1e = j[0].start()+j[0].length()
            e2s = j[1].start()
            if not prev.has_key(e1e):
                prev[e1e]={}
            if not prev[e1e].has_key(e2s):
                prev[e1e][e2s]=[]
            if j[2]==0:
                prev[e1e][e2s].append(tx)
    return prev
        
def get_transcript_junctions(feature,all=1):
    """
    Function:
    Returns :
    Args    :
    """
    jlst = []
    exons = feature.sorted_feats(type='exon')
    if len(exons)==0:
        return jlst
    i = 0
    for exon1 in exons[:-1]:
        i = i + 1
        j = 0
        for exon2 in exons[i:]:
            if not all and j > 0:
                break
            jlst.append((exon1,exon2,j))
            j = j + 1

    return jlst

def get_diff_profile(feats):
    """
    Function: Create a DiffProfile 
    Returns : a DiffProfile object
    Args    : a list of features with 'exon' sub-features
    """
    pass

def get_cassette_introns(feats,safe=0):
    """
    Function: Get cassette introns
    Returns : 
    Args    : two feature.DNASeqFeature objects

    Don't run this unless both feats:
      are on the same strand
      are on the same annotated sequence
      overlap
    """
    a = feat[0]
    b = feat[1]
    abits = get_bits(a)
    bbits = get_bits(b)
    abbits = add(a,b)
    saddles = find_saddles(abbits)
    saddles = make_saddle_feats(saddles)
    return saddles

def get_bits(feat):
    sf = feat.start()
    leng = feat.length()
    stop_at = sf + leng
    bits = []
    i = sf
    while i < stop_at:
        bits.append([i,0])
        i = i + 1
    exons = feat.feats(type='exon')
    for exon in exons:
        s = exon.start()
        leng =exon.length()
        stop_at = s + leng
        i = s
        while i < stop_at:
            bits[i-sf][1]=1
            i = i + 1
    return bits


def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass

