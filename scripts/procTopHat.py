#!/usr/bin/env python

"""Script and functions for processing tophat RNA-Seq alignments."""

import sys,os,optparse,re,time

def fileExists(fn):
    "Find out if a file exists by attempting to open it."
    try:
        fh = open(fn,'rb')
        fh.close()
        return True
    except IOError:
        return False

def makeBamFile(out_prefix=None,
                        bam_file = None,
                        force=False,
                        verbose=False):
    "Copy accepted hits to new file name and make an index"
    cmfile = out_prefix+'.bam'
    if not force and fileExists(cmfile):
        if verbose:
            sys.stderr.write("File exists: %s. No need to continue.\n"%cmfile)
    else:
        cmd = 'cp %s %s'%(bam_file,cmfile)
        run(cmd,verbose=verbose)
    cmd = 'samtools index %s.bam'%out_prefix
    run(cmd,verbose=verbose)
    
def makeJunctionsBedFile(out_prefix=None,verbose=False,force=False):
    "Re-write and rename junctions file made by tophat."
    if not fileExists("junctions.bed"):
        raise ValueError("Can't make new junctions file without junctions.bed")
    out_fn = out_prefix+'.bed.gz'
    tabix_fn = out_prefix+'.tbi'
    if force or not fileExists(tabix_fn) or not fileExists(out_fn):
        cmd = "grep -v track junctions.bed | sort -k1,1 -k2,2n | bgzip > %s"%out_fn
        run2(cmd,verbose=verbose)
        cmd = 'tabix -s 1 -b 2 -e 3 -f -0 %s'%out_fn
        run2(cmd,verbose=verbose)
    else:
        if verbose:
            sys.stderr.write("No need to create %s or %s. Both exist.\n"%(out_fn,tabix_fn))

def makeCoverageWigFile(out_prefix=None,verbose=False,force=False):
    bfn = out_prefix+'.bam'
    if not fileExists(bfn):
        raise ValueError("Can't make coverage file without %s"%bfn)
    out_fn = out_prefix+'.bedgraph.gz'
    tabix_fn=out_fn+'.tbi'
    if force or not fileExists(out_fn) or not fileExists(tabix_fn):
        if verbose:
            sys.stderr.write("Need to make: %s and/or %s. Can't find one or both.\n"%(out_fn,tabix_fn))
        cmd='genomeCoverageBed -ibam %s -split -bg | sort -k1,1 -k2,2n | bgzip > %s'%(bfn,out_fn)
        run(cmd,verbose=verbose)
        cmd = 'tabix -s 1 -b 2 -e 3 -0 -f %s'%out_fn
        run(cmd,verbose=verbose)
    else:
        if verbose:
            sys.stderr.write("No need to make: %s or %s. Files exist\n"%(out_fn,tabix_fn))

def deliverFiles(out_prefix=None,
                 dest_dir=None,
                 verbose=False,
                 move=True):
    if not(os.path.exists(dest_dir)):
        raise ValueError("Cant' deliver files to non-existant directory: %s"%dest_dir)
    files_to_copy = [out_prefix + '.bam', out_prefix + '.bam.bai',
                     out_prefix + '.bed.gz',
                     out_prefix + '.bed.gz.tbi',
                     out_prefix + '.bedgraph.gz',
                     out_prefix + '.bedgraph.gz.tbi']                     
    for fname in files_to_copy:
        if move:
            cmd = 'mv %s %s/.'%(fname,dest_dir)
        else:
            cmd = 'cp %s %s/.'%(fname,dest_dir)
        run(cmd,verbose=verbose)

def run(cmd,verbose=False):
    cwd = os.getcwd()
    if verbose:
        sys.stderr.write("running: %s in %s\n"%(cmd,cwd))
    exit_status = os.system(cmd)
    if exit_status != 0:
        if verbose:
            raise OSError("Command failed. I quit.")
        else:
            raise OSError("Command failed: %s"%cmd)

def main(options,args):
    force = options.force
    verbose = options.verbose
    dest_dir = options.dest_dir
    bam_file = options.bam_file
    move = options.move
    write_qsub_script = options.write_qsub_script
    run_name = options.run_name
    if not bam_file:
        raise ValueError("Need path to BAM format file, e.g., ./SAMPLE/accepted_hits.bam. I quit.")
    if not fileExists(bam_file):
        raise ValueError("BAM format file doesn't exist: %s. I quit."%bam_file)
    original_cwd = os.getcwd()
    toks = bam_file.split(os.sep)
    work_d = os.sep.join(toks[:-1])
    # if sample name not given, get it from the bam_file name
    # which likely would be something like
    # ./HotK2T2/accepted_hits.bam
    out_prefix = options.out_prefix
    if not out_prefix:
        out_prefix = toks[-2]
    if write_qsub_script:
        if not run_name:
            raise ValueError("Writing qsub scripts requires run_name")
        jobname = "%s%s"%(run_name,out_prefix)
        job_submission_script = "proc_"+out_prefix+".sh"
        fh = open(job_submission_script,'w')
        fh.write("#!/bin/bash\n")
        fh.write("#PBS -N %s\n"%jobname)
        fh.write("#PBS -l nodes=1:ppn=1\n")
        fh.write("#PBS -l vmem=8000mb\n")
        fh.write("#PBS -l walltime=20:00:00\n")
        fh.write("cd $PBS_O_WORKDIR\n")
        cmd = "procTopHat.py"
        if dest_dir:
            cmd = cmd + " --delivery=%s"%dest_dir
        cmd = cmd + " --out_prefix=%s"%out_prefix
        cmd = cmd + " --bam_file=%s"%bam_file
        if force:
            cmd = cmd + " --force"
        if verbose:
            cmd = cmd + " --verbose"
        if move:
            cmd = cmd + " --move"
        fh.write(cmd+"\n")
        fh.close()
        os.chmod(job_submission_script,0744)
        qsub_script = 'proc_%s.sh'%run_name
        if not fileExists(qsub_script):
            fh = open(qsub_script,'w')
            fh.write('#!/bin/bash\n')
        else:
            fh = open(qsub_script,'a')
        fh.write('qsub -o proc_%s.out -e proc_%s.err %s\n'%(out_prefix,out_prefix,job_submission_script))
        fh.close()
        os.chmod(qsub_script,0744)
        sys.exit(0)
    # move into the directory with the files we want
    if dest_dir:
        if not os.path.isdir(dest_dir):
            os.mkdir(dest_dir)
    os.chdir(work_d)
    # start processing ...
    makeBamFile(out_prefix=out_prefix,bam_file=toks[-1],
                        force=force,verbose=verbose)
    makeJunctionsBedFile(out_prefix=out_prefix,
                         verbose=verbose,
                         force=force)
    makeCoverageWigFile(out_prefix=out_prefix,
                        verbose=verbose,
                        force=force)
    if dest_dir:
        deliverFiles(out_prefix=out_prefix,
                     dest_dir=original_cwd+os.sep+dest_dir,
                     verbose=verbose,move=move)
    os.chdir(original_cwd)


if __name__=='__main__':
    usage = "%prog [options]\n\nex) find . -name  accepted_hits.bam | xargs -I FILE %prog -b FILE"
    parser = optparse.OptionParser(usage)
    parser.add_option("-d","--delivery",
                      help="directory where output files will be moved [optional]",
                      dest="dest_dir",default=None)
    parser.add_option("-o","--out_prefix",dest="out_prefix",
                      help="prefix for mm and sm files to write [optional - gets it from -b|--bam_file if not given]")
    parser.add_option("-b","--bam_file",dest="bam_file",
                      help="Path to BAM format file containing alignments")
    parser.add_option("-f","--force",dest="force",action="store_true",help="Force re-writing of files if set.")
    parser.add_option("-v","--verbose",dest="verbose",action="store_true",
                      help="Print status messages to stderr")
    parser.add_option("-m","--move",dest="move",action="store_true",
                      help="Move files to DEST_DIR")
    parser.add_option("-q","--qsub_script",
                      dest="write_qsub_script",
                      help="Don't run, but write qsub scripts. Writes them to current working directory.",
                      action="store_true")
    parser.add_option("-r","--run_name",dest="run_name",
                      help="Run name for cluster (qsub) job(s). REQUIRED if -q|--qsub_script.")
    (options,args)=parser.parse_args()
    if options.verbose:
        sys.stderr.write("*********\nStarting:\n%s\n"%' '.join(sys.argv))
    if not options.bam_file:
        parser.error("Please specify bam_file")
    main(options,args)

                                  
