#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by Adam Baxter on 2011-02-07.
Copyright (c) 2011 Loraine Lab. All rights reserved.
"""

import sys, getopt, hashlib, MySQLdb, MySQLdb.cursors, os, gc, time
import Mapping.Parser.Bed as Bed, Mapping.FeatureModel as Feature

help_message = '''
The help message goes here.
'''

createGeneModelsTable = """CREATE TABLE `%s` (
  `gene_model_id` int(11) NOT NULL auto_increment,
  `seqname` varchar(5) NOT NULL,
  `strand` tinyint(2) default NULL,
  `displayid` varchar(30) NOT NULL,
  `score` int(11) default NULL,
  `generange` linestring NOT NULL,
  `translrange` linestring NOT NULL,
  `file_fk` int(11) NOT NULL,
  PRIMARY KEY  (`gene_model_id`),
  UNIQUE KEY `displayid` (`displayid`,`seqname`,`file_fk`),
  SPATIAL KEY `gene_idx` (`generange`),
  SPATIAL KEY `transl_idx` (`translrange`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"""

createGeneExonsTable = """CREATE TABLE `%s` (
  `exon_id` int(11) NOT NULL auto_increment,
  `gene_model_fk` int(11) NOT NULL,
  `exonrange` linestring NOT NULL,
  `seqname` varchar(6) NOT NULL,
  PRIMARY KEY  (`exon_id`),
  SPATIAL KEY `exon_idx` (`exonrange`),
  KEY `seq_idx` (`seqname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"""

createGeneIntronsTable = """CREATE TABLE `%s` (
  `intron_id` int(11) NOT NULL auto_increment,
  `gene_model_fk` int(11) NOT NULL,
  `intronrange` linestring NOT NULL,
  `seqname` varchar(6) NOT NULL,
  PRIMARY KEY  (`intron_id`),
  SPATIAL KEY `intron_idx` (`intronrange`),
  KEY `seq_idx` (`seqname`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"""

createFileTable = """CREATE TABLE `files` (
  `file_id` int(11) NOT NULL auto_increment,
  `filename` varchar(255) NOT NULL,
  `digest` varchar(65) NOT NULL,
  `file_type_fk` int(11) NOT NULL,
  `experiment` varchar(255) default NULL,
  `gm_table` varchar(255) default NULL,
  `exons_table` varchar(255) default NULL,
  `introns_table` varchar(255) default NULL,
  PRIMARY KEY  (`file_id`),
  UNIQUE KEY `filename` (`filename`,`digest`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;"""

featureInsertSQL = """INSERT INTO %s 
			(seqname, strand, displayid, score, generange, translrange, file_fk) 
			VALUES"""
featureSelectSQL = """SELECT gene_model_id FROM %s 
						WHERE seqname=%s AND displayid=%s and file_fk=%s"""
exonInsertSQL = """INSERT INTO %s
			(gene_model_fk, exonrange, seqname)
			VALUES"""
intronInsertSQL = """INSERT INTO %s
			(gene_model_fk, intronrange, seqname)
			VALUES"""
featureValues = "( %s, %s, %s, %s, GeomFromText(%s), GeomFromText(%s), %s )"
intronExonValues = "(%s, GeomFromText(%s), %s)"

def importBed(bed_fh, db_cursor):
	db = db_cursor._get_db()
	bedStart = time.time()
	print >> sys.stderr, "Processing bed file: ", bed_fh.name
	filePath, fileName = os.path.split(bed_fh.name)
	bed_fh.seek(0)
	digest = hashlib.sha256()
	features = []
	for line in bed_fh:
		digest.update(line)
		if line.startswith('track'):
			continue
		features.append(Bed.bedline2feat(line))
	print >> sys.stderr, "Bed processed in: ", time.time() - bedStart
	print >> sys.stderr, "Features to import: %d" % (len(features))
	
	#if db_cursor.execute("SELECT file_type_id FROM file_types WHERE filetype='BED'") != 1:
	#	raise Exception("BED file ID not found in file_types table.")
	#bedID = db_cursor.fetchone()['file_type_id']
	bedID = 2
	db_cursor.execute("""INSERT INTO files (filename, digest, file_type_fk)
						VALUES (%s, %s, %s)""", (fileName, digest.hexdigest(), bedID))
						
	db_cursor.execute("SELECT file_id from files WHERE filename=\'%s\' AND digest=\'%s\'" 
						% (fileName, digest.hexdigest()))
	
	fileID = db_cursor.fetchone()['file_id']
	
	baseTableName = "%s_%s" % (fileName.replace('.', '_'), fileID)
	geneModelTableName = "%s_%s" % ("models", baseTableName)
	exonTableName = "%s_%s" % ("exons",baseTableName)
	intronTableName = "%s_%s" % ("introns", baseTableName)
	tableValues = db.literal((geneModelTableName, exonTableName, intronTableName, fileID))
	
	db_cursor.execute("UPDATE files set gm_table=%s, exons_table=%s, introns_table=%s WHERE file_id=%s" % tableValues)
	
	
	

	db_cursor.execute(createGeneModelsTable % (geneModelTableName))
	db_cursor.execute(createGeneExonsTable % (exonTableName))
	db_cursor.execute(createGeneIntronsTable % (intronTableName))
	
	print >> sys.stderr, "File information inserted."

	featureStart = time.time()

	insertArray = []
	for feature in features:
		sql_values = (feature.getSeqname(),
        			feature.getStrand(),
        			feature.getDisplayId(),
        			feature.getScore(),
        			"LINESTRING(%d 0, %d 0)" % (feature.getStart() + 1, feature.getEnd()),
        			"LINESTRING(%d 0, %d 0)" % feature.getTranslStartAndEnd(interobase_coords = False),
        			fileID
        			)
		
		insertArray.append(featureValues % db.literal(sql_values))
	print >> sys.stderr, "Features processed in: ", time.time() - featureStart
	featureInsert = time.time()
	db_cursor.execute(featureInsertSQL % (geneModelTableName) + ",".join(insertArray))
	print >> sys.stderr, "Features inserted in: ", time.time() - featureInsert
	exonArray = []
	intronArray = []
	
	structureStart = time.time()
	for feature in features:
		selection_values = db.literal((feature.getSeqname(),
						feature.getDisplayId(),
						fileID))
		selection_values = (geneModelTableName,) + selection_values
		db_cursor.execute(featureSelectSQL % selection_values)
		featureID = db_cursor.fetchone()['gene_model_id']
		featureSeqname = "'%s'" % feature.getSeqname()
		exonSQL = exonsToSQL(feature)
		intronSQL = intronsToSQL(feature)
		for sql in exonSQL:
			exonArray.append(intronExonValues % (featureID, sql, featureSeqname))
		for sql in intronSQL:
			intronArray.append(intronExonValues % (featureID, sql, featureSeqname))
	print >> sys.stderr, "Structures processed in: ", time.time() - structureStart

	structureInsert = time.time()
	db_cursor.execute(exonInsertSQL % (exonTableName) + ",".join(exonArray))	
	db_cursor.execute(intronInsertSQL %(intronTableName) + ",".join(intronArray))
	print >> sys.stderr, "Structures inserted in: ", time.time() - structureInsert
	
def exonsToSQL(feature):
	exons = feature.getSortedFeats("exon")
	exonSQL = []
	for exon in exons:
		sql = ("'LINESTRING(%d 0, %d 0)'" % (exon.getStart() + 1, exon.getEnd()))
		exonSQL.append(sql)
	return exonSQL

def intronsToSQL(feature):
	exons = feature.getSortedFeats("exon")
	intronSQL = []
	currentExon = 0
	end = len(exons) - 1
	while currentExon < end:
		cExon = exons[currentExon]
		nExon = exons[currentExon + 1]
		sql = ("'LINESTRING(%d 0, %d 0)'" % (cExon.getEnd() + 1, nExon.getStart()))
		intronSQL.append(sql)
		currentExon += 1
	return intronSQL
	
def resetMemory():
	sys.exc_clear()
	sys.exc_traceback = sys.last_traceback = None
	gc.collect()
	
class Usage(Exception):
	def __init__(self, msg):
		self.msg = msg


def main(argv=None):
	if argv is None:
		argv = sys.argv
	try:
		try:
			opts, args = getopt.getopt(argv[1:], "ho:v", ["help", "output="])
		except getopt.error, msg:
			raise Usage(msg)
	
		# option processing
		for option, value in opts:
			if option == "-v":
				verbose = True
			if option in ("-h", "--help"):
				raise Usage(help_message)
			if option in ("-o", "--output"):
				output = value
	
	except Usage, err:
		print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
		print >> sys.stderr, "\t for help use --help"
		return 2
		
	db = MySQLdb.connect(host="dna.transvar.org", user="abaxter5", passwd="sawn89!cuber", db="illumina_analysis", cursorclass=MySQLdb.cursors.DictCursor)
	c = db.cursor()
	for arg in args:
		bed_fh = open(arg, 'r')
		importBed(bed_fh, c)
		bed_fh.close()

if __name__ == "__main__":
	sys.exit(main())
