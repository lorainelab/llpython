# Original code located: http://code.activestate.com/recipes/286239/
# sorted list to btree algorithm came from http://www.ihas1337code.com/2010/11/convert-sorted-list-to-balanced-binary.html
# Both heavily modified for GeneModels
import sys, Mapping.Parser.Bed as Bed, Mapping.FeatureModel as Feature

class bTree:
    def __init__(self, model):
        #initialize the root member
        self.parent = None
        self.left = None
        self.leftRange = None
        self.right = None
        self.rightRange = None
        self.model = model

    def _updateRanges(self):
        lMin = None
        lMax = None
        rMin = None
        rMax = None
        minList = [self.model.getStart()]
        maxList = [self.model.getStart() + self.model.getLength()]
        if self.left is not None:
            lMin, lMax = self.left._updateRanges()
            self.leftRange = Feature.Range(lMin, lMax - lMin)
            minList.append(lMin)
            maxList.append(lMax)
        if self.right is not None:
            rMin, rMax = self.right._updateRanges()
            self.rightRange = Feature.Range(rMin, rMax - rMin)
            minList.append(rMin)
            maxList.append(rMax)

        return (min(minList), max(maxList))

    def addNode(self,model):
        return bTree(model)

    def insert(self, newModel):
    # enters into the tree
        if newModel < self.model or self.model.overlaps(newModel):
            #if the data is less than the stored one
            # goes into the left-sub-tree
            if self.left is not None:
                self.left.insert(newModel)
            else:
                self.left = self.addNode(newModel)
        else:
            #process the right-sub-tree
            if self.right is not None:
                self.right.insert(newModel)
            else:
                self.right = self.addNode(newModel)

    def lookupAndScore(self, target):
        #Looks for a overlapping GeneModel and scores those that match
        lOverlaps = 0
        rOverlaps = 0
        overlapsNode = 0
        #if it has found it
#        if self.model.overlaps(target, ignore_strand = True):
        if self.model.overlaps(target):
            self.model.setScore(self.model.getScore() + 1)
            overlapsNode = 1
        if self.left is not None and self.leftRange.overlaps(target):
            lOverlaps = self.left.lookupAndScore(target)
        if self.right is not None and self.rightRange.overlaps(target):
            rOverlaps = self.right.lookupAndScore(target)

        return lOverlaps + overlapsNode + rOverlaps

    def lookupOverlap(self, target):
        #Looks for a value into the tree
        lOverlaps = []
        rOverlaps = []
        overlapNode = []
        #if it has found it
#        if self.model.overlaps(target, ignore_strand = True):
        if self.model.overlaps(target):
            overlapNode = [self.model]
        if self.left is not None and self.leftRange.overlaps(target):
            lOverlaps = self.left.lookupOverlap(target)
        if self.right is not None and self.rightRange.overlaps(target):
            rOverlaps = self.right.lookupOverlap(target)

        lOverlaps.extend(overlapNode)
        lOverlaps.extend(rOverlaps)
        return lOverlaps

    def treeToDict(self, mDict):

        if self.left is not None:
            self.left.treeToDict(mDict)

        mDict.update([(self.model.getDisplayId(), self.model)])

        if self.right is not None:
            self.right.treeToDict(mDict)

    def treeScoresToDict(self, mDict):

        if self.left is not None:
            self.left.treeScoresToDict(mDict)

        mDict.update([(self.model.getDisplayId(), self.model.getScore())])

        if self.right is not None:
            self.right.treeScoresToDict(mDict)

    def minValue(self):
        # goes down the left arm and returns the last value
        minVal = self
        while(minVal.left is not None):
            minVal = minVal.left
        return minVal.model

    def maxDepth(self):
        ldepth, rdepth = self._callLeftAndRight(bTree.maxDepth)
        #returns the appropriate depth
        return max(ldepth, rdepth) + 1

    def size(self):
        ldepth, rdepth = self._callLeftAndRight(bTree.size)
        return ldepth + 1 + rdepth

    def resetAllScores(self):
        self._callLeftAndRight(bTree.resetAllScores)
        self.model.setScore(0)

    def joinTouchingFeats(self):
        self._callLeftAndRight(bTree.joinTouchingFeats)
        self.model.joinTouchingFeats()

    def _callLeftAndRight(self, call):
        if self.left is not None:
            lReturn = call(self.left)
        else:
            lReturn = 0

        if self.right is not None:
            rReturn = call(self.right)
        else:
            rReturn = 0

        return (lReturn, rReturn)

    def _setParent(self, parent = None):
        self.parent = parent
        if self.left:
            self.left._setParent(parent = self)
        if self.right:
            self.right._setParent(parent = self)

    def printTree(self, print_h = sys.stdout):
    #prints the tree path
        if self.left is not None:
            self.left.printTree(print_h)

        print_h.write(Bed.feat2bed(self.model) + "\n")

        if self.right is not None:
            self.right.printTree(print_h)

    def printRevTree(self):
        #prints the tree path in reverse order
        if self.right is not None:
            self.right.printTree(print_h)

        print Bed.feat2bed(self.model)

        if self.left is not None:
            self.left.printTree(print_h)

    def printScoredTree(self, sam_name, print_h = sys.stdout):
    #prints the tree path
        if self.left is not None:
            self.left.printScoredTree(sam_name, print_h)

        output = [ self.model.getSeqName(), self.model.getDisplayId(),  \
            str(self.model.getScore()), sam_name ]

        print_h.write( '\t'.join(output) + "\n")

        if self.right is not None:
            self.right.printScoredTree(sam_name, print_h)

def _sortedListToBTree_step(iterSList, start, end):
    if (start > end): return None
    mid = start + (end - start) / 2
    leftChild = _sortedListToBTree_step(iterSList, start, mid - 1)
    parent = bTree(iterSList.next())
    parent.left = leftChild
    parent.right = _sortedListToBTree_step(iterSList, mid + 1, end)
    return parent

def sortedListToBTree(sortedList):
    iterSList = iter(sortedList)
    newTree = _sortedListToBTree_step(iterSList, 0, len(sortedList) -1 )
    newTree._setParent()
    newTree._updateRanges()
    return newTree



