#!/usr/bin/env python

import make_annots_xml as m
import os,sys

ab="ffffff"

genome='A_thaliana_June_2009'

se_url=genome+'/sr45a_hs_TH2.0.5_processed'
pe_url=genome+'/SR45APE_processed'

pe_samples=[['T2MH1','T2MH1','CC00CC',ab,pe_url],
            ['T2MH2','T2MH2','990099',ab,pe_url],
            ['T2MH3','T2MH3','CC00CC',ab,pe_url],
            ['T2MC1','T2MC1','335D66',ab,pe_url],
            ['T2MC2','T2MC2','008000',ab,pe_url],
            ['T2MC3','T2MC3','335D66',ab,pe_url]]

# COLORS=c("#2171B5","#A45209","#4292C6","#FF6600")
# names(COLORS)=c('wCool','wHot','mCool','mHot')
ws_cool="2171B5"
ws_hot="A45209"
hm_cool="4292C6"
hm_hot="FF6600"
single_end_samples = [['P11_WS_Cool_T1','Cool WS P11',ws_cool,ab,se_url],
                      ['P18_WS_Cool_T1','Cool WS P18',ws_cool,ab,se_url],
                      ['P24_HM_Cool_T1','Cool WS P24',ws_cool,ab,se_url],
                      #24 samples were switched
                      ['P11_HM_Cool_T1','Cool HM P11',hm_cool,ab,se_url],
                      ['P18_HM_Cool_T1','Cool HM P18',hm_cool,ab,se_url],
                      ['P24_WS_Cool_T1','Cool HM P24',hm_cool,ab,se_url],
                      ['P2_WS_Hot_T1','Hot WS P2',ws_hot,ab,se_url],
                      ['P6_WS_Hot_T1','Hot WS P6',ws_hot,ab,se_url],
                      ['P20_WS_Hot_T1','Hot WS P20',ws_hot,ab,se_url],
                      ['P2_HM_Hot_T1','Hot HM P2',hm_hot,ab,se_url],
                      ['P6_HM_Hot_T1',"Hot HM P6",hm_hot,ab,se_url],
                      ['P20_HM_Hot_T1',"Hot HM P20",hm_hot,ab,se_url]]

def doARR10ChIPSeq():
    deploy_dir='ARR10_analysis/'
    u = genome+os.sep+deploy_dir
    b='FFFFFF'
    t='9900FF'
    c='336600'
    lsts = []
    folder='ARR10 ChIP-Seq / '
    background='ffffff'
    samples=[]
    for n in range(1,7):
        fn=deploy_dir+'GES'+str(n)
        if n == 1:
            s = 'IP R1'
            descr = "ARR10 IP replicate 1"
            foreground=t
        elif n == 2:
            s = 'IP R2'
            descr = "ARR10 IP replicate 2"
            foreground=t
        elif n == 3:
            s = 'C R1'
            descr = "Control for ARR10 IP replicate 1"
            foreground=c
        elif n == 4:
            s = 'C R2'
            descr = "Control for ARR10 IP replicate 2"
            foreground=c
        elif n == 5:
            fn = deploy_dir+'I'
            s = 'IP Combined'
            descr = 'ARR10 IP combined replicates'
            foreground=t
        else:
            fn = deploy_dir+'C'
            s = 'C Combined'
            descr = 'Control for ARR10 IP combined replicates'
            foreground=c
        a = m.Annotations(fn=fn+'.bam',
                       description = descr + ' alignments',
                       title = folder + ' Reads / ' + s,
                       url=u,
                       style=m.QuickloadReadStyle(foreground=foreground,
                                                  background=background,
                                                  namesize='20'))
        samples.append(a)
        a = m.Annotations(fn=fn+'.bedgraph.gz',
                       description = descr + ' alignments',
                       title = folder + ' Graphs / ' + s,
                       url=u,
                       style=m.QuickloadCoverageStyle(foreground=foreground,
                                                      background=background,
                                                      namesize='20',
                                                      show2tracks=False))
        samples.append(a)
    foreground='3300CC'
    for prefix in ['R1','R2','I']:
        if prefix=='I':
            title=" Combined regions"
            description="Regions found in combined samples"
        else:
            title = " " + prefix + " regions"
            description="Regions found in "+prefix
        a = m.Annotations(fn=deploy_dir+prefix+'.bed.gz',
                          title=folder+title,
                          description = 'description',
                          url=u,
                          style=m.QuickloadAnnotStyle(foreground=foreground,
                                                      background=background,
                                                      namesize='16'))
        samples.append(a)
    return samples

def main(fname=None):
    txt = m.makeFilesTag()
    r = doARR10ChIPSeq()
    for dataset in r:
        tag = m.makeFileTag(dataset)
        txt = txt + tag 
    r = m.makeSamplesForRNASeq(lsts=single_end_samples,
                               folder='sr45a-2, SE 4h heat stress',
                               deploy_dir='sr45a_hs_TH2.0.5_processed',
                               all=True)
    for dataset in r:
        tag = m.makeFileTag(dataset)
        txt = txt + tag
    r = m.makeSamplesForRNASeq(lsts=pe_samples,
                             folder='sr45a-2, PE 1h recovery',
                             deploy_dir='SR45APE_processed',
                             all=True)

    for dataset in r:
        tag = m.makeFileTag(dataset)
        txt = txt + tag
    txt = txt + m.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()
