#!/usr/bin/env python

import make_annots_xml as mk
import os,sys

# constants
ab='FFFFFF'
url='S_lycopersicum_Feb_2014/pollen_processed_2.5'
c='336600'
t='9900FF'
folder='Heat Stress RNA-Seq'
samples = [['C1','C1',c,ab,url],
         ['C2','C2',c,ab,url],
         ['C3','C3',c,ab,url],
         ['C4','C4',c,ab,url],
         ['C5','C5',c,ab,url],
         ['T1','T1',t,ab,url],
         ['T2','T2',t,ab,url],
         ['T3','T3',t,ab,url],
         ['T4','T4',t,ab,url],
         ['T5','T5',t,ab,url]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder=folder,
                                deploy_dir=u.split(os.sep)[-1],
                                all=False)
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()




        
