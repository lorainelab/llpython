#!/usr/bin/env python

import make_annots_xml as m
import sys

genome='P_patens_Jan_2008'
deploy_dir='SRP019809_processed'
du=genome+'/'+deploy_dir

ab='ffffff' # track background
hot2='3366FF'
hot1='990000'
cool='336600'

heat = [#["C","Control, combined",cool,ab,du],
        ["C-75","PE Control, 75 bp SRR790623",cool,ab,du],
        ["C-76","SE Control, 76 bp SRR790648",cool,ab,du],
        ["C-94","PE Control, 94 bp SRR790660",cool,ab,du],
        ["C-101","PE Control, 101 bp SRR790254",cool,ab,du],

        #["H","1st Heat Shock, combined",hot1,ab,du],
        ["H1-75","PE Heat 1, 75 bp SRR790624",hot1,ab,du],
        ["H1-76","SE Heat 1, 76 bp SRR790649",hot1,ab,du],
        ["H1-94","PE Heat 1, 94 bp SRR790662",hot1,ab,du],
        ["H1-101","PE Heat 1, 101 bp SRR790259",hot1,ab,du],

        #["H2","2nd Heat Shock, combined",hot2,ab,du],
        ["H2-75","PE Heat 2, 75 bp SRR790625",hot2,ab,du],
        ["H2-76","SE Heat 2, 76 bp SRR790650",hot2,ab,du],
        ["H2-94","PE Heat 2, 94 bp SRR790663",hot2,ab,du],
        ["H2-101","PE Heat 2, 101 bp SRR790261",hot2,ab,du]]

def main(fname=None):
    txt = m.makeFilesTag()
    models_info=['P.patens.V6_filtered_cosmoss_genes.gff.gz',
                 'Gene Models, v1.6',
                 'Gene model annotations from CossMoss.org, downloaded Nov. 2013',
                 '/P_patens_Jan_2008']
    style=m.QuickloadStyle(load_hint='Whole Sequence',
                           label_field='id',
                           name_size='14',
                           show2tracks='False',
                           direction_type='arrow',
                           foreground='000000',
                           background='ffffff')                           
    s = '\t'.join(models_info)
    models=m.Annotations(fn='P.patens.V6_filtered_cosmoss_genes.gff.gz',
                       title='CosMoss Gene Models, v1.6',
                       description='Gene model annotations from CossMoss.org, downloaded Nov. 2013',
                       url='https://www.cosmoss.org/physcome_project/wiki/Downloads',
                       style=style)
    tag = m.makeFileTag(models)
    txt = txt + tag
    r = m.makeSamplesForRNASeq(lsts=heat,
                               folder='RNA-Seq / Heat SRP019809',
                               deploy_dir=deploy_dir,
                               all=True)
    for dataset in r:
        tag = m.makeFileTag(dataset)
        txt = txt + tag
    txt = txt + m.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()
