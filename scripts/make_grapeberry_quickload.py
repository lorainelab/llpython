#!/usr/bin/env python

"""Make annots.xml for grape berry development QuickLoad site."""

import sys,os
import make_annots_xml_for_quickload as mk

# colors
t0='00E414' # green
t2='00A15D' # green
t6='EF597B' # rose
t12='800080' # purple

b="FFFFFF"
u='/V_vinifera_Mar_2010/vv_TH2.0.13_processed'

# file name, quickload title, foreground color, background color, URL (relative to QuickLoad root)
samples=[['T0-1','T0',t0,b,u], 
         ['T2-51','T2',t2,b,u],
         ['T6-31','T6',t6,b,u],
         ['T12-51','T12',t12,b,u]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeqSimple(lsts=samples,folder='RNA-Seq',
                                    deploy_dir=u.split(os.sep)[-1])
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()

