"A script that reads a bed file and calculates a distribution of intron sizes."

import Mapping.Parser.Bed as b

def getIntrons(fn=None):
    "Read the bed file and create intron subfeatures"
    feats = b.bed2feats(fname=fn)
    introns = {}
    for feat in feats:
        exons = feat.getSortedFeats(feat_type='exon')
        num_introns = 0
        chrom = feat.getSeqname()
        strand = feat.getStrand()
        for i in range(len(exons)-1):
            intron_start = exons[i].getStart()+exons[i].getLength()
            intron_end = exons[i+1].getStart()
            num_introns = num_introns+1
            key = '%s,%i,%i,%i'%(chrom,intron_start,intron_end,strand)
            if introns.has_key(key):
                introns[key]=introns[key]+1
            else:
                introns[key]=1
        if num_introns != len(exons)-1:
            raise Exception("Error in number of introns.")
    return introns

def writeIntrons(fn=None,introns=None,sep=','):
    "Write the introns to the given file"
    fh = open(fn,'w')
    heads = ['seqname','start','end','length','N']
    fh.write(sep.join(heads)+'\n')
    for key in introns.keys():
        vals = key.split(',')
        seqname = vals[0]
        start = int(vals[1])
        end = int(vals[2])
        length = end-start
        N = introns[key]
        fh.write('%s%s%i%s%i%s%i%s%i\n'%(seqname,sep,start,sep,end,sep,length,sep,N))
    fh.close()


                        
