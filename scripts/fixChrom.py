"""Fix chromosomes in Arabidopsis GTF from iGenomes"""

def fixChrom(infn='genes.gtf',out='tair10genes.gtf'):
    infh = open(infn,'r')
    outfh = open(out,'w')
    while 1:
        line = infh.readline()
        if not line:
            break
        toks = line.split('\t')
        toks[0]='chr'+toks[0]
        line = '\t'.join(toks)
        outfh.write(line)
    outfh.close()
    infh.close()
