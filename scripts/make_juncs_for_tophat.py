"""Create junctions for TopHat/Bowtie, using models from a Bed File"""

import os,sys
import Mapping.Parser.Bed as b
import Utils.General as utils

def getBedFromWeb(u='http://www.bioviz.org/quickload/A_thaliana_Jun_2009/TAIR9.bed.gz'):
    fh = os.popen('curl -O %s'%u)
    print fh.read()

def readBed(fn='TAIR9.bed.gz',producer="TAIR9",group_feat="mRNA"):
    models = b.bed2feats(fname=fn,group_feat=group_feat,producer=producer)
    return models

def writeJuncs(models,fn='tair9_junctions.txt'):
    min_intron = 100000000
    min_intron_model = None
    max_intron = 0
    max_intron_model = None
    fh = open(fn,'w')
    for model in models:
        chrname = model.getSeqname()
        strand = model.getStrand()
        if strand == -1:
            strand = '-'
        else:
            strand = '+'
        juncs = makeJuncs(model)
        for junc in juncs:
            intron_size = junc[1]-junc[0]
            if intron_size < min_intron:
                min_intron = intron_size
                min_intron_model=model
            if intron_size > max_intron:
                max_intron = intron_size
                max_intron_model = model
            s = '%s\t%i\t%i\t%s\n'%(chrname,junc[0],junc[1],strand)
            fh.write(s)
    return (min_intron_model,min_intron,max_intron_model,max_intron)
        
def makeJuncs(model):
    exons = model.sortedFeats(feat_type="exon")
    i = 0
    num_exons = len(exons)
    juncs = []
    for exon in exons:
        i = i + 1
        if i < num_exons:
            next_exon = exons[i]
            left = exon.getStart()+exon.getLength()
            right = next_exon.getStart()
            juncs.append((left,right))
    return juncs
