#!/bin/sh 
for bam in ${@}
do
prefix=${bam%.bam}
result=`samtools view -c $bam`
echo "$prefix: $result"
done