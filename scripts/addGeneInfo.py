#!/usr/bin/env python
"""
Add a new column with gene aliases and gene descriptions, if available.

Need to add nice main method, documentation.
"""
from Utils.General import readfile
import getopt
import re, gzip, zipfile, sys,os
import Utils.DbUtils as db

def makeDbConnection(password=None,
                     database=None,
                     host=None,
                     user=None):
    "Make a database connection. If parameters are not supplied, will use corresponding environment variables, provided they are set."
    return db.makeDbConnection(password=password,
                               database=database,
                               host=host,
                               user=user)
def get_gene_info(dbc=None):
    "Retrieve meta-data about loci from database. Returns a dictionary where AGI codes are keys."
    if not dbc:
        dbc = makeDbConnection()
    c = dbc.cursor()
    d = {}
    sql = 'select name,locus_type,other_names,specific_probesets,crosshybing_probesets,description from locus_info'
    c.execute(sql)
    while 1:
        row = c.fetchone()
        if not row:
            break
        agi =  row['name']
        d[agi]=row
    dbc.close()
    return d
    
def addCols(namecol=0,oldfn=None,newfn=None,header=True,sep="\t",gene_info=None):
    """
    Function: 
    Returns : 
    Args    : namecol - column with the AGI code, can't be last column (6 for ArabiTag output)
              oldfn - file to read
              newfn - file to write
              header - whether or not oldfn has a header row
              sep - field separator
              gene_info - output from get_locus_info
    """
    if not gene_info:
        gene_info=get_gene_info()
    fh = open(oldfn,'r')
    outfh = open(newfn,'w')
    extra_fields = ['locus_type','other_names','description']
    if header:
        firstline = fh.readline().rstrip()
        heads = firstline.split(sep)
        newheads = []
        i = 0
        for head in heads:
            newheads.append(head)
            if i == namecol:
                for extra_head in extra_fields:
                    newheads.append(extra_head)
            i = i + 1
        newheader = sep.join(newheads)+os.linesep
        outfh.write(newheader)
    while 1:
        newtoks = []
        line = fh.readline()
        if not line:
            outfh.close()
            fh.close()
            break
        toks = line.split(sep)
        name = toks[namecol].split('.')[0] # hack alert
        if not gene_info.has_key(name):
            raise ValueError("No such value in locus_info: %s from line: %s"%(name,line))
        subd = gene_info[name]
        i = 0
        for tok in toks: 
            newtoks.append(tok)
            if i == namecol:
                for key in extra_fields:
                    try:
                        newtoks.append(subd[key])
                    except TypeError:
                        print subd,key
            i = i + 1
        outfh.write(sep.join(newtoks)) # assumes line separator was on last tok

    
if __name__ == '__main__':
    import optparse
    usage = "%prog [options] INFILE OUTFILE"
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--agi-col',dest='namecol',help='field in the file with AGI code, first column is numbered 0 [default is 0]',
                     default=0,type="int")
    parser.add_option('-t','--top_header',dest='header',help="whether there's a header [default is True]",
                      action="store_true",default=True)
    (options,args)=parser.parse_args()
    if not len(args) >= 2:
        parser.error("Need INFILE and OUTFILE")
    addCols(namecol=options.namecol,oldfn=args[0],newfn=args[1],header=options.header)
