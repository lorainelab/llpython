#!/usr/bin/env python
"""
For making databases for blastx of new plant transcriptome.
Uses biopython SeqIO.
"""
import optparse,os,sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC

# dictionary
# species we would want to compare a new plant transcriptome to
# key is the NCBI taxonomy identifier and value is the genus/species name 
SPECIESD = {'29760':'Vitis vinifera', # grape 
#           '3694':'Populus trichocarpa', # black cottonwood
           '3880':'Medicago truncatula',# a model legume
           '3702':'Arabidopsis thaliana',
           '39947':'Oryza sativa Japonica Group', # rice
           '39947':'Oryza sativa Indica Group', # rice
           '3988':'Ricinus communis', # castor bean
           '3847':'Glycine max', # soybean
           '4081':'Solanum lycopersicum', # tomato
           '88036':'Selaginella moellendorffii' # club moss 
           }

SPECIES=['Vitis vinifera','Prunus persica','Medicago truncatula',
         'Arabidopsis thaliana','Oryza sativa Japonica Group',
         'Oryza sativa Indica Group','Ricinus communis','Theobroma cacao',
         'Glycine max','Solanum lycopersicum','Selaginella moellendorffii',
         'Zea mays']


# ftp site where we can get refseq sequences
ftp = 'ftp://ftp.ncbi.nih.gov/refseq/release/plant'

# script to retrieve them
"""
#!/bin/sh

# http://n00bsys0p.co.uk/blog/2012/07/09/wget-entire-ftp-folder-its-index-regex-introduction

# gets plant data
for i in $(wget ftp://ftp.ncbi.nih.gov/refseq/release/plant/ -O - | grep "ftp://" | sed 's/^.*href=\"//g' | sed 's/\".*$//
g' | grep protein.faa.gz); do
    wget $i;
done
"""

def makeDb(sep=None,species=None,path=None):
    """
    Function: Make a fasta files with plant sequences we'll use for annotation.
              Can split them into files by species or write them to one large file.
              Looks for sequences in files named plant.*.protein.faa in the current
              working directory.
    Returns : 
    Args    : path - location of refseq files downloaded from ncbi and also
                     where new files will appear
              species - dictionary, where keys are taxonomy ids (strings)
                 and values (strings) are their corresponding genus, species names
              sep - whether or not to divide the databases into different
                    files by species
    """
    fhs=None
    if sep:
        fhs = {}
        for g_s in species:
            fn = '_'.join(g_s.split(' ')[0:2])+'.aa.fa'
            fn = path+os.sep+fn
            if not fhs.has_key(g_s):
                fh = open(fn,'w')
                fhs[g_s]=fh
    else:
        writer=open(path+os.sep+'refseq.aa.fa','w')
    files=os.listdir(path)
    for f in files:
        if f.endswith('.protein.faa') and f.startswith('plant.'):
            if not f.startswith(path):
                f=path+os.sep+f
            fh = open(f,'r')
            for rec in SeqIO.parse(fh,'fasta'):
                for key in species:
                    tomatch='[%s]'%key
                    if rec.description.endswith(tomatch):
                        if sep:
                            writer = fhs[key] 
                        SeqIO.write([rec],writer,'fasta')
    if sep:
        for fh in fhs.values():
            fh.close()
    else:
        writer.close()

def main(sep=None,path=None):
    makeDb(path=path,sep=sep,species=SPECIES)
    

if __name__ == '__main__':
    usage="%prog [options]"
    parser=optparse.OptionParser(usage)
    parser.add_option('-s','--sep',help="separate databases by species [optional]",dest="sep",
                      default=False,action="store_true")
    parser.add_option('-d','--dir',help="directory containing downloaded refseq files",
                      dest="path",default=".",type="string")
    (options,args)=parser.parse_args()
    main(sep=options.sep,path=options.path)
