#!/usr/bin/env pypy
# encoding: utf-8
"""
countNovelReads.py

Takes an annotation BED file, a BED file from an RNA-Seq experiment: scores gene models based on where
the reads miss the annotation file.

Created by Adam Baxter on 2011-02-18.
Copyright (c) 2010 Loraine Lab. All rights reserved.
"""

import sys, getopt, collections, subprocess, os
import re, time, pickle
import Mapping.Parser.Bed as Bed, Mapping.FeatureModel as Feature
import execnet, btree, countReads

displayLoci = re.compile("(.{4}\d{5})(?=\..*)?")

def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

help_message = '''
usage: countReads.py -a annotationFile [-hjc] [-o outputFile] [bedFiles]
Options and arguments:
-h   : displays this help message
-a   : annotation bed file containing the gene models
-o   : output filename, otherwise uses stdout
-c   : genes will be sorted by SeqName and gene Start, rather than DisplayId
'''

class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    startTime = time.time()
    packArgs = {"annotBedFilePath" : None,
                "outputFilePath" : None,
                "jobsMax" : 1,
                "inputFilePaths" : [None],
                "isClient": False,
                "sortOutput": False}

    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ho:a:vc", ["help", "output="])
        except getopt.error, msg:
            raise Usage(msg)

        # option processing
        for option, value in opts:
            if option == "-v":
                packArgs["verbose"] = True
            if option in ("-h", "--help"):
                raise Usage(help_message)
            if option in ("-a"):
                packArgs["annotBedFilePath"] = value
            if option in ("-o", "--output"):
                packArgs["outputFilePath"] = value
            if option in ("-j"): # not implemented yet
                packArgs["jobsMax"] = int(value)
            if option in ('-c'):
                packArgs["sortOutput"] = True


        if packArgs["annotBedFilePath"] is None:
            raise Usage("No Annotation BED file provided")
        if len(args) > 0:
            packArgs["inputFilePaths"] = args

    except Usage, err:
        print >> sys.stderr, sys.argv[0].split("/")[-1] + ": " + str(err.msg)
        print >> sys.stderr, "\t for help use --help"
        return 2

    if packArgs["jobsMax"] > 1:
        server(packArgs)
    else:
        runJob(packArgs)

    print >> sys.stderr, "Processed %d files in %f seconds" % (len(args), time.time() - startTime)


def server(packArgs):
    """
    Function: Creates worker instances to run jobs
    Return  : None
    Args    : packArgs (dict) - the argument dictionary to pass along
    """
    #TODO: Update to work with novel reads
    startTime = time.time()
    print "Generating server context"
    jobsArgs = []
    jobList = []
    pickledData = []
    jobsMax = packArgs["jobsMax"]
    inputFiles = collections.deque(packArgs["inputFilePaths"])
    totalFiles = len(inputFiles)
    bucketSize = totalFiles // jobsMax
    leftover = totalFiles % jobsMax
    pythonExecutable = sys.executable

    experiments = {}

    for i in xrange(jobsMax): #create an argument, nicely distribute files
        if len(inputFiles) > 0:
            jobsArgs.append([])
            for j in xrange(bucketSize):
                jobsArgs[i].append(inputFiles.popleft())
            if leftover > 0:
                jobsArgs[i].append(inputFiles.popleft())
                leftover -= 1
    for args in jobsArgs: #create a new argument package
        if len(args) > 0:
            job = packArgs.copy()
            job["inputFilePaths"] = args
            job["jobsMax"] = 1
            job["isClient"] = True
            jobList.append(job)

    gwJobs = []
    for job in jobList:
        gw = execnet.makegateway("popen//python=%s" % pythonExecutable)
        channel = gw.remote_exec(countReads)
        channel.send(job)
        gwJobs.append(channel)
        print >> sys.stderr, "Generated job."
        channel.setcallback(pickledData.append)

    returnedResults = 0 # Process results as they come in (I hope)
    while returnedResults < totalFiles:
        for data in pickledData:
            print >> sys.stderr, "Received output"
            experiments.update(pickle.loads(data))
            pickledData.remove(data)
            returnedResults += 1
        time.sleep(30)

    for job in gwJobs:
        job.waitclose()

    for data in pickledData: # Process any remaining data
        experiments.update(pickle.loads(data))

    outputFilePath = packArgs["outputFilePath"]
    outputFormat = packArgs["outputFormat"]

    if outputFormat == OutputFormat.SCORING_MATRIX:
        normalize = packArgs["normalize"]
        lociOnly = packArgs["lociOnly"]
        bedFilePath = packArgs["bedFilePath"]
        bed_fh = open(bedFilePath, 'r')
        printScoringMatrix(experiments, bed_fh, outputFilePath, lociOnly, normalize)
        bed_fh.close()

    print >> sys.stderr, "Elapsed time: ", time.time() - startTime


def client():
    """
    Function: Receives and processed the job list
    Return  : None
    Args    : packArgs (dict) - the argument dictionary (from execnet channel)
    """
    packArgs = None
    packArgs = channel.receive()
    runJob(packArgs)

def runJob(packArgs):
    """
    Function: the function that processes the job
    Return  : None
    Args    : packArgs (dict) - the argument dictionary
    """
    annotBed_fh = None
    bed_fh = None
    out_fh = None

    experiments = {}

    inputFilePaths = packArgs["inputFilePaths"]
    annotBedFilePath = packArgs["annotBedFilePath"]
    outputFilePath = packArgs["outputFilePath"]
    sortOutput = packArgs["sortOutput"]
    isClient = packArgs["isClient"]


    annotBed_fh = openFileHandle(annotBedFilePath, 'r')
    annotGraph = readBedToTree(annotBed_fh, resetScores = True, lociOnly = True)
    
    for inputFile in inputFilePaths:
        processFile = time.time()

        seq_fh = openFileHandle(inputFile, 'r', sys.stdin)
        bedList = readBedToList(seq_fh, resetScores = False, lociOnly = False)
        seq_fh.close()
        
        path, bedName = os.path.split(inputFile)
        bedShortName, fileExt = os.path.splitext(bedName)

        novelModels, featuresProcessed = processBEDList(annotGraph, bedList)
        
        if sortOutput:
            novelModels.sort(key = lambda model: (model[1].getSeqname(), model[1].getStart()))
        
        out_fh = open(bedShortName+"_novel"+fileExt, 'w')
        print "Features processed: ", featuresProcessed
        for score, model in novelModels:
            out_fh.write(Bed.feat2bed(model))
        out_fh.close()
    annotBed_fh.close()


def returnDataToServer(jobData):
    """
    Function: Sends experiment data back to server process
    Return  : None (sends data to server process)
    Args    : jobData (dict of lists) - the scores to return
    """
    pickledData = pickle.dumps(jobData, pickle.HIGHEST_PROTOCOL)
    channel.send(pickledData)

def processBEDList(annotGraph, bedList):
    """
    Function: Processes the SAM file
    Return  : Dictionary of genemodels, count of SAM lines processed
    Args    : bed_fh (file handle) - the bed file
              sam_fh (file handle) - the sam file
              resetScores (bool)   - reset the bed file scores
              lociOnly (bool)      - Compare only gene loci
    """
    #For each line in the SAM file, score the models that overlap it
    i = 0
    novelModels = []
    for model in bedList:
        modelScore = []
        for feature in model.getSortedFeats("exon"):
            i += 1
            rangeMatches = annotGraph[model.getSeqname()].lookupOverlap(feature)
            if len(rangeMatches) == 0:
                modelScore.append("novel")
            else:
                featureOverlaps = False
                for rangeMatch in rangeMatches:
                    for featMatch in rangeMatch.getSortedFeats("exon"):
                        if featMatch.overlaps(feature, ignore_strand = True):
                            featureOverlaps = True
                            break
                    if featureOverlaps:
                        break
                if not featureOverlaps:
                    modelScore.append("intron")
        if len(modelScore) > 0:
            novelModels.append((modelScore, model))

    return novelModels, i

def openFileHandle(filePath, openOptions, optionalHandle = None):
    """
    Function: A custom openFile function.
    Return  : The open file handle, or None
    Args    : filePath (string) - the filename to open.
              openOptions (string) - filehandle options (r, w, rw)
              optionalHandle(file) - if the filePath is None, use this instead
    """
    fileHandle = None #The filehandle to be returned
    if filePath is None:
        if optionalHandle is None:
            #TODO: error out
            pass
        else:
            fileHandle = optionalHandle
    else:
        try:
            fileHandle = open(filePath, openOptions)
        except Exception, error:
            #TODO: handle exception
            pass
    return fileHandle


def readBedToList(bed_fh, resetScores = True, lociOnly = False):
    """
    Function: Reads and parses a BED file into a list
    Return  : list of GeneModels
    Args    : bed_fh (file handle) - the bed file to parse
              resetScore (bool) - reset the scores to 0
              lociOnly (bool) - whether to only process gene loci
    """
    i = 0
    bed_fh.seek(0)
    bedList = []
    lociBedDict = {}
    for line in bed_fh:
        i += 1
        if len(line) < 2:
            continue
        if line.startswith('track'):
            continue
        try:
            model = Bed.bedline2feat(line)
            if resetScores:
                model.setScore(0)
        except Exception, exc:
            handleExceptionAtLine(exc, "BED", i)
            exit(2)
        else:
            if lociOnly:
                lociId = displayLoci.search(model.getDisplayId()).group()
                if lociId in lociBedDict:
                    lModel = lociBedDict[lociId]
                    #if this model begins before the one in the dict, match it
                    if model.getStart() < lModel.getStart():
                        lModel.setStart(model.getStart())
                    #if this model ends after than the one in the dict, match it
                    if model.getEnd() > lModel.getEnd():
                        lModel.setLength(model.getEnd() - lModel.getStart())
                else:
                    model.setDisplayId(lociId)
                    lociBedDict[lociId] = model
            else:
                bedList.append(model)
    if lociOnly:
        bedList = lociBedDict.values()
    return bedList


def readBedToTree(bed_fh, resetScores = True, lociOnly = False):
    """
    Function: Reads and parses a BED file into a btree
    Return  : A dictionary of binary trees of GeneModels, seperated by seqName
    Args    : bed_fh (file handle) - the bed file to parse
              resetScore (bool) - reset the scores to 0
              lociOnly (bool) - whether to only process gene loci
    """
    i = 0
    bed_fh.seek(0)
    bedDict = {}
    bedTreeDict = {}
    lociBedDict = {}
    #An optimization technique:
    bedDictAddItem = bedDict.setdefault

    #Read all of the bed file into models and into a dictionary for each sequence
    for line in bed_fh:
        i += 1
        if len(line) < 2: #ignore empty strings
            continue
        if line.startswith('track'):
            continue
        try:
            model = Bed.bedline2feat(line, reset_scores=resetScores)

        except Exception, exc: #TODO: Update exception handling
            handleExceptionAtLine(exc, "BED", i)
            exit(2)
        else:
            seqName = model.getSeqname()
            if lociOnly: #are we checking only loci?
                lociId = displayLoci.search(model.getDisplayId()).group()
                seqLoci = lociBedDict.setdefault(seqName,{}) #seqName dict of loci dict
                if lociId in seqLoci:
                    lModel = seqLoci[lociId]
                    #if this model begins before the one in the dict, match it
                    if model.getStart() < lModel.getStart():
                        lModel.setStart(model.getStart())
                    #if this model ends after than the one in the dict, match it
                    if model.getEnd() > lModel.getEnd():
                        lModel.setLength(model.getEnd() - lModel.getStart())
                    # add all of the features to the loci model
                    for feature in model.getSortedFeats("exon"):
                        lModel.addFeat(feature, safe=1)
                else:
                    model.setDisplayId(lociId)
                    seqLoci[lociId] = model
            else:
                bedDictAddItem(seqName,[]).append(model)

    if lociOnly:
        for seqName, lociDict in lociBedDict.iteritems():
            bedDict[seqName] = lociDict.values()

    #for each sequence, sort its genes by start value and generate a b-tree
    for seqName, geneList in bedDict.iteritems():
        geneList.sort(key=lambda gene: gene.getDisplayId())
        bedTreeDict[seqName] = btree.sortedListToBTree(geneList)

    for bedTree in bedTreeDict.itervalues():
        bedTree.joinTouchingFeats()

    return bedTreeDict



if __name__ == "__main__":
    sys.exit(main())

if __name__ == "__channelexec__":
    client()
    