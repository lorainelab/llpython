"""
This script will split a file into uniq (SM) and duplicate (MM) lines based only on a single column in the file
for example the following file with col = 1 and delim = \t
	1	2	3
	1	3	3
	1	2	3
	1	3	4
	1	1	4
	1	4	5
	1	5	6
will put the following in SM

	1	1	4
	1	4	5
	1	5	6
(because 1 4 and 5 are not repeated)

will put the following in MM
	1	2	3
	1	3	3
	1	2	3
	1	3	4
because 3 and 2 are repeated.


Takes Three arguments
	1 - a SORTED file to be split
	2 - a column number
	3 - a delimiter (default '\t')

usage:
>>> sort -k3 originalFile > sortedFile
>>> python uniqCol.py sortedFile 3 ,
"""

import sys

if __name__ == '__main__':
	oldName = ""
 	oldLine = ""
 	multi = False
	infile = sys.argv[1]
	col = int(sys.argv[2])
	
	if len(sys.argv) == 3:
		delim = '\t'
	else:
		delim = sys.argv[3]
  	handle = open(infile)
  	single_handle= open(infile + ".sm","w")
  	multi_handle = open(infile + ".mm","w")
 	for line in handle:
		templist =  line.split(delim)
		name = templist[col]#who we're uniquing on
		if oldName != "":
			# See if there are multiple lines with the same accession name
			if multi == False and oldName != name:
				single_handle.write(oldLine)
			else:
				multi_handle.write(oldLine)
		multi = (oldName == name)
		oldLine = line
		oldName = name

	# final line
	if multi == False:
		single_handle.write(oldLine)
	else:
		multi_handle.write(oldLine)
	handle.close()
	single_handle.close()
	multi_handle.close()

