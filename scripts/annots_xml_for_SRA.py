"""Create annots.xml text for maize reproductive tissue RNA-Seq data harvested from SRA"""

samples_txt = """SRR189760  SRR189762  SRR189764  SRR189766  SRR189769  SRR189771  SRR189773
SRR189761  SRR189763  SRR189765  SRR189768  SRR189770  SRR189772  SRR189774"""

srp = "SRP006463"
base = 'http://www.ncbi.nlm.nih.gov/sra?term='

d = {"SRR189760":"Pre-emergence cob",
     "SRR189761":"Leaves 20-day old seedling - field",
     "SRR189762":"Pre-pollination tassel",
     "SRR189763":"Post-emergence cob",
     "SRR189764":"Post-pollination cob",
     "SRR189765":"Seed 5 days after pollination",
     "SRR189766":"Embryo 25 days after pollination",
     "SRR189768":"Endosperm 25 days after pollination",
     "SRR189769":"Whole anthers",
     "SRR189770":"Ovule",
     "SRR189771":"Pollen",
     "SRR189772":"Mature silk",
     "SRR189773":"Seed 10 days after pollination",
     "SRR189774":"Leaves 20-day old seedling - growth chamber"}

def makeAnnotsXML(fn="test.xml"):
    "Make annots.xml for these samples"
    fh = open(fn,'w')
    fh.write("<files>\n")
    for key in d.keys():
        fn = "/".join([srp,key+'.sm.bam'])
        title = 'RNASeq / SRP006463 Reproductive / Single-map reads / alignments %s %s'%(key,d[key])
        url = "%s%s"%(base,key) 
        txt = '<file name="%s" title="%s" url="%s"/>\n'%(fn,title,url)
        fh.write(txt)

        fn = "/".join([srp,key+'.sm.wig.gz'])
        title = 'RNASeq / SRP006463 Reproductive / Single-map reads / coverage %s %s'%(key,d[key])
        txt = '<file name="%s" title="%s"/>\n'%(fn,title)
        fh.write(txt)

        fn = "/".join([srp,key+'.mm.bam'])
        title = 'RNASeq / SRP006463 Reproductive / Multi-map reads / alignments %s %s'%(key,d[key])
        url = "%s%s"%(base,key)
        txt = '<file name="%s" title="%s" url="%s"/>\n'%(fn,title,url)
        fh.write(txt)
        
        fn = "/".join([srp,key+'.mm.wig.gz'])
        title = 'RNASeq / SRP006463 Reproductive / Multi-map reads / coverage %s %s'%(key,d[key])
        url = "%s%s"%(base,key) 
        txt = '<file name="%s" title="%s"/>\n'%(fn,title)
        fh.write(txt)
        
        fn = "/".join([srp,key+'.bed.gz'])
        title = 'RNASeq / SRP006463 Reproductive / Junctions / %s %s'%(key,d[key])
        url = "%s%s"%(base,key)
        txt = '<file name="%s" title="%s" url="%s"/>\n'%(fn,title,url)
        fh.write(txt)
    fh.write("</files>")
    fh.close()

        

        
        
    
     
     
