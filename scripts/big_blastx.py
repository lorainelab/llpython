"""A script that sets up and processes a big blastx search, running against multiple databases separately"""


import sys,os,re

BLASTDB="/lustre/groups/lorainelab/data/blueberry/09_blastx/2013-06-21"

scr = \
"""#!/bin/bash
#PBS -N big_blastx
#PBS -l nodes=1:ppn=6
#PBS -l vmem=16000mb
#PBS -l walltime=10:00:00
cd $PBS_O_WORKDIR\n\n"""


# blastall -p blastx -b 1 -v 1 -i ${PBS_ARRAYID}.fa -o ${PBS_ARRAYID}.At.xml -e 0.
# 1 -m 7 -d Arabidopsis_thaliana.aa.fa

def parseBigBlastx(d='.',outfn='big_blastx_results.tsv',target_dbs=None):
    """
    Function: Parse the blast results, make a big file with results
    Returns : 
    Args    : d - directory containing tsv and fa files from running
                  this big blastx
              outfn - name of results file to write
              target_dbs - list of target databases, otput from 
                           getTargetDbs 
    """
    files = os.listdir(d)
    fasta_files = filter(lambda x:x.endswith('.fa'),files)
    nums = map(lambda x:int(x.split('.')[0]),fasta_files)
    nums.sort()
    outfh = None
    for num in nums:
        prefix=d+os.sep+str(num)+'.'
        for target_db in target_dbs:
            fname = prefix + target_db + '.tsv'
            fh=open(fname)
            lines=fh.readlines()
            fh.close()
            (heads,results)=parseTsvLines(lines)
            if not outfh:
                outfh = open(outfn,'w')
                header='\t'.join(heads)+'\n'
                outfh.write(header)
            for result in results:
                line='\t'.join(result)+'\n'
                outfh.write(line)
    outfh.close()

def countNumWithHits(fn='big_blastx_results.tsv',max_evalue=0.00001):
    """
    Function: Read big blastx file (written in parseBigBlastx) and count number of 
              query ids with hits.
    Returns : 
    Args    : fn - name of file with big blastx results
              max_evalue - only consider hits with evalue at least as small as this
    """
    query_ids={}
    fh = open(fn,'r')
    heads=fh.readline().rstrip().split('\t')
    head2index={}
    i = 0
    for head in heads:
        head2index[head]=i
        i=i+1
    while 1:
        line=fh.readline()
        if not line:
            fh.close()
            break
        toks=line.rstrip().split('\t')
        query_id=toks[head2index['q.id']]
        if not query_ids.has_key(query_id):
            query_ids[query_id]=0
        evalue=toks[head2index['evalue']]
        if evalue!='NA' and float(evalue)<=max_evalue:
            query_ids[query_id]=1
    return query_ids
                    
def parseTsvLines(lines):
    "Parse a single tsv results file"
    indexes=range(0,len(lines))
    starts=filter(lambda x:lines[x].startswith('# BLASTX'),indexes)
    field_index = 0
    for line in lines:
        if line.startswith('# Fields:'):
            break
        field_index=field_index+1
    #sys.stderr.write('field_index: %i\n'%field_index)
    fields=reformatFields(lines[field_index])
    #sys.stderr.write(','.join(fields))
    results=parseQueryHits(starts,lines,len(fields))
    return (fields,results)

def parseQueryHits(indexes,lines,N_fields):
    "Get all the hits. Return lists of NA for queries with no hits."
    results=[]
    for index in indexes:
        query_id=lines[index+1].split('Query: ')[1].rstrip()
        database=lines[index+2].split('Database: ')[1].rstrip()
        if lines[index+3]=='# 0 hits found\n':
            result=map(lambda x:'NA',range(1,N_fields+1))
            result[0]=database
            result[1]=query_id
            results.append(result)
        else:
            for line in lines[index+5:len(lines)]:
                if line.startswith('#'):
                    break
                result=[database]
                vals=line.rstrip().split('\t')
                result=result+vals
                results.append(result)
    return results
                       
def reformatFields(fields):
    fields=fields.rstrip().split('# Fields: ')[1]
    fields=fields.split(', ')
    fields=map(lambda x:re.sub('subject','s.',x),fields)
    fields=map(lambda x:re.sub('query','q.',x),fields)
    fields=map(lambda x:re.sub('% ','',x),fields)
    fields=map(lambda x:re.sub(' ','.',x),fields)
    fields=map(lambda x:re.sub('\.\.','.',x),fields)
    return ['database']+fields

def makeTopHitsTable(d=None,fn=None):
    """
    Function: Create a table reporting the number of top hits per species
    Returns : d - counts for individual speces
    Args    : d - dictionary, output from getTopHits
              fn - string, if provided, writes to this file. Otherwise, writes
                   to stdout
    """
    newd = {}
    for species in target_dbs:
        newd[species]=0
    for query_id in d.keys():
        champion = None
        subd = d[query_id]
        for species in subd.keys():
            lst = subd[species]
            if not champion:
                champion = lst
            else:
                if lst[0]>champion[0]:
                    champion = lst
        newd[champion[0]]=newd[champion[0]]+1
    if fn:
        fh = open(fn,'w')
    else:
        fh = sys.stdout
    fh.write("species\tcontigs\n")
    for species in target_dbs:
        fh.write("%s\t%i\n"%(species,newd[species]))
    if fn:
        fh.close()
    return newd
    

def checkResults(d="fastas"):
    """
    Function: Check that all the results files are present
    Returns : list - if all are done, list is empty, otherwise,
                     returns the list of uncompleted files
    Args    : d - place to check for results
    """
    some_missing = []
    files = os.listdir(d)
    fasta_files = filter(lambda x:x.endswith('.fa'),files)
    for fasta_file in fasta_files:
        prefix = fasta_file.split('.')[0]
        for target in target_dbs:
            tname = '_'.join(target.split())
            fn = d + os.sep + prefix+'.'+tname+'.tsv'
            try:
                fh = open(fn,'r')
            except IOError:
                sys.stderr.write("Can't find: %s\n"%fn)
                some_missing.append(fn)
    return some_missing
    
def makeClusterScript(fn='big_blastx.sh',ds=None,
                      BLASTDB=BLASTDB):
    """
    Function: Write script to run via qsub
    Returns : 
    Args    : fn - string, name of file to write
              ds - list of databases (e.g., 'Arabidopsis_thaliana','Vitis_vinifera')
              blastdb - path to blast databases (e.g., /lustre/groups/lorainelab/blastdb)

    ex) 
    ds - target_db (see above)
    BLASTDB - /lustre/groups/lorainelab/data/blueberry/09_blastx/2013-06-21\n\n

    Then you can run it like so:

    qsub -t 0-9 big_blastx.sh 2> big_blastx.err 1>big_blastx.out 

    See also:

    https://www.rc.colorado.edu/book/explort/html/199
    """
    fh = open(fn,'w')
    fh.write(scr)
    fh.write("export BLASTDB=%s\n"%BLASTDB)
    for d in ds:
        fh.write('blastx -query ${PBS_ARRAYID}.fa -out ${PBS_ARRAYID}.%s.tsv -db %s -evalue 0.01 -max_target_seqs 1 -num_threads 6 -outfmt "7 qseqid qlen qframe qstart qend slen sstart send qcovs qcovhsp pident evalue bitscore ssid stitle"\n'%(d,d))
    fh.close()

                 
def splitFasta(fn="Both_1_out.unpadded.fasta",N=500,d='fastas'):
    """
    Function: Split up the contig fasta file into a bunch of littler
              files containing N or fewer records
    Returns : 
    Args    : fn - string, name of fasta file
              N  - positive int, number of records to include per file
              d  - string, name of directory where littler fasta files will go

    Each little file is named [number].fa
    """
    from Bio import SeqIO
    if N <= 0:
        raise Exception("N is not positive: %i"%N)
    fh = open(fn,'r')
    seqs = []
    i = 1
    for rec in SeqIO.parse(fh,'fasta'):
        seqs.append(rec)
        if len(seqs)==N:
            writeEm(seqs=seqs,i=i,
                    d=d,writer=SeqIO)
            i = i + 1
            seqs = []
    if len(seqs)>0:
        writeEm(seqs=seqs,i=i,d=d,writer=SeqIO)
    fh.close()
        
def writeEm(seqs=None,i=None,d=None,writer=None):
    """
    Function: Write the given list of sequences to a fasta file
    Returns : 
    Args    : seqs - a set of sequence records to write
              i - prefix for the file (e.g., [number].fa where i is the number)
              d - name of directory where we write the file
    """
    fn = '%s%s%i.fa'%(d,os.sep,i)
    fh = open(fn,'w')
    writer.write(seqs,fh,'fasta')
    fh.close()

def getTargetDbs(BLASTDB=None):
    files=os.listdir(BLASTDB)
    target_dbs=[]
    for f in files:
        if f.endswith('.phr'):
            db=f[:-4]
            target_dbs.append(db)
    return target_dbs
    
# doc string format guide
def zdummy():
    """
    Function: description
    Returns : type, description
    Args    : arg1 - type, description, output from method1
              arg2 - type, description, output from method2
              etc.
    """
    pass
