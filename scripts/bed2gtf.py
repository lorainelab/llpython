#!/usr/bin/env python

"Convert bed to GTF"

import Mapping.Parser.Bed as b
import Utils.General as utils
import optparse,sys,re

def readBed(fh=None,producer=None):
    "Read bed and produce gene model objects."
    feats = []
    while 1:
        line = fh.readline()
        if not line:
            break
        else:
            feat = b.bedline2feat(line,producer=producer)
            feats.append(feat)
    return feats

def feats2gtf(feats=None,gene_field=None,fh=None):
    "Write feats in GTF. Ignore CDSs."
    if gene_field==4:
        reg=re.compiled('\.[0-9]+')
    for feat in feats:
        producer=feat.getProducer()
        seqname = feat.getSeqname()
        name = feat.getDisplayId()
        gene_name=name
        if gene_field==4:
            gene_name=reg.sub('',name)
        elif gene_field==13:
            gene_name=feat.getVal('symbol')
        strand=feat.getStrand()
        if strand==1:
            strand = '+'
        elif strand == -1:
            strand = '-'
        else:
            strand = '.'
        extra_feat = 'gene_id "%s"; transcript_id "%s";'%(gene_name,name);
        exons = feat.getFeats(feat_type='exon')
        for exon in exons:
            start = str(exon.getStart()+1)
            end = str(exon.getStart()+exon.getLength())
            line = '\t'.join([seqname,producer,'exon',start,end,'.',strand,'.',extra_feat])+'\n'
            fh.write(line)
    fh.close()

def main(options,args):
    bed_file = args[0]
    producer = options.producer
    gene_field=options.gene_field
    if bed_file == '-':
        fh = sys.stdin
    else:
        fh = utils.readfile(bed_file)
    feats = readBed(fh=fh,producer=producer)
    if fh != sys.stdin:
        fh.close()
    if len(args)>1:
        gtf_file = args[1]
        fh = open(gtf_file,'w')
    else:
        fh = sys.stdout
    feats2gtf(feats=feats,gene_field=gene_field,fh=fh)
    if fh != sys.stdout:
        fh.close()

if __name__ == '__main__':
    usage = "%prog [bed_file] [gtf_file]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-p","--producer",dest="producer",
                      help="Producer (source) field for GTF file",
                      default="BED2GTF")
    parser.add_option("-g","--gene_field",dest="gene_field",type='int',
                      help="Field with gene id, default is 4",default=4)
    (options,args)=parser.parse_args()
    main(options,args)
