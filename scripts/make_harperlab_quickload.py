#!/usr/bin/env python

"""Make annots.xml for Harper lab pollen QuickLoad site."""

import sys,os
import make_annots_xml as mk

# color for control, wildtype
cw='336600'
# color for control, mutant
cm='008B8B'
# color for treatment, wildtype
tw='9900FF' 
# color for treatment, mutant
tm='8B2323'
b="FFFFFF"
u='A_thaliana_Jun_2009/AtPollen_processed'

samples=[['CW1','CW1',cw,b,u],
         ['CW2','CW2',cw,b,u],
         ['CW3','CW3',cw,b,u],
         ['CM1','CM1',cm,b,u],
         ['CM2','CM2',cm,b,u],
         ['CM3','CM3',cm,b,u],         
         ['TW1','TW1',tw,b,u],
         ['TW2','TW2',tw,b,u],
         ['TW3','TW3',tw,b,u],
         ['TM1','TM1',tm,b,u],
         ['TM2','TM2',tm,b,u],
         ['TM3','TM3',tm,b,u]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder='Harperlab / Hot Pollen',
                                deploy_dir=u.split(os.sep)[-1],
                                all=False)
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()

