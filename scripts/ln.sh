#!/bin/bash
D=${1:-".."}
FILES=$(ls $D/*FJ.bed.gz)
for FILE in $FILES
do
    echo $FILE
    ln -s $FILE .
done