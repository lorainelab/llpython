#!/usr/bin/env python


import sys,os,optparse

m = '/storage/lorainelab/illumina/mod_chromInfo.txt'

def run(cmd):
    sys.stderr.write("running: %s\n"%cmd)
    os.popen(cmd)



if __name__ == '__main__':
    usage = "usage: %prog [PATH/accepted_hits.sam]\n\nex) find . -name  accepted_hits.sam | xargs -I FILE %prog FILE"
    parser = optparse.OptionParser(usage=usage)
    (options,args)=parser.parse_args()
    if len(args)>=1:
        arg=args[0]
        if arg.endswith('accepted_hits.sam'):
            d = '/'.join(arg.split('/')[:-1])
            cwd = os.getcwd()
            os.chdir(d)
            sample = os.getcwd().split('/')[-1]
            cmd = 'countReads.py --in accepted_hits.sam --out %s.counts.txt'%sample
            run(cmd)
            cmd = 'samtools import %s accepted_hits.sam accepted_hits.bam'%m
            run(cmd)
            cmd = 'samtools sort accepted_hits.bam %s'%sample
            run(cmd)
            cmd = 'samtools index %s.bam'%sample
            run(cmd)
            cmd = 'rm accepted_hits.bam'
            run(cmd)
            cmd = 'rm accepted_hits.sam'
            run(cmd)
            cmd = "sed 's/TopHat/%s/g' junctions.bed | sed 's/=junctions/=%s_juncs/g' > %s.bed"%(sample,sample,sample)
            run(cmd)
            cmd = 'gzip %s.bed'%sample
            run(cmd)
            cmd = 'rm junctions.bed'
            run(cmd)
            cmd = "sed 's/TopHat/%s/g' coverage.wig > %s.wig"%(sample,sample)
            run(cmd)
            cmd = "gzip %s.wig"%sample
            run(cmd)
            cmd = 'rm coverage.wig'
            run(cmd)
            cmd = 'rm *.juncs'
            run(cmd)
            os.chdir(cwd)
    
