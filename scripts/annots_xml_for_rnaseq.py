#!/usr/bin/env python

import os,optparse,sys


# http://www.ncbi.nlm.nih.gov/sra/SRP007424?&report=full
# http://en.wikipedia.org/wiki/FASTQ_format
# wget -nd -nH -r -P SRP007424 ftp://ftp-trace.ncbi.nlm.nih.gov/sra/sra-instant/reads/ByStudy/sra/SRP/SRP007/SRP007424

# this study investigates parent of origin gene expression - wow!
SRP007424 = """SRR307067\tGSM756821: Col x Ler embryo RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082186?report=full
SRR307068\tGSM756821: Col x Ler embryo RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082186?report=full
SRR307069\tGSM756822: Col x Ler endosperm RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082187?report=full
SRR307070\tGSM756822: Col x Ler endosperm RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082187?report=full
SRR307071\tGSM756823: Ler x Col embryo RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082188?report=full
SRR307072\tGSM756823: Ler x Col embryo RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082188?report=full
SRR307073\tGSM756823: Ler x Col embryo RNA-seq\thttp://www.ncbi.nlm.nih.gov/sra/SRX082188?report=full

SRR307074\tSRR307074\tLib endosperm 6-7 days after pollination Ler x Col endosperm RNA-seq\thttp://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?cmd=viewer&m=data&s=viewer&run=SRR307074
SRR307075\tSRR307075\tendosperm 6-7 days after pollination Ler x Col endosperm RNA-seq\thttp://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?cmd=viewer&m=data&s=viewer&run=SRR307075
SRR307076\tSRR307075\tendosperm 6-7 days after pollination Ler x Col endosperm RNA-seq\thttp://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?cmd=viewer&m=data&s=viewer&run=SRR307076"""

SRP003234 = """
SRR064150\thttp://www.ncbi.nlm.nih.gov/sra/SRR064149
SRR064151
SRR064152
SRR064153
SRR064154
SRR064155
SRR064156
SRR064157
SRR064158
SRR064159
SRR064160
SRR064161
SRR064162
SRR064163
SRR064164
SRR064165\tflower stage 4\thttp://www.ncbi.nlm.nih.gov/sra/SRX025411
SRR064166
SRR064167"""

SRP0077511="""SRR314813
SRR314814
SRR314815
SRR314816
SRR314817
SRR314818"""

def makeSample(txt):
    "I bet there is a way to get this via eUtils"
    pass

class Sample():
    "A class representing a sample"

    def __init__(self,vals):
        if len(vals)>=0:
            self.prefix=vals[0]
        if len(vals)>=2:
            self.sample_title=vals[1]
        if len(vals)>=3:
            self.url=vals[2]
            
class Samples():
    "A class encapsulating information about a collection of samples from an RNASeq experiment."

    def __init__(self,lst):
        self._lst = lst
        self.refresh()
        
    def __iter__(self):
        "Returns an iterator object. Uses yield function."
        for vals in self._lst:
            yield Sample(vals)

    def refresh(self):
        self._last=-1

    def next(self):
        if len(self._lst)<=self._last:
            raise StopIteration
        else:
            self._last+=1
            vals = self._lst[self._last]
            return Sample(vals)

drought_samples =  Samples([["WetDT2","Wet D"],
                            ["WetFT2","Wet F"],
                            ["DryBT2","Dry B"],
                            ["DryCT2","Dry C"],
                            ["DryET2","Dry E"]])

heat_samples = Samples([["CoolL1T1","Cool L1, control (3h, 22 deg)"],
                       ["CoolL2T1","Cool L2, control (3h, 22 deg)"],
                       ["HotI2T1","Hot I2, heat shock (3h, 37 deg)"],
                       ["HotK1T1","Hot K1, heat shock (3h, 37 deg)"],
                       ["HotK2T1","Hot K2, heat shock (3h, 37 deg)"],
                       ["CoolHT2","Cool H, control recovery (24h, 22 deg)"],
                       ["CoolL1T2","Cool L1, control recovery (24h, 22 deg)"],
                       ["HotI1T2","Hot I1, heat shock recovery (24h, 22 deg)"],
                        ["HotI2T2","Hot I2, heat shock recovery (24h, 22 deg)"],
                       ["HotK1T2","Hot K1, heat shock recovery (24h, 22 deg)"],
                       ["HotK2T2","Hot K2, heat shock recovery (24h, 22 deg)"]])

def deployFiles(deploy_dir=None,source_dir=None,samples=None):
    "Copy data files into source_dir for public deployment."
    for sample in samples:
        n = sample.prefix
        fns = [n +".bam",n+".bam.bai",
               n +".bedgraph.gz",n +".bedgraph.gz.tbi",
               n +".sm.bam", n +".sm.bam.bai",
               n +".sm.bedgraph.gz", n +".sm.bedgraph.gz.tbi",
               n +".mm.bam", n +".mm.bam.bai",
               n +".mm.bedgraph.gz", n +".mm.bedgraph.gz.tbi",
               n +".bed.gz", n +".bed.gz.tbi",
               n +".alignments_distribution.txt"]
        for fn in fns:
            deployFile(deploy_dir=deploy_dir,
                       source_dir=source_dir,
                       fn=fn)

def deployFile(deploy_dir=None,
               source_dir=None,
               fn=None):
    "Copy the give file from source_dir to deploy_dir"
    cmd = 'cp %s/%s %s/.'%(source_dir,fn,deploy_dir)
    code = os.system(cmd)
    if code != 0:
        raise OSError("Failed! Could not execute: %s"%cmd)

def makeFileTag(fn=None,title=None,descr=None,url=None):
    if not url:
        txt = '  <file\n    name="%s"\n    title="%s"\n    description="%s"\n  />\n'%(fn,title,descr)
    else:
        txt = '  <file\n    name="%s"\n    title="%s"\n    description="%s"\n    url="%s"\n  />\n'%(fn,title,descr,url)
    return txt

def makeHeatDroughtAnnotsXML():
    deploy_dir = "heat_drought"
    fh = open("annots.xml","w")
    fh.write("<files>\n")
    study_name = "Drought"
    folder = "RNASeq / %s"%study_name
    for s in drought_samples:
        writeFileTags(fh=fh,deploy_dir=deploy_dir,sample=s,folder=study_name)
    study_name = "Heat"
    folder = "RNASeq / %s"%study_name
    for s in heat_samples:
        writeFileTags(fh=fh,deploy_dir=deploy_dir,sample=s,folder=study_name)
    fh.write("</files>")
    fh.close()

def makeAnnotsXML(fn=None,samples=None,deploy_dir=None,study_name=None):
    "Make annots.xml for these samples."
    folder = "RNASeq / %s"%study_name
    if not fn:
        fh = sys.stdout
    else:
        fh = open(fn,'w')
    fh.write("<files>\n")
    for s in samples:
        writeFileTags(fh=fh,deploy_dir=deploy_dir,sample=s,folder=study_name)
    fh.write("</files>")
    if fn:
        fh.close()

def writeFileTags(deploy_dir=None,fh=None,sample=None,folder="RNASeq Study"):
    prefix = sample.prefix
    sample_title = sample.sample_title
    try:
        url = sample.url
    except AttributeError:
        url = None
    descr = "alignments for reads that mapped once onto the genome"
    fn = deploy_dir + os.sep + prefix + '.sm.bam'
    title = folder + " / SM / Reads / " + sample_title + ", alignments"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))

    descr = "coverage graph for reads that mapped once onto the genome"
    fn = deploy_dir + os.sep + prefix + '.sm.bedgraph.gz'
    title = folder + " / SM / Graph / " + sample_title + ", coverage"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))

    descr = "alignments for reads that mapped to many places in the genome"
    fn = deploy_dir + os.sep + prefix + '.mm.bam'
    title = folder + " / MM / Reads / " + sample_title + ", coverage"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))

    descr = "coverage graph for reads that mapped to many places in the genome"  
    fn = deploy_dir + os.sep + prefix + '.mm.bedgraph.gz'
    title = folder + " / MM / Graph / " + sample_title + ", coverage"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))
        
    descr = "alignments for ALL reads (multi- and single-mapping)"
    fn = deploy_dir + os.sep + prefix + '.bam'
    title = folder + " / ALL / Reads / " + sample_title + ", alignments"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))
        
    descr = "coverage graph for ALL reads (multi- and single-mapping)"
    fn = deploy_dir + os.sep + prefix + '.bedgraph.gz'
    title = folder + " / ALL / Graph / " + sample_title + ", coverage"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))
        
    descr = "Junctions predicted by TopHat"
    fn = deploy_dir + os.sep + prefix + '.bed.gz'
    title = folder + " / Juncs / " + sample_title + ", junctions"
    fh.write(makeFileTag(fn=fn,title=title,descr=descr,url=url))


def doIt(fn=None,source_dir=None,deploy_dir=None,
         copy_files=None,study_name=None,samples=None):
    makeAnnotsXML(fn=fn,deploy_dir=deploy_dir,study_name=study_name,samples=samples)
    if copy_files:
        deployFiles(deploy_dir=deploy_dir,source_dir=source_dir,samples=samples)
    
def main(options,args):
    "Do it all."
    copy_files = options.copy_files
    deploy_dir = options.deploy_dir
    source_dir=options.source_dir
    study_name=options.study_name
    if len(args)==0:
        outfn = None
    else:
        outfn = args[0]
    loraine_lab = options.loraine_lab
    if loraine_lab == 'heat':
            doIt(fn=outfn,source_dir=source_dir,copy_files=copy_files,
                 study_name="Heat",samples=heat_samples,deploy_dir=deploy_dir)
    elif loraine_lab == 'drought':
            doIt(fn=outfn,source_dir=source_dir,copy_files=copy_files,
                 study_name="Drought",samples=drought_samples,deploy_dir=deploy_dir)
    else:
        raise Error("No others supported yet")

 
if __name__ == '__main__':
    usage = "%prog [options] file.xml\n\nPrints XML to stdout if run with no arguments."
    parser = optparse.OptionParser(usage)
    parser.add_option("-p","--processed_dir",
                      help="Directory with processed files. Default is current working directory",
                      dest="source_dir",default=".")
    parser.add_option("-d","--deploy_dir",help="Directory where files will reside on QL site",
                      dest="deploy_dir",default=None)
    parser.add_option("-c","--copy_files",dest="copy_files",
                      help="Don't just make XML file. Copy files into deploy_dir.",
                      action="store_true")
    parser.add_option("-s","--study_name",dest="study_name",
                      help="Name of study or experiment. Will be shown as QL folder",
                      default="study")
    parser.add_option("-l","--loraine_lab",dest="loraine_lab",default="heat")
    (options,args)=parser.parse_args()
    if not options.deploy_dir:
        parser.error("Specify -d|--deploy_dir directory to deliver files.")
    main(options,args)



        
        
    
     
     
