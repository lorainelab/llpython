#!/bin/python

"""
reads a junction file output from TopHat together with a gene models bed file and then filters out all the novel junctions present in the TopHat file

The interface:

purgeJuncs.py -g [genes.bed] -t [tophat.bed] -n [noveljuncs.bed] -o [oldjuncs.bed]

where

genes.bed are the gene models (e.g.,  TAIR9_mRNA.bed.gz)

tophat.bed is the output from TopHat

noveljuncs.bed is just all the lines from the TopHat file containing junctions that are *not* already represented in the gene models file

oldjuncs.bed is just all the lines from the TopHat file containing junctions that are already represented in the gene models file

If -o is not given, purgeJuncs.py should print those lines to stdout.

If -t is not given, purgeJuncs.py should obtain those data from stdin.

If -n is not given, purgeJuncs.py should not bother to report the new junctions. 


The long form options:

-n becomes --novel-juncs
-o becomes --old-juncs
-t becomes --tophat

My approach.

Using the gene models I'm going to create a dictionary of all the introns in the shape of

{"chr#start-stop":"bed line"}

the value isn't important.

Next, I'll create a dictionary of all the junctions in the shape of:

{"chr#start-stop":"junction bed line"}

Finally, I'll loop through all the junctions and make a try statement
for key in junctions.keys():
	try:
		x = genes[key]
		write to found output
	except KeyError:
		write to novel output
	
"""
from optparse import OptionParser
import sys, gzip, re, zipfile

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.
    """
    reg = re.compile(r'\.gz$')
    reg2 = re.compile(r'\.zip$')
    if reg.search(f):
        z = gzip.GzipFile(f)
    elif zipfile.is_zipfile(f):
        return None
    else:
        z = open(f)
    return z
    
def parseFile(file):
	"""
	Reads a .bed file and creates a dictionary
	{"chr#start-stop":"bed line"}
	"""
	ret = {}
	for line in file.readlines():
		if line.startswith("track"):
			continue
		#get all blocks starts and stops
		#create a dictionary entry for all of them
		dat = line.split('\t')
		chrom = dat[0]
		chromStart = int(dat[1])
		sizes = dat[10].split(',')
		starts = dat[11].split(',')
		if dat[10].endswith(','):
			sizes = sizes[:-1]
			starts = starts[:-1]
		
		starts = map(lambda x:int(x)+chromStart,starts)
		sizes = map(lambda x:int(x), sizes)
		
		sortBlocks(starts,sizes)	
	
		for i in range(len(starts)-1):
			st1 = starts[i]
			sz1 = sizes[i]
			st2 = starts[i+1]
			
			ret[chrom+":"+str(st1 + sz1)+"-"+str(st2)] = line
	
	
	return ret
	
	
def sortBlocks(starts,lengths):
    """
    Function: ensures the block starts read from a bed file are in order with a simple bubble sort.
              This needs to be checked for order to make introns
    Args: starts - list of block starts
          lengths - list of block lengths (these need to be moved in order with starts
    Returns: None, all sorting is done in place
    """

    for i in range(0,len(starts)-1):
        swapMade = False#short circuiting. if no swaps, we can quit
        for j in range(0,len(starts)-1-i):
            if starts[j+1]<starts[j]:
                tempS = starts[j]
                tempL = lengths[j]
                starts[j] = starts[j+1]
                lengths[j] = lengths[j+1]
                starts[j+1] = tempS
                lengths[j+1] = tempL
                swapMade = True

        if not swapMade: break


	
if __name__ == '__main__':
	parser = OptionParser()
	
	parser.add_option("-n", "--novel-juncs", dest="novelJuncs", help="All the lines from the TopHat file containing junctions that are *not* already represented in the gene models file", metavar="FILE")
	parser.add_option("-g", "--genes", dest="genes", help=".bed file of the gene models to compare to", metavar="FILE")
	parser.add_option("-t", "--tophat", dest="tophat", help="Output from TopHat. Default reads from stdin", metavar="FILE")
	parser.add_option("-o", "--old-juncs", dest="oldJuncs", help="All the lines from TopHat file containing junctions that are already represented in the gene models file. Default is stdout.", metavar="FILE")
	
	(options, args) = parser.parse_args()
	
	novel = options.novelJuncs
	genes = options.genes
	old = options.oldJuncs
	tophat = options.tophat
	
	if novel != None:
		novel = open(novel,'w')
	
	if genes == None:
		sys.stderr.write("--genes must be provided!\n")
		sys.exit()
	else:
		genes = readfile(genes)
		
	if old == None:
		old = sys.stdout
	else:
		old = open(old, 'w')
	
	if tophat == None:
		tophat = sys.stdin
	else:
		tophat = readfile(tophat)
	
	genesDictionary = parseFile(genes)
	junctionsDictionary = parseFile(tophat)
	
	for key in junctionsDictionary:
		try:
			x = genesDictionary[key]
			old.write(junctionsDictionary[key])
		except KeyError:
			try:
				novel.write(junctionsDictionary[key])
			except AttributeError:
				pass
		
	sys.exit()