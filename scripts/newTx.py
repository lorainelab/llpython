#!/usr/bin/env python

"""
Functions for working with files and data sets produced from analysis of new transcription in the genome.
Mainly this is for Arabidopsis so many of the parameter defaults assume we are working with the June 2009
release of the Arabidopsis genome.

Uses BioPython.
"""

import sys,optparse,os
import Utils.General as u
import shutil

def getSeqs(fa='A_thaliana_Jun_2009.fa'):
    """
    Function: Read chromosome sequences into memory.
    Returns : Dictionary where keys are chromosome names and values
              are SeqRecords.
    Args    : fa - string, name of fasta file with sequence
    """
    from Bio import SeqIO
    d = {}
    for seq_record in SeqIO.parse(fa,'fasta'):
        d[seq_record.id]=seq_record
    return d

def addSeq(in_file='NovelRegions500bp.txt',
           out_file='NovelRegions500bpSequence.txt',
           d=None):
    """
    Function: Read file, grab sequence name, chromosome start, and
              chromosome end from in_file. Grab sequence from d and
              add it to the end of each line. Write out to out_file.
    Returns : 
    Args    : in_file - file with coordinates
                        chromosome name is field two
                        start is field three
                        end is field four
              out_file - file to write
              d - dictionary, output from getSeqs
    """
    fh = open(in_file,'r')
    outfh=open(out_file,'w')
    header = fh.readline().rstrip()
    outfh.write(header+'\t'+'sequence\n')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            outfh.close()
            break
        toks = line.rstrip().split('\t')
        chrom = toks[0]
        start = int(toks[1])
        end = int(toks[2])
        length = end-start
        seq = d[chrom]
        sequence=seq[start:end].seq.tostring()
        toks.append(sequence)
        outfh.write('\t'.join(toks)+'\n')
    sys.stderr.write("Done!\n")
        
def checkFile(fn):
    okay=False
    try:
        size = os.path.getsize(fn)
        if size > 0:
            okay = True
    except OSError:
        okay=False
    return okay

def copyOver(in_file="NovelRegions500Sequence.txt",
             old_dir="blastx400_xml",
             new_dir="blastx_xml"):
   fh=open(in_file)
   oldfiles=os.listdir(old_dir)
   newfiles=os.listdir(new_dir)
   i = 0
   while 1:
       line = fh.readline()
       if not line:
           fh.close()
           break
       name=line.split('\t')[4]
       newfile=name+'.xml'
       if not newfile in newfiles and newfile in oldfiles:
           sys.stderr.write("Can copy: %s\n"%newfile)
           i = i + 1
           shutil.copy(old_dir+os.sep+newfile,new_dir)
   sys.stderr.write("Can copy: %i\n"%i)
               
def runBlast(in_file="NovelRegions500Sequence.txt",
             testing=1,xml="blastx_xml",program="blastx",
             database="nr",hitlist_size=10,expect=0.001,
             length=None):
    """
    Function: Run blast on-line, save output to XML file.
    Returns : 
    Args    : in_file - tab-delimited file w/ sequence
                        final field should be the sequence
              xml - directory where XML files will be saved
              testing - stop after this many, or keep going until the
                        end of the file if None [default is 1]
    """
    from Bio.Blast import NCBIWWW
    i = 0
    fh = open(in_file)
    line = fh.readline() # header
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        sequence=toks[-1]
        name=toks[4]
        seq_length=len(sequence)
        if length and seq_length<length:
            continue
        outname=xml+os.sep+name+'.xml'
        if checkFile(outname):
            continue # no need to make it
        else: # need to make it
            outfh=open(outname,'w')
            sys.stderr.write("Running blast for: %s\n"%name)
            result_handle=NCBIWWW.qblast(program,database,sequence,
                                         hitlist_size=hitlist_size,
                                         expect=expect)
            txt=result_handle.read()
            outfh.write(txt)
            outfh.close()
        i = i + 1
        if testing and i==testing:
            fh.read()
    sys.stderr.write("Done %i.\n"%i)
    
def runBlastAndParse(in_file='NovelRegions500bpSequence.txt',
                     testing=1,outfile="blastxout.txt",xml='blastx_xml'):
    """
    Function: Run blast on-line, save output to XML file, parse
              XML file and save summarized result to a file. Only run
              the blast if the corresponding XML file does not exist.
    Returns : 
    Args    : in_file - tab-delimited file w/ sequence
                        final field should be the sequence
              out_file - file to write
              d - dictionary, output from getSeqs
              xml - directory where XML files will be saved
              testing - stop after this many, or keep going until the
                        end of the file if None [default is 1]
    """
    NA='NA'
    NOHIT='NOHIT'
    fh = open(in_file)
    from Bio.Blast import NCBIWWW
    from Bio.Blast import NCBIXML
    i = 0
    line = fh.readline() # header
    results_fh = open(outfile,'w')
    heads=['region','expect','percent.ident','subject.start','title']
    results_fh.write('\t'.join(heads)+'\n')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            results_fh.close()
            break
        toks = line.rstrip().split('\t')
        sequence=toks[-1]
        name=toks[4]
        outname=xml+os.sep+name+'.xml'
        if checkFile(outname):
            outfh=open(outname,'r')
        else: # need to make it
            outfh=open(outname,'w')
            sys.stderr.write("Running blast for: %s\n"%name)
            result_handle=NCBIWWW.qblast("blastn","nt",sequence)
            txt=result_handle.read()
            outfh.write(txt)
            outfh.close()
            outfh=open(outname,'r')
        blast_record = NCBIXML.read(outfh)
        outfh.close()
        gotone = 0
        for alignment in blast_record.alignments:
            for hsp in alignment.hsps:
                if hsp.expect<0.001:
                    # http://comments.gmane.org/gmane.comp.python.bio.general/3387
                    per_ident=100.*hsp.identities/hsp.align_length
                    vals=[name,str(hsp.expect),'%.2f'%per_ident,str(hsp.sbjct_start),alignment.title]
                    results_fh.write('\t'.join(vals)+'\n')
                    gotone=gotone+1
        if gotone==0:
            vals=[name,NOHIT,NOHIT,NOHIT,NOHIT]
            results_fh.write('\t'.join(vals)+'\n')
        i = i + 1
        if testing and i==testing:
            fh.read()
    sys.stderr.write("Done %i.\n"%i)
        
def spliceReads(in_file=None,out_file=None):
    if in_file:
        fh = u.readfile(in_file)
    else:
        fh = sys.stdin
    if out_file:
        outfh = open(out_file,'w')
    else:
        outfh = sys.stdout
    while 1:
        line = fh.readline()
        if not line:
            if in_file:
                fh.close()
            break
        toks = line.rstrip().split()
        blocks = toks[11]
        if len(blocks.split(','))>1:
            # it's a spliced read
            outfh.write(line)
    if out_file:
        outfh.close()

def countReads(in_file=None,out_file=None):
    if in_file:
        fh = u.readfile(in_file)
    else:
        fh = sys.stdin
    if out_file:
        outfh = open(out_file,'w')
    else:
        outfh = sys.stdout
    while 1:
        line = fh.readline()
        if not line:
            if in_file:
                fh.close()
            break
        toks = line.rstrip().split()
        seqname = toks[0]
        start = toks[1]
        end = toks[2]
        cmd = "samtools view %s:%s-%s %s"(seqname,
                                          start,
                                          end,
                                          bam)
        

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-i","--in_file",help="BED file to filter [optional]",dest="in_file",type="string",default=None)
    parser.add_option("-o","--out_file",help="BED file to write [optional]",dest="out_file",type="string",default=None)
    parser.add_option("-f","--function",help="Function to run. Options are readBed, makeLoci",default="spliceReads",
                      type="string",dest="func")
    parser.add_option("-b","--bam",help="count reads from the given comma-separated list of one or more bam file(s)",dest="bam",default=None)
    (options,args)=parser.parse_args()
    if options.func == 'spliceReads':
        readBed(in_file=options.in_file,
                out_file=options.out_file)
    elif options.func == "countReads":
        countReads(in_file=options.in_file,
                   out_file=options.out_file,
                   bam=options.bam)
    else:
        sys.stderr.write("No such option: %s\n"%options.func)
        sys.exit(1)
    
