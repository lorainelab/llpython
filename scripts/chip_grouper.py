"""
Functions, objects, and methods for grouping probe sets.

Generally speaking, when probe sets interrogate the same gene or
transcriptional unit, we assign them to the same group.

For example, the following protocal explains how to generate groups of
probe sets such that each group member interrogates a region of the same
gene:

Step One: Data Harvesting

First, we obtain the mapping of Affymetrix consensus sequences onto
genomic sequence from the UCSC Web site using their table browser.

Next, we obtain mRNA and RefSeq mRNA alignments from UCSC in 'bed'
format.

Step Two: Overlaps analysis

We then use the genomic coordinates to link up probe sets with targets.
We use overlaps between consensus features to assign targets into gene groups.
We use RefSeq ids to assign Entrez Gene ids to gene groups.

Note some mRNAs and probe set consensus sequences are not assigned to a
single genomic location. This is due typically the large amount of
sequence duplication that occurs in genomes.

Because we cannot resolve the genomic structure of these entities, we
drop them from consideration.

Most methods and functions by default assume we are in an 'analysis'
directory, which has a sister directory called annots (../annots) that
contains annotations and other data collected from various sources,
such as NCBI Entrez Gene, UCSC Santa Cruz Genome Bioinformatics,
Affymetrix, and so on.

Note that this module contains some residual code at the bottom of the
file which will need to be cleaned out at some point.
"""
import os,sys,re
import Mapping.Parser.BedReader as br
import Mapping.Grouper as gr
import Mapping.feat_utils as fu

import Utils.ssg_utils as su
import Utils.igb_utils as iu

sep1 = '\t'
sep2 = ','
pipe = '|'

# from Entrez Gene ftp site
g2acc = '../annots/gene2accession.gz'

# fields in gene2accession
orgfield=0
accfield=3
genefield=1

# some useful constants used to create ChipGrouper objects
# note that a ChipGrouper is just a data structure that captures
# the names of files needed to perform the algorithm described
# in the text at the head of the file
fs = {
    'RAE230':['../annots/rn4.affyRAE230.bed.gz', # cons. alignments
                '../annots/rn4.knownGene.bed.gz', 
                'RAE230', # array code
                '10116', # taxid
                'rn4', # genome assembly
                '../annots/Rat230_2_probe_tab.gz', # probe sequences
                '../annots/rn4.affyRAE230.fa.gz', # cons seqs from alignments
                11], # number probes per probeset
      'MOE430':['../annots/mm8.MOE430.bed.gz',
                '../annots/mm8.knownGene.bed.gz',
                'MOE430',
                '10090',
                'mm8',
                '../annots/Mouse430_2_probe_tab.gz',
                '../annots/mm8.MOE430.fa.gz',
                11],
      'u74v2A':['../annots/mm7.u74v2.bed.gz',
                '../annots/mm7.knownGene.bed.gz',
                'u74v2A',
                '10090',
                'mm7',
                '../annots/MG-U74Av2_probe_tab.gz',
                '../annots/mm7.u74v2.fa.gz',
                16]
      }

def write_groups(groups,fn='groups.txt',chip_code=None):
    """
    Function: Writes out a file reporting grouping affiliations for
              probe sets
    Returns : 
    Args    : groups -
              fn - the name of the file to write
              chip_code - a code indicating the Affymetrix array
    """
    fh = open(fn,'w')
    fields = ['entrez','acc','ps','seqname','strand','start','end']
    fh.write(sep1.join(fields)+os.linesep)
    n = 0
    na = 'NA'
    for g in groups:
        entrez_id = g.display_id()
        if not entrez_id:
            entrez_id = repr(n)
            n = n - 1
        s = g.start()
        e = s + g.length()
        strand = g.strand()
        seqname = g.get_feats()[0].seqname()
        vals = [entrez_id,na,na,seqname,repr(strand),repr(s),repr(e)]
        fh.write(sep1.join(vals)+os.linesep)
        rna_feats = filter(lambda x:not x.producer()==chip_code,g.get_feats())
        ps_feats = filter(lambda x:x.producer()==chip_code,g.get_feats())
        for f in rna_feats:
            s = f.start()
            e = f.start()+f.length()
            acc = f.display_id()
            vals = [entrez_id,acc,na,na,na,repr(s),repr(e)]
            fh.write(sep1.join(vals)+os.linesep)
        for f in ps_feats:
            s = f.start()
            e = f.start()+f.length()
            ps = f.display_id().split(':')[-1]
            vals = [entrez_id,na,ps,na,na,repr(s),repr(e)]
            fh.write(sep1.join(vals)+os.linesep)
    fh.close()

class ChipGrouper:
    """A class with data and methods needed for grouping features"""

    def __init__(self,chip_code=None):
        if chip_code:
            self.setup(fs[chip_code])
        pass

    def setup(self,vals):
        self._chip_cons=vals[0]
        self._known_gene = vals[1]
        self._chip_code=vals[2]
        self._org_code=vals[3]
        self._genome_code=vals[4]
        self._probe_seqs = vals[5]
        self._cons_fasta = vals[6]
        self._nprobes = vals[7]
        
    def get_nprobes(self):
        """
        Function: 
        Returns : Number of probes in a probe set 
        Args    : 
        """
        return self._nprobes
    
    def get_chip_cons(self):
        """
        Function: 
        Returns : Name of the chip 'bed' consensus alignment file from UCSC
        Args    : 
        """
        return self._chip_cons

    def get_probe_file(self):
        """
        Function: 
        Returns : Name of the chip probe tab-delimited file with probe
                  sequences (download this from Affymetrix)
        Args    : 
        """
        return self._probe_seqs

    def get_cons_fasta(self):
        """
        Function: 
        Returns : Name of the chip consensus sequence fasta file
                  containing concatenated genomic sequence blocks defined
                  in the 'bed' file
        Args    : 
        """
        return self._cons_fasta

    def get_knownGene(self):
        """
        Function: 
        Returns : Name of the knownGene 'bed' alignment file from UCSC
        Args    : 
        """
        return self._known_gene

    def get_chip_code(self):
        """
        Function: 
        Returns : Name of the array we're grouping, e.g., u133
        Args    : 
        """
        return self._chip_code

    def get_org_code(self):
        """
        Function: 
        Returns : Species code (from NCBI)
        Args    : 
        """
        return self._org_code

    def get_genome_code(self):
        """
        Function: 
        Returns : genome code (from genome.ucsc.edu)
                  e.g., hg17, mm7, etc.
        Args    : 
        """
        return self._genome_code
        
def rm_multis(feats):
    """
    Function: screen out features that map to multiple locations 
    Returns : 
    Args    : a list of features
    """
    d = {}
    for feat in feats:
        if d.has_key(feat.display_id()):
            d[feat.display_id()].append(feat)
        else:
            d[feat.display_id()]=[feat]
    lst = []
    for key in d.keys():
        if len(d[key])==1:
            lst.append(d[key][0])
    return lst

def add_producer(feats,g):
    for feat in feats:
        if not feat.producer():
            feat.producer(g.get_chip_code())

def get_chip_feats(chip_grouper,rm_multis=True):
    """
    Function: Get UCSC consensus alignment features
    Returns : 
    Args    : A ChipGrouper objects
    """
    f = chip_grouper.get_chip_cons()
    producer = chip_grouper.get_chip_code()
    consensus_feats = br.bed2feats(fname=f,producer=producer,
                                   has_orf=0,group_feat='transcript',
                                   sub_feat = 'exon',
                                   format='ucsc',skip_uns=1)
    if rm_multis:
        consensus_feats = rm_multis(consensus_feats)
    return consensus_feats

def get_mrna_feats(chip_grouper):
    f = chip_grouper.get_knownGene()
    producer = 'knownGene'
    feats = br.bed2feats(fname=f,producer=producer,
                         has_orf=1,group_feat='transcript',
                         sub_feat='exon',
                         format='ucsc',skip_uns=1)
    # feats = cleanup_feats(feats)
    return feats

def group_feats(feats):
    """
    Function: Group the features into ExonOverLap groups
    Returns : 
    Args    : a list of ExonOverlapGroups

    delegates to grouper in cvs module mapping
    """
    return gr.group_feats(feats,filter_multi_mappers=1)

def add_gene_id2feat(feat,acc2g):
    """
    Function: Add Entrez Gene id tags to the given feature object
    Returns : 
    Args    : feat - a Mapping.Feature.KeyVal object, usually a
                     Mapping.CompoundDNASeqFeature representing an mrna
                     alignment
              acc2g - output from get_acc2g

    Assumes that the given feature object will have an accession as the
    display_id, and that the accession would be something reported in Entrez
    Gene's gene2accession file.

    Note also that some accessions may be associated with more than one
    Entrez Gene id.
    """
    fid = feat.display_id()
    if acc2g.has_key(fid):
        gids = acc2g[fid]
        feat.set_key_val('NCBI:EntrezGene',gids)

def add_gene_ids2groups(groups,acc2g=None):
    """
    Function: add entrez ids (if known) to each feature
              sets display_id to the numeric Entrez Gene id
    Example : add_gene_ids2groups(groups,acc2g=acc2g)
              named_genes = filter(lambda x:x.display_id(),groups)
              named_genes[0].display_id() # returns Entrez Gene id
    Returns :     
    Args    : groups - a list of Mapping.Grouper.ExonOverlapFeatureGroup objects

    Assumes that the groups are comprised of some features that have key value
    pairs with keys 'NCBI:EntrezGene' and values as lists of Entrez Gene ids.
    Typically, these will be mRNA accessions from GenBank.

    One complication is that sometimes a single mRNA accession is associated
    with multiple Entrez Gene ids. In addition, some mRNAs in a group may
    be associated with different Entrez Gene ids. Therefore, it is not always easy or
    feasible to pick a single Entrez Gene id to represent a group of overlapping
    mRNAs. Therefore, we handle this by identifying the most commonly found Entrez
    Gene id among all the mRNAs in a group and select that one most popular
    id to represent them all.
    """
    for group in groups:
        # we use this to get the most commonly used Entrez Gene id
        allgids = {}
        for feat in group.get_feats():
            gids = feat.get_val('NCBI:EntrezGene')
            if not gids and acc2g.has_key(feat.display_id()):
                gids = acc2g[feat.display_id()]
                feat.set_key_val('NCBI:EntrezGene',gids)
            if gids:
                for gid in gids:
                    if not allgids.has_key(gid):
                        allgids[gid]=1
                    else:
                        allgids[gid]=allgids[gid]+1
        if len(allgids.keys())==1:
            # if there's only one, we're done
            group.display_id(allgids.keys()[0])
            continue
        if len(allgids.keys())>1:
            maxgid = None
            for gid in allgids.keys():
                if not maxgid:
                    maxgid = gid
                else:
                    if allgids[gid]>allgids[maxgid]:
                        maxgid = gid
            group.display_id(maxgid)

def get_acc2g(f='../annots/gene2accession.gz',org=None):
    """
    Function: Get a mapping of accessions to gene ids 
    Returns : a dictionary where accessions are keys and lists of entrez gene ids
              are values
              Note that in some cases, there can be multiple NCBI Entrez Gene ids
              for a given mRNA accession.
    Args    : f - name of the file mapping entrez gene ids to accessions
                  gene2accession
              org - organism code
              
    """
    if not org:
        sys.stderr.write("Warning: require org_code, e.g., 9606 for human." + \
                         os.linesep)
        return None
    fh = su.readfile(g2acc)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            break
        toks = line.split(sep1)
        orgtok = toks[orgfield]
        if orgtok == org:
            g = toks[genefield]
            acc = toks[accfield]
            if not acc == '-':
                acc = acc.split('.')[0]
                if d.has_key(acc) and not g in d[acc]:
                    d[acc].append(g)
                else:
                    d[acc]=[g]
    return d

def list_clean_pss(groups,fn=None,chip_code=None):
    """
    Function: Report on cleaned-up probesets
    Returns :
    Args    : groups - output from do_chip
              fn - name of the file to write (if None,
                   writes to stderr)
              chip_code - same as used in do_chip run
    """
    heads = ['Entrez.Gene','probeset','array']
    if fn:
        fh = open(fn,'w')
    else:
        fh = sys.stderr
    total = 0
    fh.write(sep1.join(heads)+'\n')
    for group in groups:
        pss = filter(lambda x:x.producer()==chip_code,group.get_feats())
        if pss and len(pss)>0:
            gid = group.display_id()
            if not gid:
                gid = 'NA'
            for ps in pss:
                did = ps.display_id().split(':')[-1]
                vals = [gid,did,chip_code]
                fh.write(sep1.join(vals)+'\n')
                total = total + 1
    if fn:
        fh.close()
    sys.stderr.write("Total cleaned probesets for: " + chip_code + \
                     " " + repr(total)+"\n")

def check_bounds(feats):
    """
    Function: Checks for features that might have overlapping exons.
    Returns : a list of features with overlapping exon subfeatures
    Args    : a list Mapping.Feature.CompoundSeqFeature objects
    """
    baddies = filter(lambda x:fu.check_exon_bounds(x),feats)
    return baddies

def clean_probesets(chip_code=None,probesets_list=None,fn='Supp2.txt',
                    chip_feats=None):
    """
    Function: Examines each probe set listed in probesets_list and
              classifies them according to whether the probes are
              there, whether the probe set maps to multiple genomic
              locations, etc.
    Returns : 
    Args    : chip_code - e.g., MOE430 (must be listed in fs)
              probesets_list - a file listing probe set names
              fn - name of the file to write

    To generate probesets_list file, do something like this:

    cut -f1 Mouse430_2_probe_tab | sort | uniq | grep -v 'Probe Set Name' > Mouse430_2_probesets.txt
    
    """
    fh = open(probesets_list,'r')
    d = {}
    lst = []
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        ps = line.rstrip()
        d[ps]=[]
        lst.append(ps)
    g = ChipGrouper(chip_code=chip_code)
    if not chip_feats:
        # we want everything, so we can tell multi-mappers apart from no-mappers
        chip_feats = get_chip_feats(chip_grouper,rm_multis=False)
    d2 = {}
    for chip_feat in chip_feats:
        did = chip_feat.display_id().split(':')[1]
        if d2.has_key(did):
            d2[did].append(chip_feat)
        else:
            d2[did]=[chip_feat]
    for ps in lst:
        if ps.startswith('AFFX'):
            d[ps].append('C')
        elif d2.has_key(ps):
            if len(d2[ps])==1:
                d[ps].append('SM')
            else:
                d[ps].append('MM')
        else:
            d[ps].append('NM')
    if not chip_feats[0].get_val('seq'):
        iu.add_fasta(fn=g.get_cons_fasta(),feats=chip_feats,
                     chip_code=g.get_chip_code(),skip_uns=1,
                     reporter=sys.stderr)
    if not chip_feats[0].get_val('probe_coords'):
        iu.add_probe_coords(fn=g.get_probe_file(),feats=chip_feats)
    for ps in lst:
        if not d2.has_key(ps):
            d[ps].append('NA')
        else:
            if len(d2[ps])>1:
                d[ps].append('NA')
            else:
                chip_feat = d2[ps][0]
                num_probes = len(chip_feat.get_val('probe_coords').keys())
                d[ps].append(repr(num_probes))
    fh = open(fn,'w')
    heads = ['probe set','map','probes']
    fh.write(sep1.join(heads)+'\n')
    for ps in lst:
        fh.write(ps+sep1+sep1.join(d[ps])+'\n')
    fh.close()
    return d
    
    
def do_chip(chip_code=None,report=None,acc2g=None):
    """
    Function: Run grouping algorithm on the given chip.
    Returns : Grouped mRNAs and probe set consensus sequences, w/
              Entrez Gene ids attached, if available.
    Args    : chip_code - one of the keys in fs, an array identifier,
                          e.g., MOE430
              report - write a report to this filename, or stderr
                       if not given

    Returned groups contain mRNA and probeset consensus alignments
    The probeset alignments contain the location of probes relative
    to the sequence.
    """
    if not report:
        reporter = sys.stderr
    else:
        reporter = open(report,'w')
    reporter.write("Starting: " + chip_code + os.linesep)
    g = ChipGrouper(chip_code=chip_code)
    chip_feats=get_chip_feats(g) # removes multi-mappers
    if g.get_org_code()=='9606':
        chip_feats = filter(lambda x:not x.seqname().startswith('chr6_hla'),
                            chip_feats)
    reporter.write("Finished making array features: " + \
                     repr(len(chip_feats))+os.linesep)
    iu.add_fasta(fn=g.get_cons_fasta(),feats=chip_feats,
                 chip_code=g.get_chip_code(),skip_uns=1,
                 reporter=reporter)
    reporter.write("Finished adding fasta sequence."+os.linesep)
    n = len(filter(lambda x:x.get_val('seq'),chip_feats))
    # this should be equal to the number of chip_feats, since we got them
    # from the genome alignments in the first place
    reporter.write(repr(n) + " have consensus sequences from genomic."\
                     + os.linesep)
    iu.add_probe_coords(fn=g.get_probe_file(),feats=chip_feats)
    chip_feats = iu.filter_cons(chip_feats,nprobes=g.get_nprobes())
    trimmed_chip_feats = iu.trim_cons_feats(chip_feats,probe_length=25) # assumes 25 bp probes
    iu.feats2link(trimmed_chip_feats,fn=g.get_chip_code()+'.trimmed.link.psl',genome=g.get_genome_code())
    (mrna_feats,groups,acc2g) = proc_mrnas(grouper=g,reporter=reporter,acc2g=None)
    # this next function is DOG SLOW
    not_added = add_to_groups(feats=trimmed_chip_feats,groups=groups)
    leftovers = group_feats(not_added)
    groups = groups + leftovers
    # fn = g.get_chip_code()+'.'+g.get_genome_code()+'.pss.txt'
    # write_re_pss(groups,fn=fn,g=g)
    # reporter.write("Done: " + fn+os.linesep)
    return (groups,g)

def proc_mrnas(grouper=None,reporter=None,acc2g=None):
    """
    Function: Process mRNA features as a prelude for adding chip
              features to groups.
    Returns : a list - item 0 is a list of mRNA features
                       item 1 is the list of groups
                       item 2 is g2acc - output from 
    Args    : grouper - a chip_grouper.ChipGrouper object
              reporter - an open filehandle for writing [optional, default is sys.stderr]
              acc2g - output from get_acc2g [optional]
    """    
    mrna_feats =  get_mrna_feats(grouper)
    if grouper.get_org_code()=='9606':
        mrna_feats = filter(lambda x:not x.seqname().startswith('chr6_hla'),
                            mrna_feats)
    if not reporter:
        reporter = sys.stderr
    reporter.write("Finished making mrna features: " + \
                     repr(len(mrna_feats))+os.linesep)
    groups = group_feats(mrna_feats)
    if not acc2g:
        acc2g = get_acc2g(f=g2acc,org=grouper.get_org_code())
    # this will set the group's display id to the majority Entrez Gene id
    add_gene_ids2groups(groups,acc2g)
    map(lambda x:x.set_key_val('group_type','overlap'),groups)
    # this will change the attribute 'group_type' and set it to 'mixed_overlap'
    # if there are some features in a group that do not share the same
    # Entrez Gene id
    find_mixed_groups(fn=grouper.get_genome_code()+'.'+grouper.get_chip_code()+ \
                      '.mixed_groups.txt',groups=groups)
    return (mrna_feats,groups,acc2g)
    
def add_to_groups(feats=None,groups=None):
    """
    Function: Add the given feature objects to the given list of groups.
    Returns : 
    Args    : feats - a list of CompoundDNASeqFeature objects, e.g.,
                      trimmed chip feats
              groups - a list of groups, e.g., mrna_feats grouped by the
                       exon overlap rule (liberal)

    Note that we can potentially add a chip_feat to multiple groups, and
    some chip feats may never be added to a group.

    A further complication is that we don't allow a chip feature to be added
    to a group on the basis of its overlap with another chip_feat.
    """
    d = {}
    i = 0
    for group in groups:
        d[group]=[]
    sys.stderr.write("about to add: " + repr(len(feats)) + " feats to: " + \
                     repr(len(groups)) + " groups.\n")
    not_added = []
    for feat in feats:
        added = 0
        for group in groups:
            if not group.get_feats()[0].seqname()==feat.seqname():
                continue
            if not group.strand()==feat.strand():
                continue
            if group.belongs(feat):
                d[group].append(feat)
                added = 1
        if not added:
            not_added.append(feat)
            sys.stderr.write("couldn't add: " + feat.display_id()+"\n")
        i = i + 1
        if i % 10 == 0:
            sys.stderr.write("processed: " + repr(i) + " feats.\n")
    for group in d.keys():
        for feat in d[group]:
            group.add_feat(feat)
    return not_added
            
def do_MOE430():
    """
    Function: Group MOE430 probe sets and mouse knownGenes
    Returns : 
    Args    :

    invokes do_chip with chip code 'MOE430'
    """
    return do_chip(chip_code='MOE430')


def get_seqnames(fn):
    """
    Function: Find out what seqnames are represented in the given file
    Returns : a dictionary with feature counts
    Args    : name of a file to read
    """
    fh = su.readfile(fn)
    seqnames = {}
    while 1:
        line = fh.readline()
        if not line:
            break
        seqname=line.rstrip().split(sep1)[0]
        if not seqnames.has_key(seqname):
            seqnames[seqname]=1
        else:
            seqnames[seqname]=seqnames[seqname]+1
    fh.close()
    return seqnames

def do_u133():
    """
    Function: Group u133 chip set probe sets and human knownGenes
    Returns : 
    Args    :

    invokes do_chip with chip code 'u133'
    """
    return do_chip(chip_code='u133')

def write_re_pss(groups,fn=None,g=None):
    """
    Function: Write redundant probe sets for subsequent analysis.
    Returns : 
    Args    : groups - assembled groups
              fn - name of the file to write
              g - a Grouper object for an array

    Note that only gene groups with at least one probe set id will be
    reported.
    """
    fh = open(fn,'w')
    chip_code = g.get_chip_code()
    fh.write('# gene-to-probe set mappings for: ' + chip_code + \
             " genome: " + g.get_genome_code() + os.linesep)
    colon = ':'
    valss=[]
    for group in groups:
        gene_id = group.display_id()
        if not gene_id:
            gene_id = 'NA'
        acc = 'NA'
        mrnas = filter(lambda x:x.producer()=='knownGene',group.get_feats())
        if len(mrnas)>0:
            acc = mrnas[0].display_id()
        group_type = group.get_val('group_type')
        if not group_type:
            group_type = 'overlap'
        pss = filter(lambda x:x.producer()==chip_code,group.get_feats())
        if len(pss)>0:
            sys.stderr.write("Got some.\n")
        #ids = map(lambda x:colon.split(x.display_id()[:-1])[1],pss)
        ids = map(lambda x:re.sub(';','',x.display_id()),pss)
        # avoid printing letter ("A","B", etc) prefixes, an artifact from
        # how UCSC Genome Informatics names probeset alignments
        ids = map(lambda x:x.split(':')[-1],ids)
        if len(ids)>1:
            vals = [gene_id,acc,group_type] + ids
            valss.append(vals)
    valss.sort(lambda x,y:len(x)-len(y))
    for vals in valss:
        fh.write(sep1.join(vals[0:3])+sep2.join(vals[3:])+os.linesep)
    fh.close()

def tally_groups(groups,chip_code=None):
    """
    Function: Get information on redundant probe set counts
    Returns : a dictionary, keys are numbers of redundant probesets
              per group, values are counts
    Args    : groups - output from do_chip
              chip_code - same as given to do_chip

    Note: some groups may not have a probe set
    """
    d = {0:0,1:0,2:0,3:0,4:0,5:0,6:0,7:0,8:0,9:0,10:0,11:0,12:0,
         13:0,15:0}
    for group in groups:
        pss = filter(lambda x:x.producer()==chip_code,group.get_feats())
        npss = len(pss)
        if npss >= 5:
            d[5]=d[5]+1
        else:
            d[npss]=d[npss]+1
    return d

def get_targ_feat(feat):
    """
    Function: Build target feature from the given probe set consensus
              feature 
    Returns : a new Feature.CompoundDNAFeature bounded by the probe
              coordinates 
    Args    : feat - output from get_chip_feats, after sequence and probe coordinates
                     have been added
    """
    pass
    
        
# here for copying and pasting docs
def zdummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass

def find_repeat_name_groups(fn=None,groups=None):
    """
    Function: Identify groups that contain mRNAs with different Entrez Gene
              assignmetns. Report these to the given filename. 
    Returns : Grouped mRNAs w/ Entrez Gene ids attached, if available.
    Args    : chip_code - one of the keys in fs, an array  identifier
              report_fn - name of report file to write, defaults to orgid...
              groups - grouped mRNA features, with Entrez Gene ids added
              
    ex) test_gene_assignments(chip_code="MOE430",fn="mm8.knownGene.mixed_groups.txt.gz")
    """
    pass
    
def report_groups(fn=None,groups=None):
    if fn:
        fh = open(fn,'w')
    else:
        fh = sys.stderr
    heads = ['groupindex','entrezid','acc','seq','strand','start','end','acc2gene']
    fh.write(sep1.join(heads)+'\n')
    index = 0
    for group in groups:
        gid = group.display_id()
        if not gid:
            gid = 'NA'
        index = index + 1
        fh.write("# "+repr(index)+ " gene assignment: " \
                 + gid+'\n')
        for feat in group.get_feats():
            eids = feat.get_val("NCBI:EntrezGene")
            if not eids:
                eid = "NA"
            else:
                eid = pipe.join(eids)
            to_print = sep1.join([repr(index),gid,feat.display_id(),
                                  feat.seqname(),
                                  repr(feat.strand()),repr(feat.start()),
                                  repr(feat.start()+feat.length()),
                                  eid])+"\n"
            fh.write(to_print)
    fh.close()

def find_no_entrez_id_groups(fn=None,groups=None):
    no_eid_groups = filter(lambda x: not x.display_id(),groups)
    report_groups(fn=fn,groups=no_eid_groups)
    return no_eid_groups

def report_probe_coords(fn=None,chip_feats=None,sep=sep1):
    """
    Function: Report in tab-delimited format the numbers of probe sets in the
              given group that have 0,1,2,etc. probe coordinates.
    Returns :
    Args    : chip_feats - output from get_chip_feats following addition of the
                           sequence, probe coordinates
              fn - name of the file to write out, if not given, then print to stderr
              sep - field separator to use
    """
    N = max(map(lambda x:len(x.get_val('probe_coords')),chip_feats))
    i = 0
    if fn:
        fh = open(fn,'w')
    else:
        fh = sys.stderr
    heads = ['probes','probesets']
    fh.write(sep.join(heads)+'\n')
    while i < N+1:
        n = len(filter(lambda x:len(x.get_val('probe_coords').keys())==i,chip_feats))
        vals = [repr(i),repr(n)]
        fh.write(sep.join(vals)+'\n')
        i = i + 1
    if fn:
        fh.close()
    
def find_mixed_groups(fn=None,groups=None):
    """
    Function: Identify groups that contain mRNAs with different Entrez Gene
              assignments. Report these to the given filename. 
    Returns : Grouped mRNAs w/ Entrez Gene ids attached, if available.
    Args    : chip_code - one of the keys in fs, an array  identifier
              report_fn - name of report file to write, defaults to orgid...
              groups - grouped mRNA features, with Entrez Gene ids added
              
    ex) find_mixed_groups(groups=groups,fn="mm8.knownGene_ps.mixed_groups.txt")
    
    """
    mixed_groups = []
    index = 0
    for group in groups:
        got_one = 0
        gid = group.display_id()
        if gid:
            for feat in group.get_feats():
                eids = feat.get_val('NCBI:EntrezGene')
                if eids and not gid in eids:
                        got_one = 1
                        mixed_groups.append(group)
                        group.set_key_val('group_type','mixed_overlap')
                        break
    if fn:
        report_groups(fn=fn,groups=mixed_groups)
    return mixed_groups
