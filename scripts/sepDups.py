#!/usr/bin/env python

"""Sub-divide duplicate reads from a SAM file into two files with extensions mm (multi-mapper) and sm (single-mapper)"""

import sys,re,os,optparse,countReads,time

def sepDups(read_counts=None,
            sam_file=None,
            out_prefix=None,
            report=None):
    s = '# '+' '.join(sys.argv)
    s = '%s\n# %s\n'%(s,time.asctime())
    d = {}
    if not read_counts:
        fh = open(sam_file,'r')
        (d,count)=countReads.getReadsDict(fh)
        fh.close()
    else:
        fh = open(read_counts,'r')
        while 1:
            line = fh.readline()
            if not line:
                fh.close()
                break
            if line.startswith('#'):
                continue
            toks = line.rstrip().split('\t')
            d[toks[0]]=toks[1]
    fh = open(sam_file,'r')
    reg = re.compile(r'^(\S+)\t')
    if not out_prefix:
        out_prefix = '.'.join(sam_file.split('.')[0:-1])
    mm = open(out_prefix+'.mm.sam','w')
    sm = open(out_prefix+'.sm.sam','w')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('@'):
            mm.write(line)
            sm.write(line)
            continue
        name = reg.match(line).groups()[0]
        if not d.has_key(name):
            raise ValueError("Warning: can't find read named %s in %s"%(name,samfn))
        num_alignments = d[name]
        if num_alignments>1:
            mm.write(line)
        elif num_alignments==1:
            sm.write(line)
    if not report:
        report = out_prefix+".report.txt"
    hist = {}
    total_count = len(d.keys())
    total_count_mms = 0
    max=22
    for i in range(1,max):
        hist[i]=0
    for name in d.keys():
        count = int(d[name])
        if count>1:
            total_count_mms=total_count_mms+1
        if count<max:
            hist[count]=hist[count]+1
        else:
            hist[max-1]=hist[max-1]+1
        del(d[name])
    fh = open(report,'w')
    fh.write(s)
    fh.write("# num reads: %i\n"%total_count)
    fh.write("# num reads with > 1 alignment is: %i\n"%total_count_mms)
    for i in range(1,22):
        line = "%i\t%i\n"%(i,hist[i])
        fh.write(line)
    fh.write("# %i or more\n"%max)
    fh.write("%i\t%i\n"%(max,hist[max-1]))
    fh.close()

def main(options,args):
    sepDups(read_counts=options.read_counts,
            sam_file=options.sam_file,
            out_prefix=options.out_prefix)

    
if __name__ == '__main__':
    usage = "usage: %prog [options]"
    parser = optparse.OptionParser()
    parser.add_option("-r","--read_counts",dest="read_counts",
                     help="file to read with read counts, output from countReads.py [optional]")
    parser.add_option("-o","--out_prefix",dest="out_prefix",
                     help="prefix for mm and sm files to write [optional - gets it from -s|--sam_file if not given]")
    parser.add_option("-s","--sam_file",dest="sam_file",help="SAM format file to read containing alignments")
    (options,args)=parser.parse_args()
    main(options,args)
    
