#!/usr/bin/env python

import os,optparse,sys

"""Create annots.xml text for heat and drought samples for QL"""

class Sample(self):

    def __init__(self,vals):
        if len(vals)>0:
            self.prefix=vals[0]
        if len(vals)>1:
            self.title=vals[1]
        if len(vals)>2:
            self.descr=vals[2]
        if len(vals)>3:
            self.url=vals[3]

    
            
class Samples():
    "A class encapsulating information about a collection of samples from an RNASeq experiment."

    def __init__(self,lst):
        self._lst = lst
        self.last = -1
        
    def __iter__(self):
        return self

    def __next__(self):
        if len(self._lst)>=self.last:
            raise StopIteration
        else:
            self.last+self.last+1
            vals = return self._lst[last]
            return Sample(vals)
    
# all the samples
samples_txt = """CoolHT2
CoolL1T1
CoolL1T2
CoolL2T1
DryBT1
DryBT2
DryCT1
DryCT2
DryET1
DryET2
HotI1T1
HotI1T2
HotI2T1
HotI2T2
HotK1T1
HotK1T2
HotK2T1
HotK2T2
WetAT1
WetAT2
WetDT2
WetFT1
WetFT2"""


# source_dir = "heat_drought_processed"
# deploy_dir = "heat_drought"
drought_samples =  [["WetDT2","Wet D"],
                    ["WetFT2","Wet F"],
                    ["DryBT2","Dry B"],
                    ["DryCT2","Dry C"],
                    ["DryET2","Dry E"]]

heat_samples = [["CoolL1T1","Cool L1, control (3h, 22 deg)"],
                ["CoolL2T1","Cool L2, control (3h, 22 deg)"],
                ["HotI2T1","Hot I2, heat shock (3h, 38 deg)"],
                ["HotK1T1","Hot K1, heat shock (3h, 38 deg)"],
                ["HotK2T1","Hot K2, heat shock (3h, 38 deg)"],
                ["CoolHT2","Cool H, control recovery (24h, 22 deg)"],
                ["CoolL1T2","Cool L1, control recovery (24h, 22 deg)"],
                ["HotI1T2","Hot I1, heat shock recovery (24h, 22 deg)"],
                ["HotK1T2","Hot K1, heat shock recovery (24h, 22 deg)"],
                ["HotK2T2","Hot K2, heat shock recovery (24h, 22 deg)"]]

# tophat_2_processed - note: no ALL reads

pollen = [["pollen_1","Pollen Lane 1"],
          ["pollen_2","Pollen Lane 2"]]

mixed_cold = [["cold_control_1","Control, combined time points, Lane 1"],
              ["cold_control_2","Control, combined time points, Lane 2"],
              ["cold_treatment_1","Cold, combined time points, Lane 1"],
              ["cold_treatment_2","Cold, combined time points, Lane 2"]]

mixed_drought = [["drought_control_1_1","Control1 Lane 1"],
                 ["drought_control_1_2","Control1 Lane 2"],
                 ["drought_treatment_1_1","Drought1 Lane 1"],
                 ["drought_treatment_1_2","Drought1 Lane 2"],
                 ["drought_control_2_1","Control2 Lane 1"],
                 ["drought_control_2_2","Control2 Lane 2"],
                 ["drought_treatment_2_1","Drought2 Lane 1"],
                 ["drought_treatment_2_2","Drought2 Lane 2"]]

                


def deployFiles(deploy_dir=None,source_dir=None,samples=None):
    "Copy data files into source_dir for public deployment."
    for sample in samples):
        sample = sample.prefix
        fns = [sample+".bam",sample+".bam.bai",
               sample+".bedgraph.gz",sample+".bedgraph.gz.tbi",
               sample+".sm.bam",sample+".sm.bam.bai",
               sample+".sm.bedgraph.gz",sample+".sm.bedgraph.gz.tbi",
               sample+".mm.bam",sample+".mm.bam.bai",
               sample+".mm.bedgraph.gz",sample+".mm.bedgraph.gz.tbi",
               sample+".bed.gz",sample+".bed.gz.tbi",
               sample+".alignments_distribution.txt"]
        for fn in fns:
            deployFile(deploy_dir=deploy_dir,
                       source_dir=source_dir,
                       fn=fn)

def deployFile(deploy_dir=None,
               source_dir=None,
               fn=None):
    "Copy the give file from source_dir to deploy_dir"
    cmd = 'cp %s/%s %s/.'%(source_dir,fn,deploy_dir)
    code = os.system(cmd)
    if code != 0:
        raise OSError("Failed! Could not execute: %s"%cmd)

def makeFileTag(fn,title,descr,url=None):
    if url:
        txt = '  <file\n    name="%s"\n    title="%s"\n    description="%s"\n  />\n'%(fn,title,descr)
    else:
        txt = '  <file\n    name="%s"\n    title="%s"\n    description="%s"    url="%s"\n  />\n'%(fn,title,descr,url)
    return txt

def openFilesTag(fh=None):
    fh.write("<files>\n")

def closeFilesTag(fh=None):
    fh.write("</files>")

def writeXmlForSamples(fh=None,samples=None,deploy_dir=None,study_name=None):
    "Write XML file tags for samples."
    for tup in samples:
        key = tup[0]
        sample = tup[1]
        if len(tup)>2:
            url = tup[2]
        descr = "alignments for reads that mapped once onto the genome"
        fn = deploy_dir + os.sep + key + '.sm.bam'
        title = folder + " / SM / Reads / " + sample + ", alignments"
        fh.write(makeFileTag(fn,title,descr))

        descr = "coverage graph for reads that mapped once onto the genome"
        fn = deploy_dir + os.sep + key + '.sm.bedgraph.gz'
        title = folder + " / SM / Graph / " + sample + ", coverage"
        fh.write(makeFileTag(fn,title,descr))

        descr = "alignments for reads that mapped to multiple places in the genome"
        fn = deploy_dir + os.sep + key + '.mm.bam'
        title = folder + " / MM / Reads / " + sample
        fh.write(makeFileTag(fn,title,descr))

        descr = "coverage graph for reads that mapped to multiple places in the genome"  
        fn = deploy_dir + os.sep + key + '.mm.bedgraph.gz'
        title = folder + " / MM / Graph / " + sample + ", coverage"
        fh.write(makeFileTag(fn,title,descr))
        
        descr = "alignments for ALL reads (multi- and single-mapping)"
        fn = deploy_dir + os.sep + key + '.bam'
        title = folder + " / ALL / Reads / " + sample + ", alignments"
        fh.write(makeFileTag(fn,title,descr))
        
        descr = "coverage graph for ALL reads (multi- and single-mapping)"
        fn = deploy_dir + os.sep + key + '.bedgraph.gz'
        title = folder + " / ALL / Graph / " + sample + ", coverage"
        fh.write(makeFileTag(fn,title,descr))
        
        descr = "Junctions predicted by TopHat"
        fn = deploy_dir + os.sep + key + '.bed.gz'
        title = folder + " / Juncs / " + sample + ", junctions"
        fh.write(makeFileTag(fn,title,descr))

def makeAllXml(fn='annots.xml'):
    fh = open(fn,'w')
    openFilesTag(fh)
    writeXmlForSamples(fh=fh,deploy_dir="heat_drought",study_name="Drought",samples=drought_samples) 
    writeXmlForSamples(fh=fh,deploy_dir="heat_drought",study_name="Heat",samples=heat_samples)
    writeXmlForSamples(fh=fh,deploy_dir="pollen_pilot_processed",study_name="Pollen",samples=pollen)
    writeXmlForSamples(fh=fh,deploy_dir="pollen_pilot_processed",study_name="Mixed Cold",samples=mixed_cold)
    writeXmlForSamples(fh=fh,deploy_dir="pollen_pilot_processed",study_name="Mixed Drought",samples=mixed_drought)
    closeFilesTag(fh)
    fh.close()

    
def doIt(fn=None,source_dir=None,deploy_dir=None,
         copy_files=None,study_name=None,samples=None):
    makeAnnotsXML(fn=fn,deploy_dir=deploy_dir,study_name=study_name,samples=samples)
    if copy_files:
        deployFiles(deploy_dir=deploy_dir,source_dir=source_dir,samples=samples)
    
def main(options,args):
    "Do it all."
    copy_files = options.copy_files
    deploy_dir = options.deploy_dir
    source_dir=options.source_dir
    study_name=options.study_name
    if len(args)==0:
        outfn = None
    else:
        outfn = args[0]
    loraine_lab = options.loraine_lab
    if loraine_lab == 'heat':
            doIt(fn=outfn,source_dir=source_dir,copy_files=copy_files,
                 study_name="Heat",samples=heat_samples,deploy_dir=deploy_dir)
    elif loraine_lab == 'drought':
            doIt(fn=outfn,source_dir=source_dir,copy_files=copy_files,
                 study_name="Drought",samples=drought_samples,deploy_dir=deploy_dir)
    else:
        raise Error("No others supported yet")

 
if __name__ == '__main__':
    usage = "%prog [options] file.xml\n\nPrints XML to stdout if run with no arguments."
    parser = optparse.OptionParser(usage)
    parser.add_option("-p","--processed_dir",
                      help="Directory with processed files. Default is current working directory",
                      dest="source_dir",default=".")
    parser.add_option("-d","--deploy_dir",help="Directory where files will reside on QL site",
                      dest="deploy_dir",default=None)
    parser.add_option("-c","--copy_files",dest="copy_files",
                      help="Don't just make XML file. Copy files into deploy_dir.",
                      action="store_true")
    parser.add_option("-s","--study_name",dest="study_name",
                      help="Name of study or experiment. Will be shown as QL folder",
                      default="study")
    parser.add_option("-l","--loraine_lab",dest="loraine_lab",default="heat")
    (options,args)=parser.parse_args()
    if not options.deploy_dir:
        parser.error("Specify -d|--deploy_dir directory where files will reside on QL site.")
    main(options,args)



        
        
    
     
     
