"""A script that accepts a listing of gene model names (e.g., AGI codes in the case of Arabidopsis genome
annotatins) and then writes out IGB links in Web page format for these links."""

import os,sys,getopt
import Mapping.Parser.Bed as r
import Mapping.FeatureModel as F
import Utils.Url as uu

def usage():
    """Print a helpful message regarding usage of this program to stderr."""
    txt = """
    write_agi2igb_links - Write a Web page with IGB links for a list of AGI codes

    Commands:
    
    [-b|--bed file.bed]
        The file that contains gene models. REQUIRED.

    [-v|--version v]
        The genome version the bed file references. REQUIRED

        ex) A_thaliana_TAIR9

    [-o|--out file.html]
        The name of the Web page to write. If not supplied, prints to stdout.

    [-a | --agis file.txt]
        File containing list of AGI locus identifiers. 

    [-d | --delim c]
        If file containing list of AGIs is in tabular format, indicate the
        delimiter character.

    [-f | --field i]
        If file containing list of AGIs in in tabular format, indicate the
        field containing the AGI\n"""
    sys.stderr.write(txt)

def getArgs(args):
    """Retrieve options as dictionary. 
    Exits with an error if illegal options
    given."""
    bedfile = None
    outfile = None
    agisfile = None
    delim = None
    field = None
    version = None
    delim = None
    opts,args = getopt.gnu_getopt(args,"hb:o:a:d:f:v:",
                                  ["help","bed=","agis=",
                                   "delim=","field=","version="])
    d = {}
    for opt, arg in opts:
        if opt in ("-h","--help"):
            usage()
            sys.exit()
        elif opt in ("-b","--bed"):
            bedfile = arg
            d['bedfile']=bedfile
        elif opt in ("-a","--agisfile"):
            agisfile = arg
            d['agisfile']=agisfile
        elif opt in ("-o","--outfile"):
            outfile = arg
            d['outfile']=outfile
        elif opt in ("-v","--version"):
            version = arg
            d['version']=version
        elif opt in ("-f","--field"):
            field = arg
            d['field'] = int(field)
        elif opt in ("-d","--delim"):
            delim = arg
            d['delim']=delim
        else:
            raise getopt.GetoptError("Illegal option: " + opt)
    if not bedfile or not agisfile or not version:
        usage()
    return d

def getGeneModels(infn=None,version=None):
    """
    Function: parse the given file 
    Returns : a list of CompoundDNASeqFeature objects
              representing gene models
    Args    : infh - string, name of bed file containing gene structures
              version - string, name of a genome version IGB can recognize  

    For an example bed file, see:

    http://www.bioviz.org/quickload/A_thaliana_TAIR9/TAIR9_mRNA.bed.gz

    To retrieve it via unix, do something like:

    curl http://www.bioviz.org/quickload/A_thaliana_TAIR9/TAIR9_mRNA.bed.gz

    and save the output to a file
    """
    feats = r.bed2feats(infn)
    if len(feats)==0:
        raise ValueException("Retrieved no features from : " + \
                             infn)
    for feat in feats:
        feat.setKeyVal('version',version)
    return feats

def makeGenesFromTranscripts(feats=None):
    """
    Function: Create CompoundDNASeqFeature objects representing 
              genes, collections of transcripts arising from the
              same location in a genome.
    Returns : a dictionary, keys are AGI codes for genes,
              value are gene objects
    Args    : feats - output from getFeatures, a list of
              CompoundDNASeqFeature objects
              
    Note that we are using gene models identifiers (AGI codes) to group the gene
    models. Gene models with the same locus id will join the same
    groups.
    """
    genes = {}
    for f in feats:
        tx_id = f.getDisplayId()
        agi = tx_id.split('.')[0]
        if not genes.has_key(agi):
            genes[agi]=F.CompoundDNASeqFeature(seqname=f.getSeqname(),
                                               strand=f.getStrand(),
                                               type="locus",
                                               producer=f.getProducer(),
                                               display_id=agi)
        
        gene_feat = genes[agi]
        gene_feat.addFeat(f)
        gene_feat.setKeyVal('version',f.getVal('version'))
    for gene in genes.values():
        start = min(map(lambda x:x.getStart(),gene.getFeats()))
        ends = map(lambda x:x.getStart()+x.getLength(),gene.getFeats())
        end = max(ends)
        gene.setStart(start)
        gene.setLength(end-start)
    return genes

def addIgbLink(feat):
    """
    Function: Add IGB link URL as a key/value property
              on the given feature object
    Returns : 
    Args    : feat - a SeqFeature, with key value
                  version set
    """
    s = feat.getStart()-500
    e = feat.getStart() + feat.getLength()+500
    if s <= 0:
        s == 1
    v = feat.getVal('version')
    seqname = feat.getSeqname()
    igb_link = uu.igb_url(s,e,seqname,v)
    feat.setKeyVal('URL',igb_link)

def writeLinks(agis=None,outfn=None,genes=None):
    """
    Function: Write a Web page 
    Returns : a string, the text of the Web page
    Args    : agis - a list of AGI codes
              outfn - name of the file to write
              genes - output from make_genes_from_transcripts

    for example, for a list of AGIs, see: braytable.csv
    """
    lst = []
    for agi in agis:
        gene = genes[agi]
        lst.append(gene)
    writeIgbLinksPage(feats=lst,fn=outfn)

def writeIgbLinksPage(feats=None,fn=None):
    """
    Function: Create a Web page containing lists of IGB links
    Returns : A string
    Args    : feats - a list of DNASeqFeature objects
              fn - name of a file to write [optional]

    We will look at each feature to see if it returns something
    when asked:

      feat.getVal('version')

    This must be set to create an IGB link.
    """
    txt = "<html>\n<body>\n<ol>\n"
    for feat in feats:
        igb_link = feat.getVal('URL')
        line = "<li>"+feat.getDisplayId()+' <a href="'+igb_link+'">View in IGB</a>\n'
        txt = txt + line
    txt = txt + "</ol></body></html>\n"
    if fn:
        fh = open(fn,'w')
        fh.write(txt)
        fh.close()
    else:
        sys.stdout.write(txt)
    return txt

def getAgis(agisfile=None,
            sep='\t',
            field=None):
    """
    Function: Retrieve AGI locus codes from the given file
    Returns : A list of AGI locus codes, without duplicates,
              in the same order they appeared in the given file
    Args    : agisfile - string, name of the file
              sep - string, separator [defaults is tab]
              field - int, field containing AGI locus code in
                 agisfile, if it is tabular

    If field is not given, we just grab all possible AGI locus
    codes from the file.
    """
    import re
    fh = open(agisfile,'r')
    agis_reg = re.compile(r'at[1-5cCmM]g\d{5}',re.I)
    if not field:
        txt = fh.read()
        agis = agis_reg.findall(txt)
    else:
        agis = []
        while 1:
            line = fh.readline()
            if not line:
                break
            line = line.rstrip()
            toks = line.split(sep)
            agi = toks[field]
            if not agis_reg.match(agi):
                raise ValueError("No AGI in field " + \
                                 str(field) + " line: " +\
                                 line)
            else:
                agis.append(agi)
    d = {}
    lst = []
    for agi in agis:
        if not d.has_key(agi):
            lst.append(agi)
        d[agi]=1
    return lst
    
def doIt(d):
    """
    Function: Make a Web page with IGB links to the regions
              where each AGI in the agisfile are located
    Returns : 
    Args    : d - output from getArgs

    If field is not given, we just grab all possible AGI locus
    codes from the file.
    """
    agisfile = d['agisfile']
    if d.has_key('delim'):
        sep = d['delim']
    else:
        sep = '\t'
    if d.has_key('field'):
        field = d['field']
    else:
        field = None
    # get ids
    agis = getAgis(field=field,sep=sep,agisfile=agisfile)
    # get gene models
    feats = getGeneModels(infn=d['bedfile'],
                          version=d['version'])
    # make locus features
    genes = makeGenesFromTranscripts(feats=feats)
    for agi in genes.keys():
        addIgbLink(genes[agi])
    # write out the Web page with IGB links
    outfn = None
    if d.has_key('outfile'):
        outfn = d['outfile']
    writeLinks(agis=agis,outfn=outfn,genes=genes)
    
    
if __name__ == '__main__':
    d = getArgs(sys.argv[1:])
    doIt(d)
