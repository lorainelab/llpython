import os,re,sys
import csv
import annots_xml_for_rnaseq as a
import urllib

fn='http://www.ebi.ac.uk/arrayexpress/files/E-GEOD-30249/E-GEOD-30249.sdrf.txt'
# paper: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3219791

def readDat(fn=fn):
    "Read the data and get values put into a dictionary"
    fh = urllib.urlopen(fn,'r')
    r = csv.DictReader(fh,delimiter='\t',quoting=csv.QUOTE_MINIMAL)
    return r

def writeXML(fn=fn,outfn='tmp.xml',deploy_dir='SRP007361',
             study_name='RNASeq / Stress ABA SRP007361'):
    r = readDat(fn=fn)
    fh = open(outfn,'w')
    lsts=[]
    base_u = 'http://www.ncbi.nlm.nih.gov/sra/'
    for d in r:
        SRR = d['Scan Name']
        tissue = d['FactorValue [TISSUE]']
        treatment=d['FactorValue [TREATMENT]']
        SRX = d['Comment [ENA_EXPERIMENT]']
        u = base_u+SRX
        if treatment == '0.2M NaOH':
            sample = '%s %s %s'%(SRR,tissue,'NaOH')
        elif treatment == 'H2O':
            sample = '%s %s %s'%(SRR,tissue,'Control')
        elif treatment.endswith('exogenous ABA for 27 hrs'):
            sample = '%s %s %s'%(SRR,tissue,'ABA')
        elif treatment == '20% PEG-8000':
            sample = '%s %s %s'%(SRR,tissue,'PEG')
        else:
            raise Exception("Unrecognized row.")
        lsts.append([SRR,sample,u])
        print SRR,SRX,sample
    samples = a.Samples(lsts)
    a.makeAnnotsXML(fn=outfn,
                    samples=samples,
                    deploy_dir=deploy_dir,
                    study_name=study_name)
                        
                      
