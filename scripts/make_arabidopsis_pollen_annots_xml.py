#!/usr/bin/env python

"""Make annots.xml for Arabidospis pollen QuickLoad site."""

import make_annots_xml as mk
import os,sys

# constants
ab='FFFFFF'
url='pollen_processed_2.5'
c='008000'
t='993300'

samples = [['pollen','Pollen',t,ab,url],
         ['drought_control_1','Seedling 1',c,ab,url],
         ['drought_control_2','Seedling 2',c,ab,url]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder='RNA-Seq',
                                deploy_dir='ngs',
                                all=False)
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()

