#!/usr/bin/env python
"""
Capture and report information about A. thaliana genes. 
"""
from Utils.General import readfile
import Mapping.FeatureModel
import getopt
import re, gzip, zipfile, sys
import counts_loader

agi_regex = re.compile(r'AT[1-5CM]G\d{5,5}')

class Identifiable:
    "An object that has a display_id"

    def __init__(self,display_id=None,**kwargs):
        self.setDisplayId(display_id)

    def setDisplayId(self,display_id=None):
        self._display_id = display_id

    def getDisplayId(self):
        return self._display_id
    
class Loci:
    "A collection of Locus objects, none with the same display_id"
    def __init__(self):
        self._d = {}
        self._l = []

    def getLocus(self,key=None):
        if self._d.has_key(key):
            return self._d[key]
        else:
            return None

    def addLocus(self,locus=None):
        if self._d.has_key(locus.getDisplayId()):
            raise ValueError("Already have value for: " + locus.getDisplayId())
        self._d[locus.getDisplayId()]=locus
        self._l.append(locus)

    def __getitem__(self,val):
        return self._l.__getitem__(val)

    def __len__(self):
        return self._l.__len__()
    
    def countLoci(self):
        return len(self._d.keys())

    


class Locus(Identifiable):
    "Represents an Arabidopsis gene."
    
    def __init__(self,**kwargs):
        Identifiable.__init__(self,**kwargs)
        self._d = {}
        self._aliases = {}
        self._type = None
        self._curator_summary = None
        self._short_description = None
        self._computational_description = None

    def getDescr(self):
        pss = self.getUniqueProbesets()
        for p in pss:
            d = p.getTargetDescription(self.getDisplayId())
            if d not in ['','NA',None,"transposable element gene"]:
                return d
        d = self.getShortDescription()
        if d not in ['NA',None,'',"transposable element gene"]:
            return d
        d = self.getCuratorSummary()
        if d not in ['','NA',None,"transposable element gene"]:
            return d
        d = self.getComputationalDescription()
        if d not in ['','NA',None]:
            return d
        aliases = self.getAliases()
        ds = aliases.values()
        for d in ds:
            if d not in ['','NA',None]:
                return d
        d = self.getGeneType()
        if d not in ['','NA',None]:
            return d
        return None

    def getSymbol(self):
        aliases = self._aliases.keys()
        if len(aliases) == 0:
            return self.getDisplayId()
        else:
            return "|".join(aliases)

    def setCuratorSummary(self,curator_summary=None):
        self._curator_summary = curator_summary

    def setGeneType(self,gene_type=None):
        self._type = gene_type

    def getGeneType(self):
        return self._type
        
    def addKeyVal(self,key=None,val=None):
        if self._d.has_key(key):
            vals = self._d[key]
            if not val in vals:
                vals.append(val)
        else:
            vals = [val]
            self._d[key]=vals

    def getKeyVal(self,key=None):
        if self._d.has_key(key):
            return self._d[key]
        else:
            return None

    def addProbeSet(self,ps=None):
        self.addKeyVal(key="ps",val=ps)

    def addAlias(self,short=None,long=None):
        d = self._aliases
        if d.has_key(short):
            current_long = d[short]
            if not current_long == long:
               # sys.stderr.write("Warning: Wrong value for alias %s; old and new don't match: %s ne %s\n"%
               #                  (short,current_long,long))
                # choose the longer
                if len(long) > len(current_long):
                   d[short]=long
        else:
            d[short]=long

    def getAliases(self):
        return self._aliases

    def getUniqueProbesets(self):
        "Return all probe sets that have just one target"
        probesets = self.getKeyVal('ps')
        if not probesets:
            return []
        uniques = []
        for probeset in probesets:
            if len(probeset.getTargets())==1:
                uniques.append(probeset)
        return uniques

    def getPromiscuousProbesets(self):
        "Return all probe sets that have more than one target" 
        probesets = self.getKeyVal('ps')
        if not probesets:
            return []
        promiscuous = []
        for probeset in probesets:
            if len(probeset.getTargets())>1:
                promiscuous.append(probeset)
        return promiscuous
                                
    def setCuratorSummary(self,val):
        self._curator_summary = val

    def getCuratorSummary(self):
        return self._curator_summary

    def setComputationalDescription(self,val):
        self._computational_description = val

    def getComputationalDescription(self):
        return self._computational_description

    def setShortDescription(self,val):
        self._short_description = val

    def getShortDescription(self):
        return self._short_description


class ProbeSet(Identifiable):

    def __init__(self,**kwargs):
        Identifiable.__init__(self,**kwargs)
        self._targets = []
        self._target_descriptions={}
        
    def addTarget(self,val):
        "Add a target Locus for this ProbeSet"
        if not val in self._targets:
            self._targets.append(val)

    def getTargets(self):
        return self._targets

    
    def addTargetDescription(self,target=None,
                             target_description=None):
       d = self._target_descriptions
       d[target]=target_description

    def getTargetDescription(self,target):
        return self._target_descriptions[target]
    
def makeLocus(name=None):
    "Make a new Locus object."
    locus = Locus(display_id=name)
    return locus

def getProbeSets(psinfo='affy_ATH1_array_elements-2010-12-20.txt.gz',loci=None):
    "Parse probe set information from TAIR array elements file. Assumes one line per probe set."
    fh = readfile(psinfo)
    header = fh.readline().rstrip()
    if not header == 'array_element_name\tarray_element_type\torganism\tis_control\tlocus\tdescription\tchromosome\tstart\tstop':
        raise ValueError("Wrong header from %s: %s"%(psinfo,header))
    heads = header.split('\t')
    if not loci:
        loci = Loci()
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        is_control = toks[3]
        if is_control == 'yes':
            # ignore these
            continue
        ps = toks[0]
        description = toks[5]
        gene_target_field = toks[4]
        if description == '' or len(description)<2:
            raise ValueError("No description: %s"%description)
        if gene_target_field == '' or len(gene_target_field)<2:
            raise ValueError("No target field: %s"%gene_target_field)
        if not len(toks) == len(heads):
            raise ValueError("Can't parse from %s: %s"%(psinfo,','.join(toks)))
        if gene_target_field == 'no_match':
            continue
        gene_targets = gene_target_field.split(';')
        probeset = ProbeSet(display_id=ps)
        if len(gene_targets) == 1:
            locus = loci.getLocus(gene_targets[0])
            if not locus:
                locus = Locus(display_id=gene_targets[0])
                loci.addLocus(locus)
            probeset.addTarget(locus)
            probeset.addTargetDescription(target=gene_targets[0],
                                          target_description=description)
            locus.addProbeSet(probeset)
        else:
            descriptions = description.split('];[')
            i = 0
            for gene_target in gene_targets:
                if not agi_regex.match(gene_target):
                    raise ValueError("Don't recognize locus: %s"%gene_target)
                descr = descriptions[i]
                if descr.startswith('['):
                    descr = descr[1:]
                elif descr.endswith(']'):
                    descr = descr[:-1]
                locus = loci.getLocus(gene_target)
                if not locus:
                    locus = Locus(display_id=gene_target)
                    loci.addLocus(locus)
                probeset.addTarget(locus)
                probeset.addTargetDescription(target=gene_target,
                                              target_description=descr)
                locus.addProbeSet(probeset)
                i=i+1
    return loci

def filterStructuralAnnotations(bed='TAIR10.bed.gz',loci=None):
    "Return a new Loci object that contains only those locus represented in the bed file."
    fh = readfile(bed)
    newloci = Loci()
    while 1:
        line = fh.readline()
        if not line:
            break
        agi = line.split('\t')[3].split('.')[0]
        locus = loci.getLocus(agi)
        if not locus:
            raise ValueError("Can't find agi %s"%agi)
        if not newloci.getLocus(agi):
            newloci.addLocus(locus)
    return newloci
        
    
def getGeneAliases(aliases='gene_aliases.20101027.gz',loci=None):
    "Parse symbol and description from gene aliases file. Note that a gene can have multiple symbol/full_name pairs."
    if not loci:
        loci = Loci()
    fh = readfile(aliases)
    # header should be: "locus_name" "symbol" "full_name"
    header = fh.readline().rstrip()
    if not header == 'locus_name\tsymbol\tfull_name':
        raise ValueError("File %s not recognized as gene aliases file."%aliases)
    heads = header.split('\t')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.split('\t')
        if not len(toks)==len(heads):
            raise ValueError("Can't parse from %s: %s"%(aliases,','.join(toks)))
        name = toks[0]
        locus = loci.getLocus(name)
        if not locus:
            if not agi_regex.match(name):
                continue
            locus = Locus(display_id=name)
            loci.addLocus(locus)
        alias = toks[1].strip()
        long_name = toks[2].strip()
        if long_name == '':
            long_name = 'NA'
        locus.addAlias(short=alias, long=long_name)
    return loci

def getGeneFunctions(func_info='TAIR10_functional_descriptions.gz',loci=None):
    "Parse functional information from TAIR functional descriptions file."
    if not loci:
        loci = Loci()
    fh = readfile(func_info)
    header = fh.readline().rstrip()
    if not header == 'Model_name\tType\tShort_description\tCurator_summary\tComputational_description':
        raise ValueError("File %s not recognized as functional information from TAIR10."%header)
    heads = header.split('\t')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.split('\t')
        if not len(toks)==len(heads):
            raise ValueError("Can't parse from %s: %s"%(func_info,','.join(toks)))
        (name,var) = toks[0].split('.') # get locus id
        if not agi_regex.match(name):
            continue
        locus = loci.getLocus(name)
        if not locus:
            locus=Locus(display_id=name)
            loci.addLocus(locus)
        gene_type = toks[1].strip()
        if gene_type == '':
            raise ValueError("Can't have empty value for gene type")
        locus.setGeneType(gene_type)
        short_description = toks[2].strip()
        if short_description != '':
            if not locus.getShortDescription() or len(locus.getShortDescription())<short_description:
                locus.setShortDescription(short_description)
        curator_summary = toks[3].strip()
        if curator_summary != '':
            if not locus.getCuratorSummary():
                locus.setCuratorSummary(curator_summary)
        computational_description = toks[4].strip()
        if computational_description != '':
            locus.setComputationalDescription(computational_description)
    return loci

def writeLoci(loci=None,fn='allLoci.txt',annotations_release='TAIR10'):
    db = counts_loader.makeDbConnection()
    d = counts_loader.getLocusToId(db=db,annotations_release=annotations_release)
    n = 0
    max_descr = None
    if not loci:
        loci = doAll()
    fh = open(fn,'w')
    heads = ['locus.id','locus','locus.type','other.names','specific.probesets',
             'crosshybing.probesets','description']
    i = 0
    N = loci.countLoci()
    fh.write('\t'.join(heads)+'\n')
    while (i < N):
        locus = loci[i]
        descr = locus.getDescr()
        if len(descr)>n:
            n = len(descr)
            max_descr = descr
        specific_probesets = locus.getUniqueProbesets()
        if len(specific_probesets)>0:
            s_pss = '|'.join(map(lambda x:x.getDisplayId(),specific_probesets))
        else:
            s_pss = '\N'
        promiscuous_probesets = locus.getPromiscuousProbesets()
        if len(promiscuous_probesets)>0:
            p_pss = '|'.join(map(lambda x:x.getDisplayId(),promiscuous_probesets))
        else:
            p_pss = '\N'
        locus_id = d[locus.getDisplayId()]
        vals = (str(locus_id),locus.getDisplayId(),locus.getGeneType(),
                locus.getSymbol(),s_pss,p_pss,descr)
        fh.write('\t'.join(vals)+'\n')
        i = i + 1
    fh.close()
    sys.stderr.write("Maximum description field size: %i\n"%n)
    sys.stderr.write("It's:\n%s\n"%max_descr)
        
def doAll():
    loci = getProbeSets()
    getGeneAliases(loci=loci)
    getGeneFunctions(loci=loci)
    newloci = filterStructuralAnnotations(loci=loci)
    writeLoci(loci=newloci)
    return newloci
    
def addCols(gene_functions_d=None,
            gene_aliases_d=None,
            namecol=0,
            oldfn=None,
            newfn=None,
            sep='\t'):
    "Add gene description columns to a file. Insert symbol and genedescr fields immediately after namecol."
    fh = readfile(oldfile)
    # assume there's a header
    header = fh.readline()
    heads = header.rstrip().split(sep)
    outfh = open(newfn,'w')
    i = 0
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            outfh.close()
            break
        toks = line.rstrip().split(sep)
        vals = []
        symbol = 'NA'
        genedescr = 'NA'
        i = 0
        i = i + 1

"""
        for val in toks:
            if namecol == i:
                name = val
                if getSymbol(name,
        if i == 0:
            extra = '\t'.join(heads)+'\n'
            newline = line.strip()+'\t'+extra
            outfh.write(newline)
        else:
            toks = line.split('\t')
            name = toks[namecol].split('.')[0]
            if tup[1].has_key(name):
                d = tup[1][name]
            else:
                d = na
            extra = ''
            vals = map(lambda x:d[x],heads)
            extra = '\t'.join(vals)+'\n'
            newline = line.strip()+'\t'+extra
            outfh.write(newline)
"""
def main(options):
    gene_functions_d = getGeneFunctions(options.func_info)
    gene_aliases_d = getGeneAliases(options.aliases)
    addCols(gene_functions_d=gene_functions_d,
            gene_aliases_d=gene_aliases_d,
            namecol=options.namecol,
            oldfn=args[0],
            newfn=args[1])
    
if __name__ == '__main__':
    import optparse
    usage = "%prog [options] INFILE OUTFILE"
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--agi-col',dest='namecol',help='field in the file with AGI code, first column is numbered 0 (default is 0)',
                     default=0,type="int")
    parser.add_option('-g','--gene-aliases',dest='aliases',help="File with gene aliases from TAIR",
                     default="gene_aliases.20101027.gz")
    parser.add_option('-f','--func-info',dest='func_info',help="File with functional info from TAIR",
                     default='TAIR10_functional_descriptions.gz')
    (options,args)=parser.parse_args()
    if not len(args)==2:
        parser.error("Supply file to read and file to write.")
    main(options,args)


