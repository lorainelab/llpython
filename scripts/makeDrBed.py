"Convert ArabiTagMain.py output into BED for visualization in IGB"

import Mapping

def readAbtFile(fn=None):
    "Read Abt file and make single span features representing the difference region"
    fh = open(fn)
    return readAbtStream(fh)

def readAbtFile(fh=None):
    "Read Abt file handle and make single span features representing the difference region"
    feats = []
    while 1:
        line = fh.readline()
        if not line:
            break
        f = makeFeat(line.rstrip())
        feats.append(f)
    return feats

def makeFeat(line):
    # get first 7 tokens
    toks = line.split('\t')[0:7]
    (locus,Ga,Gp,i_overlap,e_overlap,i_alt,category) = toks
    exon = map(int,e_overlap.split(','))
    e_start = exon[0]
    e_end = e_start + exon[1]
    intron = map(int,i_overlap.split(','))
    i_start = intron[0]
    i_end = i_start + intron[1]
    if not intron[2]==exon[2]:
        raise ValueError("Ga %s and Gp %s not on same strand"%(Ga,Gp))
    dr_start = max(i_start,e_start)
    dr_end = min(i_end,e_end)
    dr_len = dr_end - dr_start
    if dr_len <=0:
        raise ValueError("Ga %s and Gp %s do not overlap"%(Ga,Gp))
    dr_display_id = '%s %s (S) vs %s (L)' % (locus,Ga.split('.')[-1],
                                             Gp.split('.')[-1])
    seqname = locus[2]
    seqname = 'chr'+seqname
    f = Mapping.Feature.DNASeqFeature(seqname=seqname,start=dr_start,length=dr_len,
                                      display_id=dr_display_id,strand=exon[2])
    return f

def writeDrBed(fn=None,feats=None):
    fh = open(fn,'w')
    for f in feats:
        line = b.feat2bed(each)
        fh.write(line)

if __name__ == "__main__":
    pass
