#!/usr/bin/env python

"""Make annots.xml for Doherty Lab rice salt stress QuickLoad site."""

import sys,os
import make_annots_xml_for_quickload as mk

# color for control, morning (am)
c_am='336600'
# color for control, evening (pm) 
c_pm='008B8B'
# color for treatment, morning (am)
t_am='9900FF' 
# color for treatment, evening (pm)
t_pm='8B2323'

b="FFFFFF"
u='O_sativa_japonica_Oct_2011/saltyrice_processed'


# Fastq file nae, Sample number (sequence facility), treatment, quickload title
"""
Doherty 1	1	t_pm TE1
Doherty 2	3	c_am CM1
Doherty 3	4	c_pm CE1
Doherty 4	7	c_am CM2
Doherty 5	11	c_am CM3
Doherty 6	18	t_am TM1
Doherty 7	19	t_am TM2
Doherty 8	21	c_pm CE2
Doherty 9	23	t_pm TE2
Doherty 10	24	s_am TM3
Doherty 11	28	c_pm CE3
Doherty 12	31	t_pm TE3
"""

# file name, quickload title, foreground color, background color, URL (relative to QuickLoad root)
samples=[['2','CM1',c_am,b,u], # control morning
         ['4','CM2',c_am,b,u],
         ['5','CM3',c_am,b,u],
         ['3','CE1',c_pm,b,u], # control evening
         ['8','CE2',c_pm,b,u],
         ['11','CE3',c_pm,b,u],         
         ['6','TM1',t_am,b,u], # treatment morning
         ['7','TM2',t_am,b,u], 
         ['10','TM3',t_am,b,u],
         ['1','TE1',t_pm,b,u], # treatment evening
         ['9','TE2',t_pm,b,u],
         ['12','TE3',t_pm,b,u]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder='Doherty Lab / Salt',
                                deploy_dir=u.split(os.sep)[-1],
                                all=False)
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()

