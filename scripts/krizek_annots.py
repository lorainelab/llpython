#!/usr/bin/env python

"""Make annots.xml for Beth Krizek RNA-Seq samples"""

import make_annots_xml as mk
import sys

b="FFFFFF"
m="9900FF"
w="336600"
u='A_thaliana_Jun_2009/BK2_processed'
samples=[['ant1','m1',m,b,u],
         ['anr2','m2',m,b,u],
         ['ant3','m3',m,b,u],
         ['Ler5','m4',m,b,u],
         ['Ler1','w1',w,b,u],
         ['Ler2','w2',w,b,u],
         ['Ler3','w3',w,b,u],
         ['ant5','w4',w,b,u]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder='ant ail6 RNA-Seq',
                                deploy_dir='BK2_processed',
                                all=False) # don't include ALL samples (big bam)
    for dataset in r:
        
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()
