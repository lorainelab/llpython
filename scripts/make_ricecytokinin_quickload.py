#!/usr/bin/env python

import sys,os
import make_annots_xml as mk

ANNOTSBACKGROUND='FFFFFF'

# color for control, root
cr="1F78B4"
# color for control, shoot
cs="33A02C"
# color for treatment, root
tr='9900FF' 
# color for treatment, shoot
ts='8B2323'

u = 'O_sativa_japonica_Oct_2011/JK-TH2.0.5_processed'

samples =[['BA2h-R1','BA Root1',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R2','BA Root2',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R3','BA Root3',tr,ANNOTSBACKGROUND,u],
         ['Control2h-R1','Cntrl Root1',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R2','Cntrl Root2',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R3','Cntrl Root3',cr,ANNOTSBACKGROUND,u],
         ['BA2h-S1','BA Shoot1',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S2','BA Shoot2',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S3','BA Shoot3',ts,ANNOTSBACKGROUND,u],
         ['Control2h-S1','Cntrl Shoot1',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S2','Cntrl Shoot2',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S3','Cntrl Shoot3',cs,ANNOTSBACKGROUND,u]]

def main(fname=None):
    txt=mk.makeFilesTag()
    r = mk.makeSamplesForRNASeq(lsts=samples,
                                folder='RNA-Seq / Cytokinin',
                                deploy_dir=u.split(os.sep)[-1],
                                all=False)
    for dataset in r:
        tag = mk.makeFileTag(dataset)
        txt=txt+tag
    txt=txt+mk.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()

