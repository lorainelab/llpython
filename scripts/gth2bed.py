import sys, re


	
PGL_open = re.compile('<predicted_gene_location>')
PGL_close = re.compile('</predicted_gene_location>') #master level

SUPPORT_open = re.compile('<supporting_evidence') 
SUPPORT_close = re.compile('</supporting_evidence>') #here's our alignment info

PGSline_open = re.compile('<PGS_line>') 
PGSline_close = re.compile('</PGS_line>')#multiple of these

gDNA_re = re.compile('<gDNA id="(.*)" strand="(.*)"/>')
refNAME_re = re.compile('<referenceDNA id="(.*)" strand="(.*)"/>')
EXON_re = re.compile('<exon start="(.*)" stop="(.*)"/>')
gDNAnone_re = re.compile('<none gDNA_id="(.*)"/>')

def fileParse(infile,outfile):
	
	fh = open(infile,'r')
	fout = open(outfile,'w')


	output = ""
	line = 1
	while line != "":
		line = fh.readline().strip()
		if PGL_open.match(line):
			curChr = -1
			curSupport = -1
			curStrand = -1
			#we have a new one - reset all the variables
		
		if SUPPORT_open.match(line):
			curSupport = supportSearch(fh)#goto the support search and get some info
			
		if gDNA_re.match(line):
			(curChr,curStrand) = gDNA_re.match(line).groups()
		
		if gDNAnone_re.match(line) and (curChr==-1 and curStrand==-1):
			curChr = gDNAnone_re.match(line).groups()[0]
			curStrand = "+"
			
		if PGL_close.match(line):
			#check all the variables and report complete stuff
			if curChr != -1 and curSupport != -1 and curStrand != -1:
				fout.write(bedCreater(curSupport,curChr,curStrand))

def supportSearch(fh):
	"""We'll return what we found. something to break the loop above"""
	ret = {}
	while True:
		line = fh.readline().strip()
		if PGSline_open.match(line):
			ret.update(pgsLine(fh))
					
		if SUPPORT_close.match(line):
			return ret
			
def pgsLine(fh):
	"""We'll do the work and return what we found and make something break the loop"""
	
	line = fh.readline().strip()

	if not line == "<gDNA_exon_coordinates>":
		print "we have a problem with assumptions"
	
	ret = {}#structure name:[(exonstart,exonstop),(start,stop)...]
	exons = []
	line = fh.readline().strip()
	while not line == "</gDNA_exon_coordinates>":
		exons.append(EXON_re.match(line).groups()) #I can do more parsing here
		line = fh.readline().strip()
	
	(name,strand) = refNAME_re.match(fh.readline().strip()).groups() #should be the <referenceDNA id="F1X5M6O01ECUV8" strand="+"/>
	
	if not PGSline_close.match(fh.readline().strip()):
		print "WE HAVE A PROBLEM WITH ASSUMPTIONS"
		
	return {name+strand:exons}
	
def bedCreater(support, chrom, strand):
	"""
	Writes the bed file
	
	chrom	chrStart	chrEnd	name	score(0)	strand	thickStart	thickEnd	itemRGB(0)	blockCount	blockSizes	blockStarts
	"""
	
	ret = ""
	
	for key in support.keys():
		#do the work to sort the exons and find the starts and find the lengths
		name = key[:-1]
		kStrand = key[-1]
		
		starts = []
		lengths = []
		
		#converting from one base to interbase
		for exons in support[key]:
			e1 = int(exons[0])
			e2 = int(exons[1])
			cStart = min(e1,e2)
			cEnd  = max(e1,e2)
			starts.append(cStart-1)
			lengths.append(cEnd-cStart+1)
		
		sortBlocks(starts,lengths)
		
		s = int(starts[0])
		e = int(starts[-1]+lengths[-1])
		
		if s > e:
			t = e
			e = s
			s = t
		
		cnt = str(len(starts))
		
		starts =  ",".join(map(lambda x:str(x-s), starts))+","
		lengths = ",".join(map(lambda x:str(x), lengths))+","
		
		ret += "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (chrom, s, e, name, "0", kStrand, s, e, "0", cnt, lengths, starts)
	
	return ret
	
def sortBlocks(starts,lengths):
    """
    Function: ensures the block starts read from a bed file are in order with a simple bubble sort.
              This needs to be checked for order to make introns
    Args: starts - list of block starts
          lengths - list of block lengths (these need to be moved in order with starts
    Returns: None, all sorting is done in place
    """

    for i in range(0,len(starts)-1):
        swapMade = False#short circuiting. if no swaps, we can quit
        for j in range(0,len(starts)-1-i):
            if starts[j+1]<starts[j]:
                tempS = starts[j]
                tempL = lengths[j]
                starts[j] = starts[j+1]
                lengths[j] = lengths[j+1]
                starts[j+1] = tempS
                lengths[j+1] = tempL
                swapMade = True

        if not swapMade: break

	
	
if __name__ == '__main__':
	(infile, outfile) = sys.argv[1:]

	fileParse(infile,outfile)