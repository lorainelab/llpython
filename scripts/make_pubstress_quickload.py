#!/usr/bin/env python

import make_annots_xml as m
import sys

genome='A_thaliana_Jun_2009'
#deploy_dir='heat_drought_2010_TH2.0.5_processed'
deploy_dir='hotT1'
du=genome+'/'+deploy_dir

ab='ffffff' # track background
wet='3366FF'
dry='990000'
cool='336600'
hot='CC0033'

drought_samples =  [["WetDT2","Wet D",wet,ab,du],
                    ["WetFT2","Wet F",wet,ab,du],
                    ["DryBT2","Dry B",dry,ab,du],
                    ["DryCT2","Dry C",dry,ab,du],
                    ["DryET2","Dry E",dry,ab,du]]

heat = [["CoolL1T1","Cool L1, T1 (3h, 22 deg)",cool,ab,du],
        ["CoolL2T1","Cool L2, T1 (3h, 22 deg)",cool,ab,du],
#        ["HotI1T1","Hot I1, T1 (3h, 38 deg)",hot,ab,du],
        ["HotI2T1","Hot I2, T1 (3h, 38 deg)",hot,ab,du],
        ["HotK1T1","Hot K1, T1 (3h, 38 deg)",hot,ab,du],
        ["HotK2T1","Hot K2, T1 (3h, 38 deg)",hot,ab,du]]#,
#         ["CoolHT2","Cool H, T2 recovery (24h, 22 deg)",cool,ab,du],
#         ["CoolL1T2","Cool L1, T2 recovery (24h, 22 deg)",cool,ab,du],
#         ["HotI1T2","Hot I1, T2 recovery (24h, 22 deg)",hot,ab,du],
#         ["HotK1T2","Hot K1, T2 recovery (24h, 22 deg)",hot,ab,du],
#         ["HotK2T2","Hot K2, T2 recovery (24h, 22 deg)",hot,ab,du]]

def main(fname=None):
    txt = m.makeFilesTag()
#    r = m.makeSamplesForRNASeq(lsts=drought_samples,
#                               folder='Drought',
#                               deploy_dir=deploy_dir,
#                               all=True)
#    for dataset in r:
#        tag = m.makeFileTag(dataset)
#        txt = txt + tag
    r = m.makeSamplesForRNASeq(lsts=heat,
                               folder='Heat',
                               deploy_dir=deploy_dir,
                               all=True)
    for dataset in r:
        tag = m.makeFileTag(dataset)
        txt = txt + tag
    txt = txt + m.closeFilesTag()
    if fname:
        fh = open(fname,'w')
    else:
        fh = sys.stdout
    fh.write(txt)
    if fname:
        fh.close()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        main()
