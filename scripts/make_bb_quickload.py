#!/usr/bin/env python

from make_annots_xml import * 
import os,sys

# constants
ANNOTSBACKGROUND='FFFFFF'
ab='ffffff'
genome='V_corymbosum_scaffold_May_2013'
u='V_corymbosum_scaffold_May_2013'
bb_url='V_corymbosum_scaffold_May_2013/bb_se_TH2.0.6_6K_processed'
ripe="0000ff"
pink="FF55FF"
green="006600"
cup="339966"
pad="00FF00"
w="FFFFFF"

bb_se=[['2-41_ripe','ripe 2-41',ripe,w,bb_url],
       ['2-42_ripe','ripe 2-42',ripe,w,bb_url],
       ['3-33_ripe','ripe 3-33',ripe,w,bb_url],
       
       ['2-41_pink','pink 2-41',pink,w,bb_url],
       ['2-42_pink','pink 2-42',pink,w,bb_url],
       ['3-33_pink','pink 3-33',pink,w,bb_url],
       
       ['2-41_green','green 2-41',green,w,bb_url],
       ['2-42_green','green 2-42',green,w,bb_url],
       ['3-33_green','green 3-33',green,w,bb_url],
       
       ['2-41_cup','cup 2-41',cup,w,bb_url],
       ['2-42_cup','cup 2-42',cup,w,bb_url],
       ['3-33_cup','cup 3-33',cup,w,bb_url],
       
       ['2-41_pad','pad 2-41',pad,w,bb_url],
       ['3-33_pad_a','pad 3-33a',pad,w,bb_url],
       ['3-33_pad_b','pad 3-33b',pad,w,bb_url]]

def get454():
    samples=[]
    # DHMRI green
    folder = "454 Alignments / 2009 O'Neal berries /"
    dhmri_454_green='GI3MI6V02'
    style = QuickloadReadStyle(foreground=green,background=ANNOTSBACKGROUND)
    descr=description="Green berries harvested May 2009 from 2-41, GMAP alignments of trimmed sequences"
    fn=dhmri_454_green+'.fastq_trimmed_sorted.bam'
    title=folder + "Green O'Neal"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    samples.append(s)

    # DHMRI ripe
    dhmri_454_ripe='GI3MI6V01'
    style = QuickloadReadStyle(foreground=ripe,background=ANNOTSBACKGROUND)
    descr=description="Ripe berries harvested June 2009 from 2-40,41,42, GMAP alignments of trimmed sequences"
    fn=dhmri_454_ripe+'.fastq_trimmed_sorted.bam'
    title=folder + "Ripe O'Neal"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    samples.append(s)

    # NCSU green
    folder = "454 Alignments / 2010 O'Neal berries / "
    ncsu_454_green='onunr'
    style = QuickloadReadStyle(foreground=green,background=ANNOTSBACKGROUND)
    descr=description="Green O'Neal berries harvested 2010, GMAP alignments of trimmed sequences"
    fn=ncsu_454_green+'.fastq_trimmed_sorted.bam'
    title=folder + "Green O'Neal"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    samples.append(s)

    # NCSU ripe
    ncsu_454_ripe='onrip'
    style = QuickloadReadStyle(foreground=ripe,background=ANNOTSBACKGROUND)
    descr=description="Ripe O'Neal berries harvested 2010, GMAP alignments of trimmed sequences"
    fn=ncsu_454_ripe+'.fastq_trimmed_sorted.bam'
    title=folder + "Ripe O'Neal"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    samples.append(s)

    # Rowland lab
    study='SRA047024'
    # this was helpful
    # http://sra.dnanexus.com/runs/SRR353291
    path='V_corymbosum_scaffold_May_2013/01_bams/'
    lst=['SRR353282',
         'SRR353283',
         'SRR353285',
         'SRR353286',
         'SRR353287',
         'SRR353288',
         'SRR353289',
         'SRR353290',
         'SRR353291']
    d = {'SRR353282':'MID1',
         'SRR353283':'MID2',
         'SRR353285':'MID3',
         'SRR353286':'MID4',
         'SRR353287':'MID5',
         'SRR353288':'MID6',
         'SRR353289':'MID7',
         'SRR353290':'MID8',
         'SRR353291':'MID10'}
    folder = '454 Alignments / Rowland Lab / '
    style = QuickloadReadStyle(foreground='0033CC',
                               background=ANNOTSBACKGROUND)
    descr='Sequences from Short Read Archive study %s from USDA project led by Jeannie Rowland'%study
    #u=path
    for name in lst:
        sample=d[name]
        title=folder+sample + ' ' + name
        fn='01_bams/'+name+'.fastq.fasta_sorted.bam'
        s = Annotations(fn=fn,description=descr,title=title,style=style)
        samples.append(s)
    return samples

def getBbGeneModels():
    annots=[]
    title = "Blueberry genes"
    descr="Reference gene models from Vikas Gupta's annotation pipeline."
    style=QuickloadAnnotStyle(foreground='000000',
                              background=ANNOTSBACKGROUND,
                              load_hint='Whole Sequence')
    fn="V_corymbosum_scaffold_May_2013_wDescrPwy.bed.gz"
    s = Annotations(fn=fn,
                    description=descr,
                    title=title,
                    style=style)
    annots.append(s)

    style=QuickloadAnnotStyle(foreground='CC0033',
                              background=ANNOTSBACKGROUND)
    title="Gene Predictions / CuffLinks"
    descr="CuffLinks gene models from RNA-Seq Berry Development"
    style=QuickloadAnnotStyle(foreground='3300FF',
                              background=ANNOTSBACKGROUND)
    fn="Cufflinks_6K.bed.gz"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    annots.append(s)

    style=QuickloadAnnotStyle(foreground='CC0033',
                              background=ANNOTSBACKGROUND)
    fn="11_CombineGFF3/genemark.bed.gz"
    title="Gene Predictions / Genemark"
    descr="Genemark gene models"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    annots.append(s)
    fn="11_CombineGFF3/glimmer.bed.gz"

    title="Gene Predictions / Glimmer"
    descr="Glimmer gene models"
    style=QuickloadAnnotStyle(foreground='9900FF',
                              background=ANNOTSBACKGROUND)
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    annots.append(s)

    style=QuickloadAnnotStyle(foreground='009900',
                              background=ANNOTSBACKGROUND)
    fn="11_CombineGFF3/augustus.bed.gz"
    title="Gene Predictions / Augustus"
    descr="Augustus gene models"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    annots.append(s)
    return annots

def getESTs():
    annots=[]
    style=QuickloadAnnotStyle(foreground='CC0000',
                              background=ANNOTSBACKGROUND)
    style.forceSet('show2tracks','false')
    style.forceSet('label_field','id')
    fn="EST.bed.gz"
    title="EST"
    descr="GMAP alignments of blueberry ESTs and mRNAs from dbEST and Genbank"
    s = Annotations(fn=fn,description=descr,title=title,style=style)
    annots.append(s)
    return annots

def doBB(annots_file="annots.xml"):
    txt = makeFilesTag()
    datasets = getBbGeneModels()
    for dataset in datasets:
        txt = txt + makeFileTag(dataset)
    datasets = getESTs()
    for dataset in datasets:
        txt = txt + makeFileTag(dataset)
    r = makeSamplesForRNASeq(lsts=bb_se,
                             folder='RNA-Seq Berry Development',
                             deploy_dir='bb_se_TH2.0.6_6K_processed',
                             all=True)
    for dataset in r:
        dataset.setUrl(None)
        tag = makeFileTag(dataset)
        txt=txt+tag
    datasets = get454()
    for dataset in datasets:
        txt = txt + makeFileTag(dataset)
    txt=txt + closeFilesTag()
    fh = open(annots_file,'w')
    fh.write(txt)
    fh.close()

def main():
    doBB()

if __name__ == '__main__':
    main()
    
