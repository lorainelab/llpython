"""Methods related to creating PAXML files for ProtAnnot application"""

import cPickle
import Utils.General as utils
import Utils.DasUcsc as das_utils
import Mapping.Parser.Bed as b
import Mapping
import sys,urllib,os,re
import xml.dom.minidom
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import IUPAC
from Bio import SeqIO


gtltreg = re.compile(r'><')

# note: GRCh37 is hg19

def doIt(bed="refGene.GRCh37.pc.bed",
         refseq="human.gene2refseq.gz",
         tax_id="9606",
         gene_info="human.gene_info.gz",
         min_vars=3,
         genome_version="hg19",
         protein_file="hg19-genomic-translations.aa.fa"):
    """
    Function: Makes PAXML files with diff motifs for H sapiens, M musculus, or any other
              sequenced genome for which Entrez gene and RefSeq gene model annotations are
              available.
    Returns :
    Args    : bed - bed file with gene models
              refseq - mapping between locus ids and refseq rna mappings
              tax_id - the organism's taxonomy id
              gene_info - file from NCBI
              min_vars - only report genes with this many models or more
              protein_fasta - name of the file to write containing protein translations
                              for gene models
    """
    # get the gene models
    models = readBed(fn=bed)
    sys.stderr.write("Got models from %s\n"%bed)
    # get rid of the ones that map to multiple locations in the genome
    models = filterModels(models=models).values()
    # get a mapping between gene ids (Entrez Gene ids) and gene model ids (accessions)
    accs = getRefSeqAccs(fn=refseq,
                         tax_id=tax_id)
    # add accession and entrez gene id information to gene models
    addDescriptors(d=accs,
                   models=models)
    gene_info = getGeneInfo(fn=gene_info,tax_id=tax_id)
    addGeneInfo(models,gene_info)
    genes_d = groupGenes(models,filter=min_vars) # only care about genes with 3 or more models
    models = []
    for geneid in genes_d.keys():
        subd = genes_d[geneid]
        for model in subd.values():
            models.append(model)
    # use UCSC DAS1 to retrieve sequence for exons
    # very slow
    getSeqForModels(models,gversion=genome_version,force=1) 
    writeAddSeqs(models,fn=protein_file)
    return genes_d
    
    
def readBed(fn='refGene.GRCh37.pc.bed'):
    """
    Function: Read a bed file and create gene model objects
    Returns : 
    Args    : fn - string, name of file to read.
    """
    lst = b.bed2feats(fname=fn,
                      producer="RefSeq",
                      has_orf="1",
                      format="ucsc",
                      group_feat="mRNA",
                      sub_feat="exon",
                      skip_uns="1")
    return lst

def filterModels(models=None):
    """
    Function: Remove all models with multiple mappings.
    Returns : dictionary where keys are model display ids (accessions)
              and values are the models themselves 
    Args    : models - output from readBed
    """
    d = {}
    for m in models:
        display_id = m.getDisplayId()
        if not d.has_key(display_id):
            d[display_id]=[m]
        else:
            d[display_id].append(m)
    uniques = filter(lambda x:len(d[x])==1,d.keys())
    newd = {}
    for key in uniques:
        newd[key]=d[key][0]
    return newd

def addDescriptors(d=None,models=None):
    """
    Function: Transfer values from d as descriptors for models
    Returns : 
    Args    : models - a list, output from getBed
              d - output from getRefSeqAccs

    Example descriptors that will be added:

    {'mRNA accession': 'NM_001003806.1',
    'mRNA GI': '51702237',
    'protein GI': '51702238',
    'Entrez Gene': '445347',
    'protein_product_id': 'NP_001003806.1'}

    """
    for m in models:
        acc = m.getDisplayId()
        if d.has_key(acc):
            subd = d[acc]
            for key in subd.keys():
                subval = subd[key]
                m.setKeyVal(key,subval)
                    
    
def getRefSeqAccs(fn="human.gene2refseq.gz",
                  tax_id="9606"):
    """
    Function: Create a mapping between gene ids and
              RefSeq accessions (protein and mRNA)
    Returns : dictionary - keys are accessions, values are 
                 sub-dictionaries, with keys, values both strings:
                   protein_product_id - value of protein accessions
                   mRNA accession - the mRNA accession
                   mRNA GI - the GenBank identifier for the mRNA accession
                   protein GI - the Genbank identifier for the protein product accession
                   Entrez Gene - Entrez Gene id
    Args    : fn - name of file to read
              tax_id - limit the analysis to genes from this species

    file comes from:
      ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2refseq.gz

    The header:

    #Format: tax_id GeneID status RNA_nucleotide_accession.version
    #RNA_nucleotide_gi
    protein_accession.version protein_gi genomic_nucleotide_accession.version
    genomic_nucleotide_gi start_position_on_the_genomic_accession
    end_position_on_the_genomic_accession orientation assembly
    (tab is used as a separator, pound sign - start of a comment)
    """
    fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        if not toks[0]==tax_id:
            continue
        gene_id = toks[1]
        mrna_acc = toks[3]
        if mrna_acc == '-':
            continue
        mrna_gi = toks[4]
        protein_acc = toks[5]
        protein_gi = toks[6]
        key = mrna_acc.split('.')[0] # we use accession, no version as the key
        if d.has_key(key):
            if not gene_id == d[key]['Entrez Gene']:
                raise Exception("Duplicate gene entry for: %s gene id: %s\n" %
                                (key,gene_id))
        else:
            subd = {}
            subd['Entrez Gene']=gene_id
            subd['mRNA accession']=mrna_acc
            subd['mRNA GI']=mrna_gi
            subd['protein GI']=protein_gi
            subd['protein_product_id']=protein_acc # ProtAnnot needs this!
            d[key]=subd
    return d

def addGeneInfo(models,gene_d=None):
    """
    Function: Add data from gene_d to models, as key values
    Returns :
    Args    : models - list, output from getBed, after running
                       addDescriptors
              gene_d - dictionary, output from getGeneInfo
    """
    for model in models:
        entrez_id = model.getVal('Entrez Gene')
        if gene_d.has_key(entrez_id):
            subd = gene_d[entrez_id]
            for subd_key in subd.keys():
                model.setKeyVal(subd_key,subd[subd_key])
        else:
            sys.stderr.write("Warning: no Entrez Gene id for: %s\n"%model.getDisplayId())

def dumpModels(fn='saved_models.dump',models=None):
    """
    Function: Save the given models to a file on disk.
    Returns :
    Args    : models - list
              fn - name of file to dump the list to
    """
    fh = open(fn,'w')
    cPickle.dump(models,fh)
    fh.close()
    
def getGeneInfo(fn='human.gene_info.gz',
                tax_id='9606'):
    """
    Function: Make and return a dictionary with gene information,
              keys are Entrez Gene ids, values are fields from
              the Entrez Gene gene_info file
    Returns : dictionary
    Args    : fn - name of Gene Info file

    See: ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz
    """
    fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        gene_id = toks[1]
        symbol = toks[2]
        descr = toks[8]
        d[gene_id]={'gene_symbol':symbol,
                    'gene_description':descr}
    return d

def groupGenes(models,filter=3):
    """
    Function: Group the models according to shared Entrez Gene ids
    Returns : dictionary - keys are Entrez Gene ids, values are dictionaries of
                 models, with display ids as keys and models as values
    Args    : models - a list of CompoundDNASeqFeature objects, with Entrez Gene
                 key val descriptors set
              filter - if more than 1, only include genes with this number (or more)
                 models
    """
    d = {}
    for model in models:
        gid  = model.getVal('Entrez Gene')
        if not gid:
            raise Exception("No Entrez Gene id for %s\n",model.getDisplayId())
        if d.has_key(gid):
            subd = d[gid]
        else:
            subd = {}
            d[gid]=subd
        d[gid][model.getDisplayId()]=model
    if filter>1:
        new_d = {}
        for each in d.keys():
            if len(d[each].keys())>=filter:
                new_d[each]=d[each]
        d = new_d
    return d

def filterByDiffMotifs(genes_d):
    """
    Function: Grab all the gene groups where there is a diff motif
    Returns : dictionary
    Args    : genes_id - dictionary, output from groupGenes
    """
    new_d = {}
    tested=0
    diffs=0
    for entrez_id in genes_d.keys():
        models = genes_d[entrez_id].values()
        try:
            diff = diffMotifs(models=models)
            tested = tested+1
            if diff:
                diffs=diffs+1
                new_d[entrez_id]=genes_d[entrez_id]
        except NoSimSearchException:
            sys.stderr.write("Couldn't test Entrez Gene id %s\n"%entrez_id)
            continue
    sys.stderr.write("Got %i diff motifs from %i genes tested\n"%(diffs,tested))
    return new_d

def writePaxmls(gversion="hg19",genes_d=None,paxml_dir="tmp",jobsdir="iprsearch"):
    """
    Function: Write PAXML files for every gene in genes_d
    Returns : 
    Args    : 
    """
    # genes_d is output from groupGenes
    num_wrote = 0
    for entrez_id in genes_d.keys():
        models = genes_d[entrez_id].values()
        fn = map(lambda x:x.getVal('gene_symbol'),models)[0]+'.'+entrez_id+'.paxml'
        try:
            fh = open(paxml_dir+os.sep+fn,'r')
            fh.close()
        except IOError:
            sys.stderr.write("Starting: %s\n"%fn)
            did_it = writePaxml(gversion=gversion,
                                models=models,
                                paxml_dir=paxml_dir,
                                jobsdir=jobsdir,
                                fn=fn)
            num_wrote = num_wrote + did_it
            if not did_it:
                sys.stderr.write("Didn't write: %s\n"%fn)
            else:
                sys.stderr.write("Wrote: %s\n"%fn)
    sys.stderr.write("Wrote: %i files\n"%num_wrote)

def writePaxml(gversion="hg19",
               models=None,
               paxml_dir='paxml',
               jobsdir="iprsearch",
               fn=None):
    """
    Function: Write ProtAnnot XML
    Returns : 1 - if we wrote the PAXML, 0 if not
    Args    : gversion - genome version
              models - list, grouped gene models representing one gene
              paxml - string, name of directory where PAXML files will go
              jobsdir - string, name of directory where iprscan results live
              fn - string, name of PAXML file to write

    If iprscan has not been run for each model, returns 0.
    """
    for model in models:
        if not model.getVal('simsearches'):
            try:
                addSimsearch(model,jobsdir=jobsdir)
            except IOError:
                sys.stderr.write("Can't write PAXML. Don't have simsearch results.\n")
                return 0
    if not fn:
        fn = map(lambda x:x.getVal('gene_symbol'),models)[0]+'.paxml'
    fn = paxml_dir + os.sep + fn
    doc = makePaxml(gversion=gversion,models=models)
    fh = open(fn,'w')
    txt = doc.toxml(encoding="utf-8")
    txt = gtltreg.sub('>\n<',txt)
    fh.write(txt)
    fh.close()
    return 1

def makePaxml(gversion="hg19",models=None):
    """
    Function: Create an xml.dom.minidom.Document representing a genomic scene
    Returns : An xml.dom.minidom.Document
    Args    : gversion - the genome version
              models - a list of mRNA gene models, with protein annotations and
                       descriptors.
    """
    dom = xml.dom.minidom.getDOMImplementation()
    doctype=dom.createDocumentType("dnaseq","","")
    doc = dom.createDocument(None,"dnaseq",doctype)
    top_el = doc.documentElement
    top_el.setAttribute("xmlns:xs","http://www.w3.org/2001/XMLSchema-instance")
    top_el.setAttribute("xs:noNamespaceSchemaLocation","protannot.xsd")
    top_el.setAttribute("version",gversion)
    min_start = None
    max_end = 0
    seqname = 0
    overall_strand = 0
    for model in models:
        simsearches = None
        ppid = None
        model_seqname = model.getSeqname()
        name = model.getDisplayId()
        start = model.getStart()
        end = model.getStart()+model.getLength()
        strand = model.getStrand()
        if not overall_strand:
            overall_strand = strand
        elif not overall_strand == strand:
            raise Exception("Different strands for %s"%name)
        if end > max_end:
            max_end = end
        if not min_start:
            min_start = start
        elif start < min_start:
            min_start = start
        rnael = doc.createElement("mRNA")
        top_el.appendChild(rnael)
        rnael.setAttribute("start",str(start))
        rnael.setAttribute("end",str(end))
        if strand == -1:
            strand = '-'
        elif strand == 1:
            strand = '+'
        rnael.setAttribute("strand",strand)
        if not seqname:
            seqname = model_seqname
            top_el.setAttribute("seq",seqname)
        elif not seqname == model_seqname:
            raise Exception("Models on different chromosomes: %s",name)
        for key in model.getKeyVals().keys():
            val = model.getVal(key)
            if key == "simsearches":
                simsearches = val
                continue
            if key == 'protein_product_id':
                ppid = val
            if key == 'gene_symbol':
                key = 'Gene symbol'
            if key == 'gene_description':
                key = 'Gene Description'
            if key in  ['mRNA sequence','mRNA coding sequence','protein sequence']:
                val = str(val.seq)
            if key == 'Gene symbol':
                symbol = val
            if key == 'mRNA sequence':
                key = 'mRNA coding sequence'
            d_el = doc.createElement("descriptor")
            d_el.setAttribute("type",key)
            text = doc.createTextNode(val)
            d_el.appendChild(text)
            rnael.appendChild(d_el)
            if key == 'mRNA accession':
                u = 'http://www.ncbi.nlm.nih.gov/nuccore/%s'%val
                d_el = doc.createElement("descriptor")
                d_el.setAttribute("type","URL")
                d_el.appendChild(doc.createTextNode(u))
                rnael.appendChild(d_el)
        for exon in model.sortedFeats(feat_type='exon'):
            estart = exon.getStart()
            eend = estart + exon.getLength()
            e_el = doc.createElement('exon')
            e_el.setAttribute("start",str(estart))
            e_el.setAttribute("end",str(eend))
            val = exon.getVal("seq")
            if val:
                d_el = doc.createElement("descriptor")
                d_el.setAttribute("type","genomic sequence")
                d_el.appendChild(doc.createTextNode(val.upper()))
                e_el.appendChild(d_el)
            rnael.appendChild(e_el)
        cds = model.getFeats(feat_type='CDS')[0]
        # NOTE: the CDS includes the stop codon
        cds_el = doc.createElement('cds')
        cds_el.setAttribute('start',
                            str(cds.getStart()))
        cds_el.setAttribute('end',
                            str(cds.getStart()+cds.getLength()))
        rnael.appendChild(cds_el)
        if simsearches:
            aa_el = doc.createElement("aaseq")
            top_el.appendChild(aa_el)
            aa_el.setAttribute('id',ppid) # critical! matching aaseq id attributes to mRNA protein_product_id
            # descriptors is how ProtAnnot matches simhits with their corresponding encoding mRNAs
            for simsearch in simsearches:
                simsearch_el = doc.createElement("simsearch")
                aa_el.appendChild(simsearch_el)
                simsearch_el.setAttribute("method",simsearch.getMethod())
                sim_hits = simsearch.getSimHits()
                for sim_hit in sim_hits:
                    sh_el = doc.createElement("simhit")
                    simsearch_el.appendChild(sh_el)
                    key_vals = sim_hit.getKeyVals()
                    for key in key_vals.keys():
                        val = key_vals[key]
                        d_el = doc.createElement("descriptor")
                        d_el.setAttribute("type",key)
                        d_el.appendChild(doc.createTextNode(val))
                        sh_el.appendChild(d_el)
                        """
                        if key == 'InterPro Accession':
                            if not val == 'noIPR':
                                url = "http://www.ebi.ac.uk/interpro/IEntry?ac=%s"%val
                            else:
                                # it's not integrated yet
                                url = "http://www.ebi.ac.uk/interpro/IEntry?ac=%s"%sim_hit.getVal("Match id")
                            d_el = doc.createElement("descriptor")
                            d_el.setAttribute("type","URL")
                            d_el.appendChild(doc.createTextNode(url))
                            sh_el.appendChild(d_el)
                        """
                    simspans = sim_hit.getSpans()
                    for simspan in simspans:
                        start = simspan.getStart()
                        end = simspan.getLength()+start
                        sim_el = doc.createElement("simspan")
                        sh_el.appendChild(sim_el)
                        sim_el.setAttribute("query_start",str(start))
                        sim_el.setAttribute("query_end",str(end))
                        key_vals = simspan.getKeyVals()
                        for key in key_vals.keys():
                            val = key_vals[key]
                            d_el = doc.createElement("descriptor")
                            d_el.setAttribute("type",key)
                            d_el.appendChild(doc.createTextNode(val))
                            sim_el.appendChild(d_el)
    min_start = min_start - 5000
    max_end = max_end + 5000
    seq = grabResidues(gversion=gversion,
                       seqname=seqname,
                       start=min_start+1,
                       end=max_end)
    res_el = doc.createElement('residues')
    res_el.setAttribute("start",str(min_start))
    res_el.setAttribute("end",str(max_end))
    res_el.appendChild(doc.createTextNode(seq))
    top_el.appendChild(res_el)
    return doc
            
def grabResidues(gversion="hg19",
                 start=None,
                 end=None,
                 seqname=None):
    """
    Function: Retrieve DNA sequence
    Returns : string, DNA sequence
    Args    : gversion - string, genome version
              start - int, one-based sequence coordinate
              end - int, one-based sequence coordinate
              seqname - string, name of assembly or chromosome sequence
    """
    u = 'http://genome.cse.ucsc.edu/cgi-bin/das/%s/dna?segment=%s:%i,%i'%\
        (gversion,seqname,start,end)
    parser = make_parser()
    fh = urllib.urlopen(u)
    handler = SequenceHandler()
    parser.setContentHandler(handler)
    parser.parse(fh)
    sequence = handler.seq
    return sequence

class SequenceHandler(ContentHandler):
    """Class for grabbing sequence data from DAS1 server at UCSC"""

    seq = ""
    on_seq = False
    subby = re.compile(r'\s')

    def characters(self,content):
        chars = self.subby.sub('',content).encode('ascii')
        if len(chars)>0:
            self.seq = self.seq + chars
        
def getSeqForModel(model,gversion="hg19",force=0):
    """
    Function: Use the UCSC DAS1 service to retrieve sequences corresponding
              to exons in the given model. Adds a "key val" property to each
              exon, where the key is "seq" and the value is the DNA sequence,
              plus-strand interbase.
    Returns :
    Args    : model - CompoundDNASeqFeature object
              gversion - string, corresponding to a genome version recognized by
                         the DAS1 service
              force - 1 or 0; if 1, force retrieval of the sequence; if 0, only
                      retrieve sequence if the KeyVal "seq" has not alrady been set
    """
    base_u = 'http://genome.cse.ucsc.edu/cgi-bin/das/%s/dna?'%gversion
    parser = make_parser()
    exons = model.getFeats(feat_type="exon")
    for exon in exons:
        if not force and exon.getVal('seq'):
            sys.stderr.write("Already got seq for exon in %s\n"%model.getDisplayId())
            continue
        u = base_u + 'segment=%s:%i,%i'%(model.getSeqname(),
                                exon.getStart()+1,
                                exon.getStart()+exon.getLength())
        fh = urllib.urlopen(u)
        handler = SequenceHandler()
        parser.setContentHandler(handler)
        parser.parse(fh)
        exon.setKeyVal('seq',handler.seq)
    sys.stderr.write("Got sequence for: %s\n"%model.getDisplayId())
    

def getSeqForModels(models,gversion="hg19",force=0):
    """
    Function: Invokes getSeqForModel for each of the given models.
    Returns : 
    Args    : models - a list of models (CompundDNASeqFeature objects)
              gversion - see getSeqForModel
              force - see getSeqForModel
    """
    i = 0
    for model in models:
        getSeqForModel(model,gversion=gversion,force=force)
        i = i + 1
        if i % 100 == 0:
            sys.stderr.write("Got sequence for %i models.\n"%i)

def writeAddSeqs(models,fn="models.aa.fa"):
    """
    Function: Write protein sequence and add protein and mRNA SeqRecord
              objects as properties (key vals) for the given models
    Returns : 
    Args    : models - a list of CompoundDNASeqFeature objects representing gene
                models
              fn - name of protein sequence file to write
    """
    lst = map(lambda x:(x,getTranslatedSeq(x)),models)
    fh = open(fn,'w')
    for each in lst:
        model = each[0]
        mrnaseq = each[1]
        aaseq = mrnaseq.translate()
        if '*' in aaseq:
            sys.stderr.write("Warning: Stop codon in: %s\n"%model.getDisplayId())
        rec = SeqRecord(aaseq,
                        id=model.getDisplayId(),
                        description="genomic translation")
        model.setKeyVal('protein sequence',rec)
        SeqIO.write([rec],fh,'fasta')
        rec = SeqRecord(mrnaseq,
                        id=model.getDisplayId(),
                        description='spliced genomic exons, minus termination codon')
        model.setKeyVal('mRNA sequence',rec)
    fh.close()

def getTranslatedSeq(model):
    """
    Function: Generate the CDS sequence for the given model, using
              its exon and CDS subfeatures. Assumes that getSeqForModel
              has already been invoked on the given model.
    Returns : A Bio.Seq object represent the spliced CDS
    Args    : model - a CompoundDNASeqFeature object
    """
    exons = model.sortedFeats(feat_type='exon')
    cds = model.getFeats(feat_type='CDS')[0]
    cds_start = cds.getStart()
    cds_end = cds.getStart()+cds.getLength()
    mRNA_start = model.getStart()
    cds_seq = ''
    for exon in exons:
       exon_start = exon.getStart()
       exon_end = exon.getStart() + exon.getLength()
       # the entire exon is translated
       if exon_start >= cds_start and exon_end <= cds_end:
           cds_seq = cds_seq + exon.getVal('seq')
           continue
       # we're in the first translated exon
       if cds_start >= exon_start and cds_start <= exon_end:
           subseq = exon.getVal('seq')[cds_start-exon_start:cds_end-exon_start]
           cds_seq = cds_seq + subseq
           continue
       if cds_end <= exon_end and cds_end >= exon_start:
           subseq = exon.getVal('seq')[0:cds_end-exon_start]
           cds_seq = cds_seq + subseq
    bioseq = Seq(cds_seq,IUPAC.unambiguous_dna)
    if model.getStrand()==-1:
        bioseq = bioseq.reverse_complement()
    # get rid of the final three bases, which are the stop codon
    seq = str(bioseq).upper()
    if seq[-3:] in ['TAA','TGA','TAG']:
        seq = seq[0:-3]
        bioseq = Seq(seq,IUPAC.unambiguous_dna)
    else:
        sys.stderr.write("Warning: No stop codon for: %s\n"%model.getDisplayId())
    return bioseq


def runProteins(d,jobsdir='iprsearch',stop_at=100):
    """
    Function: Run InterProScan on the sequences contained in the
              given dictionary
    Returns : a dictionary of job ids
    Args    : d - output from groupGenes
              jobsdir - name of a directory with output from iprscan
              stop_at - int, stop after running this many
    """
    jobsd = {}
    i = 0
    for each in d.keys():
        subd = d[each]
        for model in subd.values():
            name = model.getDisplayId()
            rec = model.getVal('protein sequence')
            if '*' in rec:
                sys.stderr.write("Stop character in %s, skipping it.\n"%name)
                continue
            fn = '%s%s%s.interpro.txt'%(jobsdir,os.sep,name)
            try:
                fh = open(fn,'r')
                sys.stderr.write("%s exists. Skipping %s.\n"%(fn,name))
                fh.close()
                continue
            except IOError:
                sys.stderr.write("Can't find %s. Running %s.\n"%(fn,name))
                jobname = runInterProWS(model,jobsdir=jobsdir)
                jobsd[name]=[model,jobname]
                i = i + 1
                if i >= stop_at:
                    break
        if i >= stop_at:
            break
    return jobsd

def getResults(jobsd,jobsdir="iprsearch"):
    """
    Function: Retrieve InterProScan results for the jobs
              saved as values in jobsd 
    Returns : a copy of jobsd, but only with the jobs that did not
              yet complete
    Args    : jobds - output from runProteins
    """
    not_done = {}
    for each in jobsd.keys():
        model = jobsd[each][0]
        jobname = jobsd[each][1]
        got_it = getResult(model=model,jobsdir=jobsdir,jobname=jobname)
        if not got_it:
            not_done[each]=jobsd[each]
    return not_done
            
def getResult(model=None,jobsdir='iprsearch',jobname=None):
    """
    Function: Retrieves results from the given InterproScan search
    Returns :
    Args    :

    interproscan.pl --status --jobid iprscan-20100226-1214498712
    interproscan.pl --polljob --jobid iprscan-20100226-1214498712

    """
    cmd = 'interproscan.pl --status --jobid %s'%jobname
    fh = os.popen(cmd)
    txt =fh.read().rstrip()
    name = model.getDisplayId()
    fn = '%s%s%s.interpro'%(jobsdir,os.sep,name)
    if txt == 'DONE':
        sys.stderr.write("Retrieving data for: %s\n"%jobname)
        cmd = 'interproscan.pl --polljob --jobid %s --outformat toolraw --outfile %s'%(jobname,fn)
        fh = os.popen(cmd)
        txt = fh.read()
        fh.close()
        sys.stderr.write(txt)
        cmd = 'interproscan.pl --polljob --jobid %s --outformat toolxml --outfile %s'%(jobname,fn)
        fh = os.popen(cmd)
        txt = fh.read()
        fh.close()
        sys.stderr.write(txt)
        return True
    return False

def runInterProWS(model=None,jobsdir='iprsearch'):
    """
    Function:
    Returns :
    Args    :

    interproscan.pl --status --jobid iprscan-20100226-1214498712
    interproscan.pl --polljob --jobid iprscan-20100226-1214498712

    """
    rec = model.getVal('protein sequence')
    name = model.getDisplayId()
    tmpfn = '%s%s%s.tmp.fa'%(jobsdir,os.sep,name)
    sys.stderr.write("making: %s\n"%tmpfn)
    fh = open(tmpfn,'w')
    SeqIO.write([rec],fh,'fasta')
    fh.close()
    fh = os.popen('interproscan.pl --email aloraine@uncc.edu --async %s 2>/dev/null'%tmpfn)
    txt = fh.read()
    fh.close()
    jobname = txt.rstrip()
    sys.stderr.write("job:%s\n"%jobname)
    sys.stderr.write("deleting: %s\n"%tmpfn)
    os.unlink(tmpfn)
    return jobname

def addSimSearches(models,jobsdir="iprsearch"):
    """
    Function: Read the XML documents returned by InterProScan
    Returns : 
    Args    : a list of gene models

    Note: all SimHit and SimSpan coordinates are interbase, with the protein
    translation providing the reference coordinate system.
    """
    for model in models:
        addSimsearch(model,jobsdir=jobsdir)
    
    
def addSimsearch(model,jobsdir="iprsearch",
                 include_unintegrated=False):
    """
    Function: Read the given XML document returned by InterProScan
    Returns : 
    Args    : model - a gene model

    Note: all SimHit and SimSpan coordinates are interbase, with the protein
    translation providing the reference coordinate system.
    """
    name = model.getDisplayId()
    fn = '%s%s%s.interpro.xml'%(jobsdir,os.sep,name)
    sys.stderr.write("Opening: %s\n"%fn)
    try:
        fh = open(fn,'r')
    except IOError:
        sys.stderr.write("Can't open: %s\n"%fn)
        return 0
    doc = xml.dom.minidom.parse(fh)
    fh.close()
    simsearch = readIprXml(doc,
                           include_unintegrated=include_unintegrated)
    model.setKeyVal('simsearches',[simsearch])

def readIprXml(doc,include_unintegrated=False):
    """
    Function: Read the given XML document returned by InterProScan
    Returns : a SimSearch object containing SimHits, with SimSpans attached
    Args    : doc - xml.dom.minidom.Document instance
              include_unintegrated - if False, ignore hits without IPR ids

    Note: all SimHit and SimSpan coordinates should be interbase, with the protein
    translation providing the reference coordinate system.

    Also, some InterProScan results are "unintegrated," meaning they have no
    IPR accession. We can recognize them via their InterPro Type, which is
    "unintegrated." For those, we can't build links to the InterPro site in the
    usual way. 
    """
    simsearch = SimSearch(method="InterPro")
    for node in doc.getElementsByTagName('interpro'):
        interpro_acc = node.getAttribute('id')
        interpro_name = node.getAttribute('name')
        interpro_type = node.getAttribute('type')
        for match_node in node.getElementsByTagName('match'):
            match_id = match_node.getAttribute('id')
            match_name = match_node.getAttribute('name')
            match_db = match_node.getAttribute('dbname')
            simspans = []
            for loc_node in match_node.getElementsByTagName('location'):
                start = int(loc_node.getAttribute('start'))-1
                end = int(loc_node.getAttribute('end'))
                length = end - start
                simspan=SimSpan(start=start,length=length)
                simspan.setKeyVal("score",loc_node.getAttribute("score"))
                simspan.setKeyVal("evidence",loc_node.getAttribute("evidence"))
                simspans.append(simspan)
            simhit = SimHit(simspans=simspans)
            simhit.setDisplayId(match_id)
            simhit.setKeyVal('InterPro Accession',interpro_acc)
            simhit.setKeyVal('InterPro Name',interpro_name)
            simhit.setKeyVal('InterPro Type',interpro_type)
            simhit.setKeyVal('Match id',match_id)
            simhit.setKeyVal('Match Database',match_db)
            url = None
            if interpro_acc.startswith('IPR'):
                url = "http://www.ebi.ac.uk/interpro/IEntry?ac=%s"%interpro_acc
            else:
                if match_db == 'PANTHER':
                    url = "http://www.pantherdb.org/panther/family.do?clsAccession=%s"%match_id
                if match_db == 'SUPERFAMILY':
                    url = "http://supfam.cs.bris.ac.uk/SUPERFAMILY/cgi-bin/scop.cgi?ipid=%s"%match_id
            if not url:
                url = "http://www.ebi.ac.uk/interpro/ISearch?query=%s"%match_id
            if not include_unintegrated and not interpro_acc.startswith('IPR'):
                if match_db=='TMHMM':
                    sys.stderr.write("Got a TMHMM hit.\n")
                else:
                    if match_id =='SignalP':
                        sys.stderr.write("Got a SignalP hit.\n")
                    else:
                        continue
            simhit.setKeyVal("URL",url)
            simsearch.addSimHit(simhit)
    return simsearch
            
def diffMotifs(models=None):
    """
    Function: Test whether the given models have different pattern of motifs
    Returns : boolean
    Args    : models - a list of models, they should be from the same gene
    """
    if len(filter(lambda x:not x.getVal("simsearches"),models))>0:
        raise NoSimSearchException("Can't evaluate DiffMotifs without simsearches")
    i = 0
    for this in models[:-1]:
        for that in models[i+1:]:
            if diffMotif(this,that):
                return True
        i = i + 1
    return False

class NoSimSearchException(Exception):
    pass

def diffMotif(this,that):
    """
    Function: Compares two models and determines if they have different motif patterns
    Returns : boolean
    Args    : this, that - two gene models
    """
    this_sss = this.getVal('simsearches')
    that_sss = that.getVal('simsearches')
    this_hits = {}
    that_hits = {}
    for ss in this_sss:
        this_hits[ss.getMethod()]=ss.getSimHits()
    for ss in that_sss:
        that_hits[ss.getMethod()]=ss.getSimHits()
    for key in this_hits.keys():
        this_hits = this_hits[key]
        that_hits = that_hits[key]
        for hit in this_hits:
            if not foundHit(hit,that_hits):
                return True
    return False

def foundHit(hit,hits):
    """
    Function:
    Returns :
    Args    :
    """
    for other_hit in hits:
        if other_hit.getDisplayId()==hit.getDisplayId():
            if len(other_hit.getSpans())==len(hit.getSpans()):
                return True
    return False

class SimHit(Mapping.Feature.Identifiable,
             Mapping.Feature.KeyVal,
             Mapping.Feature.Range):

    def __init__(self,simspans=None,**kwargs):
        Mapping.Feature.Identifiable.__init__(self,**kwargs)
        Mapping.Feature.KeyVal.__init__(self,**kwargs)
        Mapping.Feature.Range.__init__(self,**kwargs)
        self._spans = simspans

    def getSpans(self):
        return self._spans


class SimSpan(Mapping.Feature.Identifiable,
              Mapping.Feature.KeyVal,
              Mapping.Feature.Range):

    def __init__(self,**kwargs):
        Mapping.Feature.Identifiable.__init__(self,**kwargs)
        Mapping.Feature.KeyVal.__init__(self,**kwargs)
        Mapping.Feature.Range.__init__(self,**kwargs)


class SimSearch():

    def __init__(self,method=None):
        self.setMethod(method)
        self._simhits = []

    def setMethod(self,method):
        self._method = method

    def getMethod(self):
        return self._method

    def addSimHit(self,simhit):
        self._simhits.append(simhit)

    def getSimHits(self):
        return self._simhits

            
def zDummy():
    """
    Function:
    Returns :
    Args    :
    """
    pass

